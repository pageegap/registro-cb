$(document).ready(function() {

    $('a[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            var topHeight = 70;
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top - topHeight
                }, 1000);
                return false;
            }
        }
    });

    $.validator.addMethod("nospacesonly", function (value, element) {
        return this.optional(element) || valida(value);
    }, "Debe contener información");
    $.validator.addMethod("valueNotEquals", function(value, element, arg){
        return arg !== value;
    }, "No debe ser igual a");
    $.validator.addMethod("nospace", function(value, element) { 
        return value.indexOf(" ") < 0 && value != ""; 
    }, "No se aceptan espacios");
    $.validator.addMethod("regex", function(value, element, regexp) {
        return this.optional(element) || regexp.test(value);
    },"Caracteres permitidos");
});

//busca caracteres que no sean espacio en blanco en una cadena
function vacio(q) {
    for (i = 0; i < q.length; i++) {
        if (q.charAt(i) != " ") {
            return true
        }
    }
    return false
}
//valida que el campo no este vacio y no tenga solo espacios en blanco
function valida(text) {
    return vacio(text);
}