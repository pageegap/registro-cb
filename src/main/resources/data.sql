/* initializes the items TODO include new values*/
INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (1,"inscripcion adulto", 100, 150,true,'2019-5-07', 11,300)
INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (35,"inscripcion nino" , 100, 0  ,true,'2019-5-07', 0,10)

INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (2,"desayuno sabado",  200, 60, true,'2019-5-07',11,300)
INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (3,"desayuno domingo", 300, 60, true,'2019-5-07',11,300)
INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (4,"desayuno lunes",   400, 60, true,'2019-5-07',11,300)
INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (5,"comida sabado",    500, 60, true,'2019-5-07',11,300)
INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (6,"comida domingo",   600, 60, true,'2019-5-07',11,300)
INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (7,"cena viernes",     700, 60, true,'2019-5-07',11,300)
INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (8,"cena sabado",      800, 60, true,'2019-5-07',11,300)
INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (9,"cena domingo",     900, 60, true,'2019-5-07',11,300)

INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (10,"desayuno nino sabado",   200, 30,true,'2019-5-07',5,10)
INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (11,"desayuno nino  domingo", 300, 30,true,'2019-5-07',5,10)
INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (12,"desayuno nino lunes",    400, 30,true,'2019-5-07',5,10)
INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (13,"comida nino sabado",     500, 30,true,'2019-5-07',5,10)
INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (14,"comida nino domingo",    600, 30,true,'2019-5-07',5,10)

INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (15,"cena nino viernes", 700, 30,true,'2019-5-07',5,10)
INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (16,"cena nino sabado",  800, 30,true,'2019-5-07',5,10)
INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (17,"cena nino domingo", 900, 30,true,'2019-5-07',5,10)

INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (18,"desayuno bebe sabado",   200, 0,true,'2019-5-07',0,4)
INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (19,"desayuno bebe  domingo", 300, 0,true,'2019-5-07',0,4)
INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (20,"desayuno bebe lunes",    400, 0,true,'2019-5-07',0,4)
INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (21,"comida bebe sabado",     500, 0,true,'2019-5-07',0,4)
INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (22,"comida bebe domingo",    600, 0,true,'2019-5-07',0,4)
INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (23,"cena bebe viernes",      700, 0,true,'2019-5-07',0,4)
INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (24,"cena bebe sabado",       800, 0,true,'2019-5-07',0,4)
INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (25,"cena bebe domingo",      900, 0,true,'2019-5-07',0,4)

INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (26,"estadia adulto", 1000, 150,true,'2019-5-07',11,300)
INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (27,"estadia nino",   1000,  75,true,'2019-5-07',5,10)
INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (28,"estadia bebe",   1000,  0,true,'2019-5-07',0,4)

INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (29,"transporte adulto viernes ida",   1100, 140,true,'2019-5-07',11,300)
INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (30,"transporte adulto sabado ida",    1200, 140,true,'2019-5-07',11,300)
INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (31,"transporte adulto lunes regreso", 1300, 140,true,'2019-5-07',11,300)

INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (32,"transporte nino viernes ida",   1100, 70,true,'2019-5-07',0,10)
INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (33,"transporte nino sabado ida",    1200, 70,true,'2019-5-07',0,10)
INSERT INTO item (item_id,name, type, price,is_active,update_date,initial_age,end_age) values (34,"transporte nino lunes regreso", 1300, 70,true,'2019-5-07',0,10)

/* intializes roles*/
INSERT INTO role (role_id,description,is_active) values (1,"GUEST",true)
INSERT INTO role (role_id,description,is_active) values (2,"DESK_ATTENDANT",true)
INSERT INTO role (role_id,description,is_active) values (3,"LIASON",true)
INSERT INTO role (role_id,description,is_active) values (4,"KITCHEN_ATTENDANT",true)
INSERT INTO role (role_id,description,is_active) values (5,"DESK_ATTENDANT",true)
INSERT INTO role (role_id,description,is_active) values (6,"STAFF",true)
INSERT INTO role (role_id,description,is_active) values (7,"ADMIN",true)

/* intializes users */
/*INSERT INTO user (user_id,email,is_valid,password) values (1,"epacheco2285@gmail.com",true,"welcome1")*/
INSERT INTO user (user_id,email,is_valid,password) values (2,"liason@gmail.com",true,"$2a$10$GDyJshR.FteT96/L/q5c9OAtqCtcXulnE38vb.Gjn/8U48RXJam/u")
INSERT INTO user (user_id,email,is_valid,password) values (3,"admin@gmail.com",true,"$2a$10$GDyJshR.FteT96/L/q5c9OAtqCtcXulnE38vb.Gjn/8U48RXJam/u")
  /* initializes the roles for users*/
/*INSERT INTO user_role  (user_id,role_id) values (1,1) */
INSERT INTO user_role  (user_id,role_id) values (2,1) 
INSERT INTO user_role  (user_id,role_id) values (2,3) 
INSERT INTO user_role  (user_id,role_id) values (3,7)

/* initializes payment types*/
INSERT INTO payment_type (payment_type_id,is_active,description) values (1,1,"normal")
INSERT INTO payment_type (payment_type_id,is_active,description) values (2,1,"cortesia")
INSERT INTO payment_type (payment_type_id,is_active,description) values (3,1,"ofrenda")
