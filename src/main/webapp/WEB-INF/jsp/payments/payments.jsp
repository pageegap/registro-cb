<html>

<head>
    <%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
    <jsp:include page="../layout/head-elements.jsp">
        <jsp:param name="title" value="Registro de perfil" />
        <jsp:param name="description" value="Alta de información de perfil Servicios e Invitados" />
    </jsp:include>
</head>

<body>
    <jsp:include page="../layout/header.jsp" />

    <div class="col-sm-12 col-md-7"><input type="text" name="email" id="liasonEmail" class="form-control"
            value="${liasonEmail}" hidden></div>

    <div class="page__container">
        <div class="container">
            <div class="page__head">
                <h1 class="page__head__title p-4">Total Cobrado</h1>
            </div>
            <div class="page__content bordered mb-5">
                <div class="row m-3">
                    <div class="col-sm-12 col-md-12">
                        <button type="button" class="btn btn-primary float-right" id="showRecords"><i class="icon-format_line_spacing"></i>&nbsp; Ver mis cobros</button>
                    </div>
                </div>
                <div class="row m-3 mb-3">
                    <div class="col-sm-12 col-md-4">
                        <div class="dashboard__card dashboard__card--fullwidth">
                            <div class="dashboard__card__title bg-primary"><i class="icon-attach_money"></i>Cobro Normal
                            </div>
                            <div class="dashboard__card__info">
                                <div id="totalNormal" class="h3 text-center text-dark font-weight-bold">
                                    <fmt:formatNumber value="${totalNormalHtml}" minFractionDigits="2"/></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="dashboard__card dashboard__card--fullwidth">
                            <div class="dashboard__card__title bg-primary"><i class="icon-attach_money"></i>Cortesía
                            </div>
                            <div class="dashboard__card__info">
                                <div id="totalDiscounted" class="h3 text-center text-warning font-weight-bold">
                                    <fmt:formatNumber value="${totalDiscountedHtml}" minFractionDigits="2"/></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="dashboard__card dashboard__card--fullwidth">
                            <div class="dashboard__card__title bg-primary"><i class="icon-attach_money"></i>Suma Total
                            </div>
                            <div class="dashboard__card__info">
                                <div id="totalReceived" class="h3 text-center text-success font-weight-bold">
                                    <fmt:formatNumber value="${totalReceivedHtml}" minFractionDigits="2"/></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="page__head">
                <h1 class="page__head__title p-4">Registra Pago</h1>
            </div>
            <div class="page__content page__content--fixed bordered mb-5">
                <div class="table__header">
                    <div class="table__header__content container">
                        <div class="row mr-2 ml-2 mt-3 mb-3">
                            <div class="col-lg-2 d-none d-lg-block">
                                <h5>Filtrar por: </h5>
                            </div>
                        
                            <div class="col-sm-12 col-md-2">
                                <select class="form-control" id="searchFilter">
                                    <option value="EMAIL">Email</option>
                                    <option value="ID">Id</option>
                                    <option value="IGLESIA">Iglesia</option>
                                    <option value="CIUDAD">Ciudad</option>
                                    <option value="TODOS">Todos</option>
                                </select>
                            </div>
                            <div class="col-sm-12 col-md-8">
                                <div class="input-group ">
                                    <input type="text" id="search-input" class="form-control" placeholder="Ingresa tu búsqueda"
                                        aria-label="Recipient's username" aria-describedby="button-addon2">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary" type="button"
                                            id="search-button">Buscar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table__header__head">
                            <table class="table table-responsive">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col" class="text-center" width="10%">Id</th>
                                        <th scope="col" class="text-center" width="25%">Email</th>
                                        <th scope="col" class="text-center" width="10%">Total</th>
                                        <th scope="col" class="text-center" width="10%">Pagado</th>
                                        <th scope="col" class="text-center" width="10%">Cortesia</th>
                                        <th scope="col" class="text-center" width="10%">Saldo</th>
                                        <th scope="col" class="text-center" width="10%">Completado </th>
                                        <th scope="col" class="text-center" width="15%"></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row m-3">
                    <div class="col-sm-12">
                        <div class="guest__list">
                            <table class="table table-payments table-hover table-responsive">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col" class="text-center" width="10%">Id</th>
                                        <th scope="col" class="text-center" width="25%">Email</th>
                                        <th scope="col" class="text-center" width="10%">Total</th>
                                        <th scope="col" class="text-center" width="10%">Pagado</th>
                                        <th scope="col" class="text-center" width="10%">Cortesia</th>
                                        <th scope="col" class="text-center" width="10%">Saldo</th>
                                        <th scope="col" class="text-center" width="10%">Completado </th>
                                        <th scope="col" class="text-center" width="15%"></th>
                                    </tr>
                                </thead>
                                <tbody id="accounts-list" class="dinamicListGuest tableDinamicList">
                                    <!--Load dinamic -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Payment-->
    <div class="modal fade" id="modalPayment" tabindex="-1" role="dialog" aria-labelledby="modalGuestLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalGuestLabel"> <strong>
                            <div id="emailHolder">${email}</div>
                        </strong></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="payments__register__form">
                        <form name="paymentsRegister" id="paymentsRegister" action="" method="POST">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="row m-2">
                                        <div class="row m-2">
                                            <div class="col-sm-2 col-md-2 text-right"><label for=""
                                                    class="font-weight-bold">Pago:</label></div>
                                            <div class="col-sm-4 col-md-4  "><input type="text" name="amount"
                                                    class="form-control input-text__small" id="amountPaid"></div>
                                            <div class="col-sm-2 col-md-2 text-right"><label for=""
                                                    class="font-weight-bold">Tipo:</label></div>
                                            <div class="col-sm-4 col-md-4">
                                                <select name="paymentType" class="form-control" id="paymentType">
                                                    <option value="normal">Normal</option>
                                                    <option value="cortesia">Cortesía</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="payments__register__data d-flex justify-content-end">
                        <div class="response__message"></div>
                        <div class="payments__register__btn">
                            <div class="d-flex justify-content-end">
                                <button type="button" class="btn btn-primary d-flex flex-row" id="addPayment"><i
                                        class="icon-attach_money"></i> Guardar
                                    Pago</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="recordsList" tabindex="-1" role="dialog" aria-labelledby="modalGuestLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title subtitle--small">Mis Cobros</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table id="table_id" class="display">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Fecha</th>
                                <th>Usuario</th>
                                <th>Abono($)</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary d-flex" id="downloadCobros"><i class="icon-get_app"></i>&nbsp;Descargar Excel</button>

                </div>
            </div>
        </div>
    </div>

    <div id="toastAction" class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-delay="5000" data-animation="true">
        <div class="toast-header">
            <strong class="mr-auto">Registro de Pago</strong>
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="toast-body">
            Tu pago se ha registrado correctamente.
        </div>
    </div>

    <jsp:include page="../layout/footer.jsp" />


    <div class="modal" id="modalConfirmPayment" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Confirma pago</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="height:190px;">
                    <p>¿Registrar pago (<strong><span
                                id="confirmPaymentTypeHtml">${confirmPaymentTypeHtml}</span></strong>) por &#36;
                        <strong><span id="confirmAmountHtml">${confirmAmountHtml}</span></strong>
                        a la cuenta <strong><span id="confirmUserHtml">${confirmUserHtml}</span></strong>?</p>
                    <div class="modal-footer">
                        <div class="d-flex justify-content-end">
                            <div class="modal-footer-buttons">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>&nbsp;
                                <button type="button" class="btn btn-primary" id="confirmPaymentButton">Registar</button>
                            </div>
                            <div class="modal-footer-load">
                                <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <script src="/js/xlsx.full.min.js"></script>

        <script>
            let accounts = [];
            let email = $('#emailHolder').html("");
            let confirmAmountHtml = $('#confirmAmountHtml').html("");
            let confirmPaymentTypeHtml = $('#confirmPaymentTypeHtml').html("");
            let typeHtml = $("#typeHtml").html("");
            let myCobros = [];
            let selectedAccount = -1;

            let totalDiscountedHtml = $('#totalDiscounted').html("0.0");
            let totalReceivedHtml = $('#totalReceived').html("0.0");
            let totalOfferingHtml = $('#totalOffering').html("0.0");
            let totalNormalHtml = $('#totalNormal').html("0.0");

            let confirmPaymentHtml = $('#confirmPaymentHtml').html("");
            let confirmPayTypeHtml = $('#confirmPayTypeHtml').html("");
            let confirmUserHtml = $('#confirmUserHtml').html("");

            //function to compare two accounts
            function compareAccounts(a, b) {
                return (a.userId > b.userId) ? 1 : ((b.userId > a.userId) ? -1 : 0);
            }

            //retrieves ALL users/accounts
            function getSummaryAccountsByFilter(filter, value) {
                $.ajax({
                    url: "/recupera-resumen-cuentas?filter=" + filter + "&value=" + value,
                    type: "GET",
                    success: function (response) {
                        response.sort(compareAccounts);
                        setAccountList(response);
                        accounts = response;
                    }
                });

            }

            //creates dinamically the list of users/accounts
            function setAccountList(accounts) {
                var tmpListGuest = "";
                accounts.forEach(function (element, position) {
                    let topay = element.total - (element.amountPaid + element.totalDiscount);
                    let settledDebt = element.settledDebt;
                    let hasPaidClass = (element.settledDebt !== "-") ? "text-success" : "";
                    let titular = getTitular(element.guests);
                    let name = titular == null ? element.email : titular;
                    let buttonColor = (element.settledDebt !== "-") ? "btn-green" : "btn-success";
                    tmpListGuest = tmpListGuest +
                        "<tr><td class='text-center'><strong>" + element.userId + "</strong></td>" +
                        "<td class='" + hasPaidClass + "'>" + name + "</td>" +
                        "<td class='text-center'><span class='label-for-mobile'>Total</span>" + element.total + "</td>" +
                        "<td class='text-center'><span class='label-for-mobile'>Pagado</span>" + element.amountPaid + "</td>" +
                        "<td class='text-center'><span class='label-for-mobile'>Cortesía</span>" + element.totalDiscount + "</td>" +
                        "<td class='text-center'><span class='label-for-mobile'>Saldo</span>" + topay + "</td>" +
                        "<td class='text-center' style='font-size:14px'>" + settledDebt + "</td>" +
                        "<td class='text-center' hidden>" + element.email + "</td>" +
                        "<td><div style='text-align:center'><button type='button' data-item=" + position + " class='editItemList btn m-2 " + buttonColor + "'><i class='icon-attach_money'></i></button>" +
                        " <a href='/edita-perfil?email=" + element.email + "' class='btn btn-warning'><i class='icon-mode_edit'></i></a> </div>";
                });
                $(".dinamicListGuest").html(tmpListGuest);
            }

            //We serach for the titular of the account/user
            function getTitular(guests) {
                for (index in guests) {
                    if (guests[index].isOwner) {
                        return guests[index].firstName + " " + guests[index].lastName;
                    }
                }
                return null;
            }

            //List Button Action - Show Modal to edit data
            $('.dinamicListGuest').on('click', '.editItemList', function () {
                let index = $(this).data("item");
                selectedAccount = $(this).data("item");
                let account = accounts[index];
                email = $('#emailHolder').html(account.email);
                $('#addPayment').data("item", index);
                $('#amountPaid').val('');
                $("#modalPayment").modal("show");
            });

            //confirm to register payment
            $('#confirmPaymentButton').on('click', function () {
                $(".modal-footer-buttons").hide();
                $(".modal-footer-load").show();
                let account = accounts[selectedAccount];
                let amountPaid = $('#amountPaid').val();
                let paymentType = $('#paymentType').val();
                let payload = { "debtorEmail": account.email, "liasonEmail": "", "amount": amountPaid, "typeDesc": paymentType };
                sendRequestActionNoResponse('/liason/registrar-pago', payload);
                
            });


            $('#addPayment').on('click', function () {
                if (validatePayment()) {
                    $('#modalConfirmPayment').modal("show");
                    let amountPaid = $('#amountPaid').val();
                    let paymentType = $('#paymentType').val();
                    let account = accounts[selectedAccount];

                    confirmAmountHtml = $('#confirmAmountHtml').html(amountPaid);
                    confirmUserHtml = $('#confirmUserHtml').html(account.email);
                    confirmPaymentTypeHtml = $('#confirmPaymentTypeHtml').html(paymentType);
                }
            });

            // registers payment in backend via POST
            function sendRequestActionNoResponse(endpoint, payload) {
                $.ajax({
                    url: endpoint,
                    type: "POST",
                    data: JSON.stringify(payload),
                    contentType: "application/json; charset=utf-8",
                    success: function () {
                        //getAllMyPaymentCollects();
                        getSummaryAccountsByFilter('ID',"1");
                        $('#toastAction').toast('show'); 
                        $(".modal-footer-buttons").show();
                        $(".modal-footer-load").hide();
                        $('#modalConfirmPayment').modal("hide");
                        $("#modalPayment").modal("hide");
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("it failed request");
                        $(".modal-footer-buttons").show();
                        $(".modal-footer-load").hide();
                        $('#modalConfirmPayment').modal("hide");
                        $("#modalPayment").modal("hide");
                    }
                });

            }

            //retrieves all cobros by currently logged in liason
            function sendGetRequestAction(endpoint) {

                $.ajax({
                    url: endpoint,
                    type: "GET",
                    success: function (allCollected) {

                        let totalNormal = 0;
                        let totalDiscounted = 0;
                        let totalOffering = 0;
                        let totalReceived = 0;

                        for (const key in allCollected) {

                            let collect = allCollected[key];
                            if (collect.type === 'normal') {
                                totalNormal += collect.amount;
                            } else if (collect.type === 'cortesia') {
                                totalDiscounted += collect.amount;
                            } else if (collect.type === 'ofrenda') {
                                totalOffering += collect.amount;
                            }

                            totalNormalHtml = $('#totalNormal').html(totalNormal.toLocaleString());
                            totalDiscountedHtml = $('#totalDiscounted').html(totalDiscounted.toLocaleString());
                            totalOfferingHtml = $('#totalOffering').html(totalOffering.toLocaleString());
                            totalReceivedHtml = $('#totalReceived').html((totalNormal + totalDiscounted + totalOffering).toLocaleString());
                        }

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $(".response__message").addClass("error").html(thrownError);
                        $(".loading").hide();
                    }
                });

            }

            
            function downloadMyCobros() {
                //TODO create one function to do this in utilities!!!
                let fileName = 'misCobros.xlsx';
                let header = ["Pago Id", "Fecha", "Titular Cuenta", "Pago"];
                let innerData = []

                innerData.push(header);
                for (i in myCobros) {
                    let row = [];
                    elem = myCobros[i];
                    row.push(elem.paymentId);
                    row.push(elem.date);
                    row.push(elem.debtorName);
                    row.push(elem.amount);
                    innerData.push(row);
                }

                
                var filename = "mis_cobros.xlsx";                
                var ws_name = "cobros";
                
                var wb = XLSX.utils.book_new(),
                    ws = XLSX.utils.aoa_to_sheet(innerData);                
                XLSX.utils.book_append_sheet(wb, ws, ws_name);                
                XLSX.writeFile(wb, filename);


            }

            function getAllMyRecords() {

                let liasonEmail = $('#liasonEmail').val();
                $.ajax({
                    url: "/liason/trae-my-cobros?liasonEmail=" + liasonEmail,
                    type: "GET",
                    success: function (allCollected) {
                        myCobros = allCollected;

                        $("#table_id").dataTable().fnDestroy();
                        $('#table_id').DataTable({
                            buttons: [
                                'excel', 'pdf', 'print'
                            ],
                            dom: 'Bfrtip',
                            data: allCollected,
                            columns: [{ data: 'paymentId' }, { data: 'date' }, { data: 'debtorName' }, { data: 'amount' }]
                        });
                        $("#recordsList").modal("show");

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("ERROR!!! " + thrownError);
                    }
                });

            }
            
            function getAllMyPaymentCollects() {
                var liasonEmail = $('#liasonEmail').val();
                sendGetRequestAction("/liason/trae-my-cobros?liasonEmail=" + liasonEmail);
            }

            $('#downloadCobros').on('click', function () {
                downloadMyCobros();
            });
            
            window.onload = function () {
                getAllMyPaymentCollects()
                getSummaryAccountsByFilter('ID','1');
                
            }

            $('#showRecords').on('click', function () {
                getAllMyRecords();

            });      

            //Validation Function ValidatePayment
            function validatePayment() {
                var selector = $("#paymentsRegister");
                selector.validate({
                    rules: {
                        amount: {
                            required: true,
                            nospacesonly: true,
                            regex: /^\s*(?=.*[1-9])\d*(?:\.\d{1,2})?\s*$/
                        }
                    },
                    messages: {
                        amount: {
                            required: "No has ingresado una cantidad",
                            nospacesonly: "No has ingresado una cantidad",
                            regex: "No parece una cifra válida"
                        }
                    }
                });
                return selector.valid();
            }

            $(document).ready(function () {
                $("#search-by-id").on("keyup", function () {
                    var value = $(this).val().toLowerCase();
                    $("#accounts-list tr").filter(function (i, row) {
                        let td1 = $(row).find("td:nth-child(1)").text()
                        if (value === "") {
                            $(this).toggle(true)
                        } else {
                            $(this).toggle(td1.toLowerCase() === value)
                        }
                    });
                });

                $("#search-by-other").on("keyup", function () {
                    var value = $(this).val().toLowerCase();
                    $("#accounts-list tr").filter(function (i, row) {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                });

                $(window).scroll(function (event) {

                    var st = $(this).scrollTop();
                    var startPoint = $(".page__content--fixed").offset().top;

                    if (st > startPoint) {
                        $(".table__header").addClass("fixed");
                        $(".table-payments").addClass("fixed");
                    }else {
                        $(".table__header").removeClass("fixed");
                        $(".table-payments").removeClass("fixed");
                    }

                });

                $("#paymentsRegister").submit(function(e){
                    e.preventDefault();
                });
            });


            function retrieveAccountsByFilter() {
                let filter = $("#searchFilter").val();
                let value = $("#search-input").val();   
                console.log("searching by "+ filter +" = "+ value);             
                getSummaryAccountsByFilter(filter, value);
            }

            $('#search-button').on('click', function () {
                retrieveAccountsByFilter();
            });

        </script>
</body>

</html>