<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<head>
    <%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
    <jsp:include page="../layout/head-elements.jsp">
        <jsp:param name="title" value="Modificar Contraseña" />
        <jsp:param name="description" value="Ingrese la nueva contraseña" />
    </jsp:include>
</head>

<body class="login">
    <jsp:include page="../layout/header.jsp" />
    <div class="page__container">
        <div class="container">
            <div class="users__password__reset">
                <div class="page__head">
                    <h1 class="page__head__title p-4">Modificar contraseña</h1>
                </div>
                <div class="page__content bordered">
                    <div class="page__form">
                        <c:if test="${not empty errorMessage}">
                            <div class="row m-3">
                                <div class="col-sm-12">
                                    <div class="alert alert-danger" role="alert">
                                        ${errorMessage}
                                    </div>
                                </div>
                            </div>
                        </c:if>
                        <form name="passwordResetForm" id="passwordResetForm" action="/password/cambia-contrasena" method="POST">
                            <div class="row m-3">
                                <div class="col-sm-12">
                                    <h2 class="h6 p-2 colorbase1">Ingrese la nueva contraseña</h2>
                                </div>
                            </div>
                            <input type="text" hidden value=${token} name="token" />
                            <div class="row m-3">
                                <div class="col-sm-12 col-lg-6 d-flex align-items-center"><i class="icon-lock_outline"></i>
                                    <label for="" class="font-weight-bold m-0">Nueva Contraseña</label></div>
                                <div class="col-sm-12 col-lg-6"><input type="password" name="newPassword" id="password"
                                        class="form-control"></div>
                            </div>
                            <div class="row m-3">
                                <div class="col-sm-12 col-lg-6 d-flex align-items-center"><i class="icon-lock_outline"></i>
                                    <label for="" class="font-weight-bold m-0">Repetir Contraseña</label></div>
                                <div class="col-sm-12 col-lg-6"><input type="password" name="passwordConfirm" id="passwordConfirm"
                                        class="form-control"></div>
                            </div>
                            <div class="row m-3">
                                <div class="col-sm-12">
                                    <div class="d-flex justify-content-end">
                                        <button type="button" class="btn btn-primary d-flex" id="passwordResetBtn"> Guardar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <jsp:include page="../layout/footer.jsp" />
    <script>
        $(document).ready(function () {
            $("#passwordResetBtn").on("click", function () {
                if (validatePasswordResetForm()) {
                    $("#passwordResetForm").submit();
                }
            });
        });

        //Function ValidateForm
        function validatePasswordResetForm() {
            var form = $("#passwordResetForm");
            var selector = form;
            selector.validate({
                rules: {
                    newPassword: {
                        required: true,
                        nospace: true,
                        regex: /^(?=.*\d)(?=.*[#$^+=!*()@%&])./,
                        minlength: 8,
                        maxlength: 12
                    },
                    passwordConfirm: {
                        required: true,
                        equalTo: "#password"
                    }
                },
                messages: {
                    newPassword: {
                        required: "La contraseña es requerida",
                        nospace: "No se aceptan espacios",
                        minlength: "Ingrese mínimo 8 caracteres",
                        maxlength: "Ingrese máximo 12 caracteres",
                        regex: "Requiere al menos 1 número, y 1 caracter especial #$^+=!*()@%&"
                    },
                    passwordConfirm: {
                        required: "Confirmación de Contraseña es requerido",
                        equalTo: "Las contraseñas no coinciden"
                    }
                }
            });
            return form.valid();
        }

    </script>
</body>

</html>