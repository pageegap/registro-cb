<html>

<head>
    <%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
    <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
    <jsp:include page="../layout/head-elements.jsp">
        <jsp:param name="title" value="Registro de perfil" />
        <jsp:param name="description" value="Alta de información de perfil Servicios e Invitados" />
    </jsp:include>
</head>

<body>
    <jsp:include page="../layout/header.jsp" />

    <div class="col-sm-12 col-md-7"><input type="text" name="email" id="email" class="form-control"
            value="${currentUser}" hidden></div>

    <div class="page__container">
        <div class="container">
            <div class="page__head">
                <div class="page__head__title p-4"><strong>Información de registro</strong>
                    <div class="badge badge-info" style="background-color: #5b102e;float:right">
                        ID : <span id="accountIdHtml">${accountIdHtml}</span>
                    </div>
                </div>
            </div>
            <div class="page__content bordered mb-5">
                <div class="row m-3 mt-5">
                    <div class="col-sm-12">
                        <div class="alert alert-warning colorbase1" role="alert">
                            <h3><strong>Antes de empezar…</strong></h3>
                            <p>El ingreso recaudado por inscripciones y servicios será usado en su totalidad para cubrir
                                los gastos de la conferencia.</p>
                            <p><strong>Cristianismo Bíblico no persigue ningún fin de lucro.</strong></p>
                            <p><strong>En caso de cancelación por causas de la magnitud y logistica del evento
                                    no se hará devolución de los pagos ya efectuados.</strong>
                                Si por algún motivo o circunstancia <strong>NO PODRÁ ASISTIR</strong> a la conferencia y
                                ya cubrió parcial o totalmente el costo, es <strong>NECESARIO</strong> que por favor
                                <strong>NOTIFIQUE</strong> a su encargado de cobro.</p>
                        </div>
                    </div>
                </div>
                <div class="row m-3">
                    <div class="col-sm-12" id="servicePrices">
                        <h5 class="subtitle--small">Costo de servicios</h5>
                        <table class="table table-prices table-responsive">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col" class="text-center">Edad</th>
                                    <th scope="col" class="text-center"> Inscripción</th>
                                    <th scope="col" class="text-center"> Comida</th>
                                    <th scope="col" class="text-center"> Servicios de estadía</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">0-4 años</td>
                                    <td class="text-center">Gratis</td>
                                    <td class="text-center">Gratis</td>
                                    <td class="text-center">Gratis</td>
                                </tr>
                                <tr>
                                    <td class="text-center">5-10 años</td>
                                    <td class="text-center">Gratis</td>
                                    <td class="text-center">&#36; 30 c/u</td>
                                    <td class="text-center">&#36; 75</td>
                                </tr>
                                <tr>
                                    <td class="text-center">11 en adelante</td>
                                    <td class="text-center">&#36; 150</td>
                                    <td class="text-center">&#36; 60 c/u</td>
                                    <td class="text-center">&#36; 150</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="page__head" id="lista">
                <h1 class="page__head__title p-4">Mi lista de asistentes</h1>
            </div>
            <div class="page__content bordered mb-5">
                <div class="profile__guests__list">

                    <div class="row m-3 mt-5">

                        <div class="col-sm-12">
                            <div class="guest__list">
                                <table class="table table-services table-responsive">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col" class="text-center" width="10%">Relación</th>
                                            <th scope="col" class="text-center" width="5%">Edad</th>
                                            <th scope="col" class="text-center" width="25%">Nombre</th>
                                            <th scope="col" class="text-center" width="25%">Comidas</th>
                                            <th scope="col" class="text-center" width="5%">Estadía</th>
                                            <th scope="col" class="text-center" width="10%">Total</th>
                                            <th scope="col" class="text-center" width="20%"></th>
                                        </tr>
                                    </thead>
                                    <tbody class="dinamicListGuest">
                                        <!--Load dinamic -->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row m-3">
                        <div class="col-sm-12">
                            <div class="alert alert-warning colorbase1" role="alert">
                                <strong>No se registre en grupo</strong>, realícelo de manera individual a menos que asista como familia.
                            </div>
                        </div>
                    </div>
                    <div class="row m-3 mt-4">
                        <div class="col-sm-12 col-md-8">
                            <div class="guest__added">
                                <h4><span class="guest__added__counter">0</span> asistente(s) registrado(s)</h4>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="guest__add float-right" id="addGuestButtonDiv">
                                <button type="button" id="addGuestButton"
                                    class="guest__add__btn btn btn-primary d-flex flex-row"><i
                                        class="icon-person_add"></i>Añadir
                                    invitado</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="page__head">
                <h3 class="page__head__title p-4">Resumen de cuenta</h2>
            </div>
            <div class="page__content bordered mb-5">
                <div class="row m-3">
                    <div class="col-sm-12 col-md-4">
                        <div class="dashboard__card dashboard__card--fullwidth">
                            <div class="dashboard__card__title bg-primary"><i class="icon-attach_money"></i>Total
                                Acumulado</div>
                            <div class="dashboard__card__info">
                                <div id="totalDueAccount" class="h3 text-center text-success font-weight-bold">
                                    ${totalDueAccount}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="dashboard__card dashboard__card--fullwidth">
                            <div class="dashboard__card__title bg-primary"><i class="icon-attach_money"></i>Pagado
                            </div>
                            <div class="dashboard__card__info">
                                <div id="amountPaid" class="h3 text-center text-success font-weight-bold">
                                    ${amountPaid}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="dashboard__card dashboard__card--fullwidth">
                            <div class="dashboard__card__title bg-primary"><i class="icon-attach_money"></i>Adeudo
                            </div>
                            <div class="dashboard__card__info">
                                <div id="remaining" class="h3 text-center text-danger font-weight-bold">${remaining}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row m-3 mt-5">
                    <div class="col-sm-12">
                        <div class="alert alert-warning colorbase1" role="alert">
                            <strong>No está asegurado tu lugar, comida, ni estadía hasta no haber efectuado el pago total de tu
                            registro</strong>. Si solicitó servicios de estadía la habitación será asignada hasta que se haya
                            cubierto el <strong>pago total</strong> de tu registro.
                        </div>
                    </div>
                </div>
            </div>
            <div class="page__head bordered mb-6" id="contacto">
                <h3 class="page__head__title p-4">Lista de encargados de cobro</h2>
            </div>
            <div class="page__content bordered mb-5">
                    <div class="row m-3 mt-5">
                        <div class="col-sm-12">
                            <div class="alert alert-warning colorbase1" role="alert">
                                Para pagar puedes contactar a la persona encargada más cercana a ti. <strong>Ten a la mano tu número de identificación (ID de usuario) para proporcionárselo</strong>.
                            </div>
                        </div>
                    </div>
                    <div class="row m-3">
                        <div class="col-md-6 col-lg-3">
                            <div class="dashboard__card dashboard__card--fullwidth">
                                <div class="dashboard__card__title bg-primary-badge">
                                    Ciudad de México</div>
                                <div class="dashboard__card__info">
                                    <div  class="text-center  font-weight-bold">
                                            <i class="icon-face"></i> José Barrón  </br>
                                        <i class="icon-mail_outline"></i> jabm.com@live.com.mx </br>
                                        <i class="icon-phone_android"></i>55-11-42-17-54</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="dashboard__card dashboard__card--fullwidth">
                                <div class="dashboard__card__title bg-primary-badge">
                                    Coahuila
                                </div>
                                <div class="dashboard__card__info">
                                    <div  class="text-center  font-weight-bold">
                                            <i class="icon-face"></i>Aracely Zamarripa </br>
                                        <i class="icon-mail_outline"></i> aracelyzo76@gmail.com </br>                                    
                                        <i class="icon-phone_android"></i>84-43-00-24-64
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-3">
                            <div class="dashboard__card dashboard__card--fullwidth">
                                <div class="dashboard__card__title bg-primary-badge">
                                        Coahuila
                                    </div>
                                <div class="dashboard__card__info">
                                    <div  class=" text-center font-weight-bold">
                                            <i class="icon-face"></i>José Ramírez   </br>                                             
                                            <i class="icon-mail_outline"></i>josegrmzg@gmail.com </br>
                                            <i class="icon-phone_android"></i>84-48-80-46-61
                                        </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="dashboard__card dashboard__card--fullwidth">
                                <div class="dashboard__card__title bg-primary-badge">
                                    Estado de México
                                </div>
                                <div class="dashboard__card__info">
                                    <div class="text-center  font-weight-bold">                                        
                                            <i class="icon-face"></i>Víctor Velasco</br>
                                        <i class="icon-mail_outline"></i> vm_vel@hotmail.com</br>
                                        <i class="icon-phone_android"></i>72-24-31-26-99
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row m-3">
                        <div class="col-md-6 col-lg-3">
                            <div class="dashboard__card dashboard__card--fullwidth">
                                <div class="dashboard__card__title bg-primary-badge">
                                    Jalisco
                                </div>
                                <div class="dashboard__card__info">
                                    <div  class="text-center  font-weight-bold">
                                            <i class="icon-face"></i>Daniel López<br>
                                        <i class="icon-mail_outline"></i> dnz053q@gmail.com</br>
                                        <i class="icon-phone_android"></i>33-15-60-86-08
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="dashboard__card dashboard__card--fullwidth">
                                <div class="dashboard__card__title bg-primary-badge">
                                    Querétaro
                                </div>
                                <div class="dashboard__card__info">
                                    <div  class="text-center  font-weight-bold">
                                            <i class="icon-face"></i> Gabriela Fattel  </br>
                                        <i class="icon-mail_outline"></i> gabriela.fattel@gmail.com</br>
                                        <i class="icon-phone_android"></i>44-28-08-71-18
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <!-- Modal Guest-->
    <div class="modal fade" id="modalGuest" tabindex="-1" role="dialog" aria-labelledby="modalGuestLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title subtitle--small" id="modalGuestLabel">Información del asistente</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form name="profileGuestForm" id="profileGuestForm" action="" method="POST">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row m-2">
                                    <div class="col-sm-3 col-md-3 text-right"><label for=""
                                            class="font-weight-bold">Nombre(s)</label></div>
                                    <div class="col-sm-9 col-md-9"><input type="text" name="guestFirstName"
                                            class="form-control" id="guestFirstName"></div>

                                </div>
                                <div class="row m-2">
                                    <div class="col-sm-3 col-md-3 text-right"><label for=""
                                            class="font-weight-bold">Apellido(s)</label></div>
                                    <div class="col-sm-9 col-md-9"><input type="text" name="guestLastName"
                                            class="form-control" id="guestLastName"></div>
                                </div>
                                <hr>
                                <div id="ownerOnlyDiv">
                                    <div class="row m-2">
                                        <div class="col-sm-3 col-md-3 text-right"><label for=""
                                                class="font-weight-bold">Teléfono</label></div>
                                        <div class="col-sm-9 col-md-9"><input type="text" name="phoneNumber"
                                                class="form-control" id="phoneNumber"></div>
                                    </div>
                                    <div class="row m-2">
                                        <div class="col-sm-3 col-md-3 text-right"><label for=""
                                                class="font-weight-bold">País</label></div>
                                        <div class="col-sm-9 col-md-9">
                                            <select name="country" id="guestCountry" class="select__relation">
                                                <option value="País"> País</option>
                                                <option value="MÉXICO">México</option>
                                                <option value="ESTADOS UNIDOS">Estados Unidos</option>
                                                <option value="COLOMBIA">Colombia</option>
                                                <option value="OTRO">Otro</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row m-2">
                                        <div class="col-sm-3 col-md-3 text-right"><label for=""
                                                class="font-weight-bold">Estado</label></div>
                                        <div class="col-sm-9 col-md-9">
                                            <select name="state" id="guestState" class="select__relation">
                                                <option value="Estado"> Estado</option>
                                                <option value="AGUASCALIENTES">Aguascalientes</option>
                                                <option value="BAJA CALIFORNIA">Baja California</option>
                                                <option value="BAJA CALIFORNIA SUR">Baja California Sur</option>
                                                <option value="CHIHUAHUA">Chihuahua</option>
                                                <option value="COAHUILA">Coahuila</option>
                                                <option value="COLIMA">Colima</option>
                                                <option value="Durango">Durango</option>
                                                <option value="ESTADO DE MÉXICO">Estado de México</option>
                                                <option value="GUERRERO">Guerrero</option>
                                                <option value="HIDALGO">Hidalgo</option>
                                                <option value="JALISCO">Jalisco</option>
                                                <option value="MICHOACÁN">Michoacán</option>
                                                <option value="MORELOS">Morelos</option>
                                                <option value="NAYARIT">Nayarit</option>
                                                <option value="NUEVO LEÓN">Nuevo León</option>
                                                <option value="OAXACA">Oaxaca</option>
                                                <option value="PUEBLA">Puebla</option>
                                                <option value="QUERÉTARO">Querétaro</option>
                                                <option value="QUINTANA ROO">Quintana Roo</option>
                                                <option value="SAN LUIS POTOSÍ">San Luis Potosí</option>
                                                <option value="SINALOA">Sinaloa</option>
                                                <option value="SONORA">Sonora</option>
                                                <option value="TABASCO">Tabasco</option>
                                                <option value="TAMAULIPAS">Tamaulipas</option>
                                                <option value="TLAXCALA">Tlaxcala</option>
                                                <option value="VERACRUZ">Veracruz</option>
                                                <option value="YUCATÁN">Yucatán</option>
                                                <option value="ZACATECAS">Zacatecas</option>
                                                <option value="OTRO">Otro</option>
                                            </select>
                                        </div>


                                    </div>

                                    <div class="row m-2">
                                        <div class="col-sm-3 col-md-3 text-right"><label for=""
                                                class="font-weight-bold">Ciudad</label></div>
                                        <div class="col-sm-9 col-md-9"><input list="cities" type="text" name="city"
                                                class="form-control" id="guestCity">
                                            <datalist id="cities">
                                                <option>D.F.</option>
                                                <option>Guadalajara</option>
                                                <option>Querétaro</option>
                                                <option>Toluca</option>
                                                <option>Monterrey</option>
                                                <option>Saltillo</option>
                                                <option>Tepic</option>
                                                <option>Otro</option>
                                            </datalist>
                                        </div>
                                    </div>
                                    <div class="row m-2">
                                        <div class="col-sm-3 col-md-3 text-right"><label for=""
                                                class="font-weight-bold">Iglesia</label></div>
                                        <div class="col-sm-9 col-md-9">
                                            <select name="church" id="guestChurch" class="select__relation">
                                                <option value="Iglesia">Iglesia</option>
                                                <option value="IGLESIA DEL CENTRO">Iglesia del Centro</option>
                                                <option value="COMUNIDAD BÍBLICA DE LA GRACIA">Comunidad Bíblica de la Gracia</option>
                                                <option value="GOZO SOBERANO">Gozo Soberano</option>
                                                <option value="IGLESIA BÍBLICA SÓLO POR GRACIA">Iglesia Bíblica Sólo por Gracia (Toluca)</option>
                                                <option value="IGLESIA BÍBLICA POR GRACIA"> Iglesia Bíblica por Gracia (Saltillo)</option>
                                                <option value="IGLESIA BÍBLICA GRACIA SALVADORA"> Iglesia Bíblica Gracia Salvadora (Monterrey)</option>
                                                <option value="OTRA">Otra</option>
                                            </select>
                                        </div>

                                    </div>
                                    <hr>
                                </div>

                                <div class="row m-2" id="parentescoDiv">
                                    <div class="col-sm-3 col-md-3 text-right"><label for=""
                                            class="font-weight-bold">Parentesco</label></div>
                                    <div class="col-sm-9 col-md-9">
                                        <input type="hidden" name="currentRelation" value="0" id="currentRelation" />
                                        <select name="guestRelation" class="select__small" id="guestRelation">
                                            <option value="0">Selecciona</option>
                                            <option value="Esposa">Esposa</option>
                                            <option value="Esposo">Esposo</option>
                                            <option value="Hijo">Hijo</option>
                                            <option value="Hija">Hija</option>
                                            <option value="Padre">Padre</option>
                                            <option value="Madre">Madre</option>
                                            <option value="Hermano">Hermano</option>
                                            <option value="Hermana">Hermana</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row m-2">
                                    <div class="col-sm-3 col-md-3 text-right"><label for=""
                                            class="font-weight-bold">Sexo</label></div>
                                    <div class="col-sm-9 col-md-9">
                                        <select name="guestGender" class="select__small" id="guestGender">
                                            <option value="0">Selecciona</option>
                                            <option value="h">Hombre</option>
                                            <option value="m">Mujer</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row m-2">
                                    <div class="col-sm-3 col-md-3 text-right"><label for=""
                                            class="font-weight-bold">Edad</label></div>
                                    <div class="col-sm-9 col-md-9"><input type="text" name="guestAge"
                                            class="form-control input-text__small" id="guestAge"></div>
                                </div>
                                <div class="row m-2">


                                    <div class="col-sm-3 col-md-3 text-right"><label for=""
                                            class="font-weight-bold">Edo. civil</label></div>
                                    <div class="col-sm-9 col-md-9">
                                        <select name="maritalStatus" class="select__small" id="guestMaritalStatus">
                                            <option value="0">Selecciona</option>
                                            <option value="soltero">Soltero(a)</option>
                                            <option value="casado">Casado(a) </option>
                                            <option value="otro">Otro </option>
                                        </select>
                                    </div>
                                </div>
                                <div id="selectors__errors__label" class="response__error text-center"></div>
                                <hr>
                                <div class="row m-2">
                                    <div class="col-sm-7 col-md-7 text-right"><label for=""
                                            class="font-weight-bold">¿Padece una discapacidad?</label></div>
                                    <div class="col-sm-5 col-md-5 d-flex flex-row ">
                                        <div class="radio-group">
                                            <input type="radio" name="guestHasDisability" value="0" checked>
                                            <label for="">No</label>
                                        </div>
                                        <div class="radio-group">
                                            <input type="radio" name="guestHasDisability" value="1">
                                            <label for="">Si</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row m-2">
                                    <div class="col-sm-7 col-md-7 text-right"><label for=""
                                            class="font-weight-bold">¿Requiere medicación?</label></div>
                                    <div class="col-sm-5 col-md-5 d-flex flex-row ">
                                        <div class="radio-group">
                                            <input type="radio" name="guestIsMedicated" value="0" checked>
                                            <label for="">No</label>
                                        </div>
                                        <div class="radio-group">
                                            <input type="radio" name="guestIsMedicated" value="1">
                                            <label for="">Sí</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio__guest__ispregnant d-none">
                                    <div class="row m-2">
                                        <div class="col-sm-7 col-md-7 text-right"><label for=""
                                                class="font-weight-bold">¿Está embarazada?</label></div>
                                        <div class="col-sm-5 col-md-5 d-flex flex-row ">
                                            <div class="radio-group">
                                                <input type="radio" name="guestIsPregnant" value="0" checked>
                                                <label for="">No</label>
                                            </div>
                                            <div class="radio-group">
                                                <input type="radio" name="guestIsPregnant" value="1">
                                                <label for="">Si</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row m-3">
                                    <div class="col-sm-3 col-md-3 text-right"><label for=""
                                            class="font-weight-bold">Comentarios Relevantes</label></div>
                                    <div class="col-sm-9 col-md-9">
                                        <textarea name="guestNote" id="guestNote" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-12" id="guestServicesDiv">
                                <h5 class="subtitle--small">Seleccione sus servicios
                                    ${param.guest_number}</h5>
                                <table class="table table-services table-responsive">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col" class="text-center" colspan="8">Comidas </th>
                                        </tr>
                                    </thead>
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col" class="text-center">Vie</th>
                                            <th scope="col" class="text-center" colspan="3">Sáb</th>
                                            <th scope="col" class="text-center" colspan="3">Dom</th>
                                            <th scope="col" class="text-center">Lun</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center"><input type="checkbox" name="guestFoodFriday1"
                                                    id="guestFoodFriday1"><br>Cena</td>
                                            <td class="text-center"><input type="checkbox" name="guestFoodSaturday1"
                                                    id="guestFoodSaturday1"><br>Des.</td>
                                            <td class="text-center"><input type="checkbox" name="guestFoodSaturday2"
                                                    id="guestFoodSaturday2"><br>Com.</td>
                                            <td class="text-center"><input type="checkbox" name="guestFoodSaturday3"
                                                    id="guestFoodSaturday3"><br>Cena</td>
                                            <td class="text-center"><input type="checkbox" name="guestFoodSunday1"
                                                    id="guestFoodSunday1"><br>Des.</td>
                                            <td class="text-center"><input type="checkbox" name="guestFoodSunday2"
                                                    id="guestFoodSunday2"><br>Com.</td>
                                            <td class="text-center"><input type="checkbox" name="guestFoodSunday3"
                                                    id="guestFoodSunday3"><br>Cena</td>
                                            <td class="text-center"><input type="checkbox" name="guestFoodMonday1"
                                                    id="guestFoodMonday1"><br>Des</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="table table-services table-responsive" style="margin-bottom: 0px;">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col" class="text-center">Servicios de estadía</th>
                                            <th scope="col" class="text-center">Inscripción</th>
                                            <!--<th scope="col" class="text-center" colspan="3">Transporte</th>-->
                                        </tr>
                                    </thead>
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col" class="text-center">3 noches</th>
                                            <th scope="col" class="text-center"></th>
                                            <!--<th scope="col" class="text-center">Vie</th>
                                            <th scope="col" class="text-center">Sab</th>
                                            <th scope="col" class="text-center">Lun</th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center" class="text-center"><input type="checkbox"
                                                    name="guestLodging" id="guestLodging"><br>Vie-Lun</td>
                                            <td class="text-center" class="text-center"><input type="checkbox"
                                                    name="guestRegistration" checked disabled
                                                    id="guestRegistration"><br>Vie-Lun</td>
                                            </td>
                                            <!-- <td class="text-center" class="text-center"><input type="checkbox"
                                                    name="guestTransportationFriday"
                                                    id="guestTransportationFriday"><br>Ida</td>
                                            <td class="text-center" class="text-center"><input type="checkbox"
                                                    name="guestTransportationSaturday"
                                                    id="guestTransportationSaturday"><br>Ida</td>
                                            <td class="text-center" class="text-center"><input type="checkbox"
                                                    name="guestTransportationMonday"
                                                    id="guestTransportationMonday"><br>Vuelta</td>-->
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="modal__response__message"></div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary d-flex" id="createGuest"><i
                            class="icon-person_add"></i> Añadir a mi lista</button>
                    <button type="button" class="btn btn-primary d-none" id="editGuest"><i class="icon-done"></i>
                        Actualizar registro</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modalConfirmDelete" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Eliminar registro</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>¿Esta seguro de eliminar de su lista a <strong><span
                                class="guest__deleted__name"></span></strong>?.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" id="guestDelete">Eliminar</button>
                </div>
            </div>
        </div>
    </div>

    <jsp:include page="../layout/footer.jsp" />

    <script>

        //Declare Global variables
        let guests = [];
        let totalRemain = 10000;
        let paidDisc = 0;
        let accountId = -100;
        let phoneNumberAccount = "";
        let totalDueAccount = $('#totalDueAccount').html("0.0");
        let amountPaid = $('#amountPaid').html("0.0");
        let remaining = $('#remaining').html("0.0");
        let accountIdHtml = $('#accountIdHtml').html("");
        let city = "";
        let country = "";
        let church = "";
        let state = "";
        let email = "";
        let isWifeRegister = false;
        let isHusbandRegister = false;
        let isFatherRegister = false;
        let isMotherRegister = false;

        // when page loads
        window.onload = function () {
            let email = $('#email').val();
            getAllGuests(email);
        }

        function getAllGuests(email) {
            $.ajax({
                url: "/recupera-invitados?email=" + email,
                type: "GET",
                success: function (response) {
                    setUpGlobalVars(response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $(".response__message").addClass("error").html(thrownError);
                    $(".loading").hide();
                }
            });

        }
        function setUpGlobalVars(response) {
            guests = []; // clear old list of guests

            for (const key in response.guests) {
                guests.push(response.guests[key]);
            }

            accountIdHtml = $("#accountIdHtml").html(response.userId);
            totalDueAccount = $('#totalDueAccount').html(response.total);
            amountPaid = $('#amountPaid').html(response.amountPaid);
            remaining = $('#remaining').html(response.total - (response.amountPaid + response.totalDiscount));

            paidDisc = response.amountPaid + response.totalDiscount
            totalRemain = response.total - paidDisc;

            if (totalRemain <= 0 && paidDisc > 0) {//paid something    
                $("#addGuestButtonDiv").hide();
            }

            email = response.email;
            city = response.city;
            country = response.country;
            church = response.church;
            state = response.state;
            accountId = response.userId;
            phoneNumberAccount = response.phoneNumber;

            setListGuest();
        }



        //prints Guests in List
        function setListGuest() {
            var tmpListGuest = "";
            isWifeRegister = false;
            isHusbandRegister = false;
            isFatherRegister = false;
            isMotherRegister = false;
            guests.forEach(function (element, position) {

                lodging = element.lodging ? "<i class='icon-location_city'></i><span class='badge badge-primary'><i class='icon-done'></i></span>" : "";
                food1 = element.foodFriday1 ? "V<span class='badge badge-primary'>1</span>" : "V<span class='badge badge-danger'>0</span>";
                food4 = element.foodMonday1 ? "L<span class='badge badge-primary'>1</span>" : "L<span class='badge badge-danger'>0</span>";
                sumFoodSaturday = 0;
                sumFoodSunday = 0;
                sumFoodSaturday = element.foodSaturday1 ? sumFoodSaturday + 1 : sumFoodSaturday;
                sumFoodSaturday = element.foodSaturday2 ? sumFoodSaturday + 1 : sumFoodSaturday;
                sumFoodSaturday = element.foodSaturday3 ? sumFoodSaturday + 1 : sumFoodSaturday;
                sumFoodSunday = element.foodSunday1 ? sumFoodSunday + 1 : sumFoodSunday;
                sumFoodSunday = element.foodSunday2 ? sumFoodSunday + 1 : sumFoodSunday;
                sumFoodSunday = element.foodSunday3 ? sumFoodSunday + 1 : sumFoodSunday;

                food2 = sumFoodSaturday > 0 ? "S<span class='badge badge-primary'>" + sumFoodSaturday + "</span>" : "S<span class='badge badge-danger'>0</span>";
                food3 = sumFoodSunday > 0 ? "D<span class='badge badge-primary'>" + sumFoodSunday + "</span>" : "D<span class='badge badge-danger'>0</span>";

                let editButtons = (element.isOwner == true || (totalRemain <= 0 && paidDisc > 0)) ? "<td><button type='button' data-item=" + position + " class='editItemList btn btn-warning m-2'><i class='icon-mode_edit'></i></button>" :
                    "<td><button type='button' data-item=" + position + " class='editItemList btn btn-warning m-2'><i class='icon-mode_edit'></i></button><button type='button' data-item=" + position + " class='removeItemList btn btn-danger m-2'><i class='icon-delete'></i></button></td>";

                tmpListGuest = tmpListGuest + "<tr><td class='text-uppercase'><strong>" + element.relation + "</strong></td> " +
                    "<td class='text-center list-guest-font'>" + element.age + "</td>" +
                    "<td>" + element.firstName + " " + element.lastName + "</td>" +
                    "<td class='text-center'><div class='d-flex justify-content-center align-items-center'><i class='icon-spoon-knife'></i>&nbsp;&nbsp;<div class='item__service'>" + food1 + "</div><div class='item__service'>" + food2 + "</div><div class='item__service'>" + food3 + "</div><div class='item__service'>" + food4 + "</div></div></td> " +
                    "<td class='text-center'><div class='item__service'>" + lodging + "</div></td><td class='text-center'>" + element.total + "</td>" +
                    editButtons;

                //Relation Unique Counter
                if( element.relation == 'Esposa'){
                    isWifeRegister = true;
                } 
                if( element.relation == 'Esposo'){
                    isHusbandRegister = true;
                }
                if( element.relation == 'Padre'){
                    isFatherRegister = true;
                }
                if( element.relation == 'Madre'){
                    isMotherRegister = true;
                }
            });

            $(".dinamicListGuest").html(tmpListGuest);
            $(".guest__added__counter").html(guests.length);
        }


        $(document).ready(function () {

            //Clean Guest Form when close modal
            $('#modalGuest').on('hide.bs.modal', function (e) {
                clearGuest();
            });


            //Active Deactive radio guestIsPregnant
            $('#guestGender').on('change', function () {
                if (this.value == 'm') {
                    $(".radio__guest__ispregnant").removeClass('d-none').addClass('d-block');
                } else {
                    $(".radio__guest__ispregnant").removeClass('d-block').addClass('d-none');
                    $("input[name='guestIsPregnant'][value=0]").prop('checked', true);
                }
            });

            //Modal Button Action - Add Guest to List Javascript
            $('#createGuest').on('click', function () {
                if (validateAddGuest()) {
                    let guest = getModalForm();
                    sendRequestAction(guest, "addGuest");
                    $("#modalGuest").modal("hide");
                    clearGuest();
                } else {
                    $(".modal__response__message").addClass("error").html("¡Algunos de los campos requeridos no se han completado! Verifique sus datos nuevamente.");
                }
            });

            $('#addGuestButton').on('click', function () {
                $('#ownerOnlyDiv').hide();
                $("#modalGuest").modal("show");
                $('#parentescoDiv').show();
            });

            //List Button Action - Show Modal to edit data
            $('.dinamicListGuest').on('click', '.editItemList', function () {
                $("#createGuest").removeClass("d-flex").addClass("d-none");
                $("#editGuest").removeClass("d-none").addClass("d-flex");
                $("#editGuest").data("item", $(this).data("item"));
                let index = $(this).data("item");
                let guest = guests[index];
                // guest vs owner
                if (!guest.isOwner) {
                    $('#ownerOnlyDiv').hide();
                    $('#parentescoDiv').show();
                } else {
                    $('#ownerOnlyDiv').show();
                    $('#parentescoDiv').hide();
                }
                // enable vs disable editing
                if (totalRemain <= 0 && paidDisc > 0) {//no more changes
                    $("#guestServicesDiv").hide();
                    $("#guestAge").prop("disabled", true); // changes price
                } else {
                    $("#guestServicesDiv").show();
                    $("#guestAge").prop("disabled", false); //if previously disabled
                }

                setGuestToModalForm(guest);

                $("#modalGuest").modal("show");
            });

            //Modal Button Action - Edit guest info in List Javascript
            $('#editGuest').on('click', function () {
                if (validateAddGuest()) {
                    let index = $("#editGuest").data("item");
                    let guest = getModalForm();
                    guest.guestId = guests[index].guestId;
                    // is this the owner?                    
                    guest["owner"] = guests[index].isOwner
                    sendRequestAction(guest, "updateGuest");
                    $("#modalGuest").modal("hide");
                    clearGuest();
                } else {
                    $(".modal__response__message").addClass("error").html("¡Algunos de los campos requeridos no se han completado! Verifique sus datos nuevamente.");
                }
            });

            //List Button Action - Show Modal to remove Guest
            $('.dinamicListGuest').on('click', '.removeItemList', function () {
                let index = $(this).data("item");
                let guest = guests[index];
                $("#modalConfirmDelete").find('.guest__deleted__name').html(guest.firstName + " " + guest.lastName);
                $("#modalConfirmDelete").find('#guestDelete').attr("data-item", $(this).data("item"));
                $("#modalConfirmDelete").modal("show");
            });

            //Modal Button Action - Delete guest in List Javascript
            $('#guestDelete').on('click', function () {
                let index = $(this).data("item");
                let guest = guests[index];
                console.log("index, guest", index, guest);
                $(".loading").show();
                deleteGuest(guest.guestId);
                $("#modalConfirmDelete").modal("hide");
            });



            //Function Clear Data in Modal
            function clearGuest() {
                $("#modalGuest").find('input[type=text]').val('');
                $("#modalGuest").find('input:radio[value=0]').prop("checked", true);
                $("#modalGuest").find('input[type=checkbox]').prop("checked", false);
                $("#modalGuest").find('select > option').removeAttr("selected");
                $("#modalGuest").find('textarea').val('');
                $("#createGuest").removeClass("d-none").addClass("d-flex");
                $("#editGuest").removeClass("d-flex").addClass("d-none");
                $("#editGuest").removeAttr("data-item");
                $("#modalGuest").find("label.error").remove();
                $("#phoneNumber").val('');
                $("#modalGuest").find(".modal__response__message").removeClass("error").html("");
                $("#modalGuest").find('#guestRegistration').prop("checked", true);
                $("#selectors__errors__label").html('');
                $('#currentRelation').val(0);
            }

            //Function Get Data in Modal
            function getModalForm() {
                let object = {
                    type: "guest",
                    firstName: $('#guestFirstName').val(),
                    lastName: $('#guestLastName').val(),
                    phoneNumber: $('#phoneNumber').val(),
                    relation: $('#guestRelation option:selected').val(),
                    gender: $("#guestGender option:selected").val(),
                    age: $('#guestAge').val(),
                    hasDisability: $("input[name='guestHasDisability']:checked").val() === "1" ? true : false,
                    isMedicated: $("input[name='guestIsMedicated']:checked").val() === "1" ? true : false,
                    pregnant: $("input[name='guestIsPregnant']:checked").val() === "1" ? true : false,
                    note: $('#guestNote').val(),
                    foodFriday1: $('#guestFoodFriday1').is(':checked') ? true : false,
                    foodSaturday1: $('#guestFoodSaturday1').is(':checked') ? true : false,
                    foodSaturday2: $('#guestFoodSaturday2').is(':checked') ? true : false,
                    foodSaturday3: $('#guestFoodSaturday3').is(':checked') ? true : false,
                    foodSunday1: $('#guestFoodSunday1').is(':checked') ? true : false,
                    foodSunday2: $('#guestFoodSunday2').is(':checked') ? true : false,
                    foodSunday3: $('#guestFoodSunday3').is(':checked') ? true : false,
                    foodMonday1: $('#guestFoodMonday1').is(':checked') ? true : false,
                    lodging: $('#guestLodging').is(':checked') ? true : false,
                    transportationFriday: $('#guestTransportationFriday').is(':checked') ? true : false,
                    transportationSaturday: $('#guestTransportationSaturday').is(':checked') ? true : false,
                    transportationMonday: $('#guestTransportationMonday').is(':checked') ? true : false,
                    isOwner: true,
                    ownerEmail: email,
                    guestId: "",
                    maritalStatus: $('#guestMaritalStatus option:selected').val(),
                    // in case it is the owner's account
                    city: $('#guestCity').val(),
                    state: $('#guestState').val(),
                    country: $('#guestCountry').val(),
                    church: $('#guestChurch').val(),
                };

                return object;
            }

            //Function Set Data in Modal
            function setGuestToModalForm(guest) {
                //TODO validar guest?
                if (!guest) {
                    return;
                }

                $('#guestFirstName').val(guest.firstName);
                $('#guestLastName').val(guest.lastName);
                $('#guestPhoneNumber').val(guest.phoneNumber);
                $('#currentRelation').val(guest.relation);
                $('#guestRelation').val(guest.relation).trigger('change');
                $('#guestGender').val(guest.gender).trigger('change');
                $('#guestMaritalStatus').val(guest.maritalStatus).trigger('change');
                $('#guestAge').val(guest.age);
                $("input[name='guestHasDisability'][value=" + (guest.hasDisability ? "1" : "0") + "]").prop('checked', true);
                $("input[name='guestIsMedicated'][value=" + (guest.isMedicated ? "1" : "0") + "]").prop('checked', true);
                $("input[name='guestIsPregnant'][value=" + (guest.isPregnant ? "1" : "0") + "]").prop('checked', true);
                $("#guestNote").val(guest.note);
                guest.foodFriday1 == 1 ? $('#guestFoodFriday1').prop('checked', true) : $('#guestFoodFriday1').prop('checked', false);
                guest.foodSaturday1 == 1 ? $('#guestFoodSaturday1').prop('checked', true) : $('#guestFoodSaturday1').prop('checked', false);
                guest.foodSaturday2 == 1 ? $('#guestFoodSaturday2').prop('checked', true) : $('#guestFoodSaturday2').prop('checked', false);
                guest.foodSaturday3 == 1 ? $('#guestFoodSaturday3').prop('checked', true) : $('#guestFoodSaturday3').prop('checked', false);
                guest.foodSunday1 == 1 ? $('#guestFoodSunday1').prop('checked', true) : $('#guestFoodSunday1').prop('checked', false);
                guest.foodSunday2 == 1 ? $('#guestFoodSunday2').prop('checked', true) : $('#guestFoodSunday2').prop('checked', false);
                guest.foodSunday3 == 1 ? $('#guestFoodSunday3').prop('checked', true) : $('#guestFoodSunday3').prop('checked', false);
                guest.foodMonday1 == 1 ? $('#guestFoodMonday1').prop('checked', true) : $('#guestFoodMonday1').prop('checked', false);
                guest.lodging == 1 ? $('#guestLodging').prop('checked', true) : $('#guestLodging').prop('checked', false);
                guest.transportationFriday == 1 ? $('#guestTransportationFriday').prop('checked', true) : $('#guestTransportationFriday').prop('checked', false);
                guest.transportationSaturday == 1 ? $('#guestTransportationSaturday').prop('checked', true) : $('#guestTransportationSaturday').prop('checked', false);
                guest.transportationMonday == 1 ? $('#guestTransportationMonday').prop('checked', true) : $('#guestTransportationMonday').prop('checked', false);

                $('#guestCity').val(city);
                $('#guestCountry').val(country);
                $('#guestChurch').val(church);
                $('#guestState').val(state);
                $('#phoneNumber').val(phoneNumberAccount);
            }



            //Function ValidateFormGuest
            function validateAddGuest() {
                var form = $("#profileGuestForm");
                var selector = form;
                var validSelectors = true;
                selector.validate({
                    rules: {
                        guestFirstName: {
                            required: true, nospacesonly: true
                        },
                        guestLastName: {
                            required: true, nospacesonly: true
                        },
                        guestAge: {
                            required: true, nospacesonly: true, number: true, maxlength: 2
                        },
                        city: {
                            required: true, nospacesonly: true
                        },
                        country: {
                            required: true, nospacesonly: true,
                            valueNotEquals: "País"
                        },
                        state: {
                            required: true,
                            nospacesonly: true,
                            valueNotEquals: "Estado"
                        },
                        church: {
                            required: true,
                            nospacesonly: true,
                            valueNotEquals: "Iglesia"
                        },
                        guestGender: { valueNotEquals: "0" },
                        guestRelation: { valueNotEquals: "0" },
                        maritalStatus: { valueNotEquals: "0" },
                        phoneNumber: { required: true }
                    },
                    messages: {
                        guestFirstName: {
                            required: "Este campo es requerido", nospacesonly: "Ingrese información válida"
                        },
                        guestLastName: {
                            required: "Este campo es requerido", nospacesonly: "Ingrese información válida"
                        },
                        guestAge: {
                            required: "Este campo es requerido", nospacesonly: "Ingrese información válida", number: "Ingrese sólo números", maxlength: "Ingrese un rango de edad válido"
                        },
                        city: {
                            required: "Este campo es requerido", nospacesonly: "Ingrese un rango de edad válido"
                        },
                        country: {
                            required: "Este campo es requerido", nospacesonly: "Ingrese un rango de edad válido", valueNotEquals: "Seleccione País"
                        },
                        state: {
                            required: "Estado es requerido",
                            nospacesonly: "Sólo espacios no es permitido",
                            valueNotEquals: "Seleccione Estado"
                        },
                        church: {
                            required: "Iglesia es requerida",
                            nospacesonly: "Sólo espacios no es permitido",
                            valueNotEquals: "Seleccione Iglesia"
                        },
                        guestGender: { valueNotEquals: "Este campo es requerido" },
                        guestRelation: { valueNotEquals: "Este campo es requerido" },
                        maritalStatus: { valueNotEquals: "Este campo es requerido" },
                        phoneNumber: { required: "Telefono es requerido" }
                    }
                });

                validSelectors = isValidSelectors();

                return form.valid() && validSelectors;
            }

            function deleteGuest(guestId) {
                $.ajax({
                    url: guestId,
                    type: 'DELETE',
                    success: function (result) {
                        if (result.email) {
                            getAllGuests(result.email);
                        }
                    }
                });

            }


            function sendRequestAction(payload, endpoint) {

                $.ajax({
                    url: endpoint,
                    type: "POST",
                    data: JSON.stringify(payload),
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    success: function (json) {
                        console.log("it worked! ", json);

                        getAllGuests(json.email);
                        if (json.type == 'error') {
                            $(".response__message").addClass("error").html(json.message);
                        } else {
                            if (json.type == 'success') {
                            }
                        }
                        $(".loading").hide();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $(".response__message").addClass("error").html(thrownError);
                        $(".loading").hide();
                    }
                });

            }

            //Selectors Validation
            $('#guestRelation').on('change', function(){

                $("#selectors__errors__label").html("");
                let relationSelected = this.value;

                if(relationSelected== 'Esposa' || relationSelected== 'Madre' || relationSelected== 'Hija' || relationSelected== 'Hermana'){
                    $("#guestGender option[value=m]").prop("selected", true);
                } 

                if(relationSelected== 'Esposo' || relationSelected== 'Padre' || relationSelected== 'Hijo' || relationSelected== 'Hermano'){
                    $("#guestGender option[value=h]").prop("selected", true);
                } 

                if(relationSelected== 'Esposo' || relationSelected== 'Esposa'){
                    $("#guestMaritalStatus option[value=casado]").prop("selected", true);
                }else{
                    $("#guestMaritalStatus option[value=0]").prop("selected", true);
                }
            });

            function isValidSelectors(){
                let guestRelation = $("#currentRelation").val();
                let relationSelected = $("#guestRelation option:selected").val();
                let genderSelected = $("#guestGender option:selected").val();
                let ageSelected = $("#guestAge").val();
                let maritalStatusSelected = $("#guestMaritalStatus option:selected").val();

                let messageErrors = "";
                let isValid = true;

                if( (relationSelected== 'Esposa' && isWifeRegister) &&  guestRelation!='Esposa'){
                    isValid = false;
                    $("#selectors__errors__label").html("Ya se encuentra un registro con este <strong>Parentesco</strong> y no puede repetirse");
                    return isValid;
                }
                if( (relationSelected== 'Esposo' && isHusbandRegister) &&  guestRelation!='Esposo'){
                    isValid = false;
                    $("#selectors__errors__label").html("Ya se encuentra un registro con este <strong>Parentesco</strong> y no puede repetirse");
                    return isValid;
                }
                if( (relationSelected== 'Padre' && isFatherRegister) &&  guestRelation!='Padre'){
                    isValid = false;
                    $("#selectors__errors__label").html("Ya se encuentra un registro con este <strong>Parentesco</strong> y no puede repetirse");
                    return isValid;
                }
                if( (relationSelected== 'Madre' && isMotherRegister) &&  guestRelation!='Madre'){
                    isValid = false;
                    $("#selectors__errors__label").html("Ya se encuentra un registro con este <strong>Parentesco</strong> y no puede repetirse");
                    return isValid;
                }
                if( (relationSelected== 'Esposa' && isHusbandRegister) || (relationSelected== 'Esposo' && isWifeRegister) ){
                    isValid = false;
                    $("#selectors__errors__label").html("No parece coherente registrar <strong>Esposa y Esposo</strong> en un mismo registro.");
                    return isValid;
                }

                if( (relationSelected== 'Esposa' || relationSelected== 'Madre' || relationSelected== 'Hija' || relationSelected== 'Hermana') && genderSelected == 'h'){
                    messageErrors = "Tu información de <strong>Parentesco</strong> y <strong>Sexo</strong> no parecen coherentes<br>"
                    isValid = false;
                }
                if( (relationSelected== 'Esposo' || relationSelected== 'Padre' || relationSelected== 'Hijo' || relationSelected== 'Hermano') && genderSelected == 'm'){
                    messageErrors = "Tu información de <strong>Parentesco</strong> y <strong>Sexo</strong> no parecen coherentes<br>"
                    isValid = false;
                }

                if( (relationSelected== 'Esposo' || relationSelected== 'Esposa') && maritalStatusSelected != 'casado'){
                    messageErrors = messageErrors + "Tu información de <strong>Parentesco</strong> y <strong>Edo. Civil</strong> no parecen coherentes<br>"
                    isValid = false;
                }

                if( (relationSelected== 'Esposo' || relationSelected== 'Esposa' || relationSelected== 'Padre' || relationSelected== 'Madre') && ageSelected < 16){
                    messageErrors = messageErrors + "Tu información de <strong>Parentesco</strong> y <strong>Edad</strong> no parecen coherentes<br>"
                    isValid = false;
                } 

                $("#selectors__errors__label").html(messageErrors);

                return isValid;
            }

        });
    </script>
</body>

</html>