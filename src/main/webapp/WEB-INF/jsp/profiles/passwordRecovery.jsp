<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<head>
    <%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
    <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
    <jsp:include page="../layout/head-elements.jsp">
        <jsp:param name="title" value="Ingreso" />
        <jsp:param name="description" value="Ingresa tus datos para ingresar" />
    </jsp:include>
</head>

<body class="login">
    <jsp:include page="../layout/header.jsp" />
    <div class="page__container">
        <div class="container">
            <div class="page__head">
                <h1 class="page__head__title p-4">Recuperar mi contraseña</h1>
            </div>
            <div class="page__content bordered">
                <div class="page__form">
                    <c:if test="${not empty errorMessage}">
                        <div class="row m-3">
                            <div class="col-sm-12">
                                <div class="alert alert-danger justify-content-center" role="alert">
                                    ${errorMessage}
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${not empty message}">
                        <div class="row m-3">
                            <div class="col-sm-12">
                                <div class="alert alert-success justify-content-center" role="alert">
                                    ${message}
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <form name="passwordRecoveryForm" id="passwordRecoveryForm" action="/password/solicita-nueva-contrasena"
                        method="POST">
                        <div class="row m-3">
                            <div class="col-sm-12">
                                <h2 class="h6 p-2 colorbase1">Para iniciar tu solicitud de recuperación de contraseña
                                    ingresa los siguientes datos:</h2>
                            </div>
                        </div>
                        <div class="row m-3">
                            <div class="col-sm-12 col-lg-6 d-flex align-items-center">
                                <i class="icon-mail_outline"></i>
                                <label for="" class="font-weight-bold m-0">Correo electrónico</label>
                            </div>
                            <div class="col-sm-12 col-lg-6"><input type="text" name="email" id="email" class="form-control">
                            </div>
                        </div>
                        <div class="row m-3">
                            <div class="col-sm-12">
                                <div class="response__message"></div>
                                <div class="d-flex justify-content-end">
                                    <button type="button" class="btn btn-primary d-flex"
                                        id="passwordRecoveryBtn">Enviar</button>
                                </div>
                                <div class="loading">
                                    <div class="loading__container d-flex justify-content-center align-items-center">
                                        <div class="loading__message text-center">
                                            <div class="lds-ripple">
                                                <div></div>
                                                <div></div>
                                            </div>
                                            <p class="d-block text-center">Procesando solicitud por favor espere</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <jsp:include page="../layout/footer.jsp" />
    <script>
        $(document).ready(function () {
            $("#passwordRecoveryBtn").on("click", function () {
                if (validatePasswordRecoveryForm()) {
                    $("#passwordRecoveryForm").submit();
                }
            });
        });

        //Function ValidateForm
        function validatePasswordRecoveryForm() {
            var form = $("#passwordRecoveryForm");
            var selector = form;
            selector.validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    }
                },
                messages: {
                    email: {
                        required: "Este campo es requerido",
                        email: "Ingrese un email válido"
                    }
                }
            });
            return form.valid();
        }

    </script>
</body>

</html>