<header class="Header" id="Header">
        <%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
        <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
    <div class="container">
        <div class="d-flex justify-content-between align-items-center">
            <div class="d-flex justify-content-end align-items-center">
                <a href="/inicio" class="Header__logo">
                    <img src="/img/cb-logo.png" alt="Cristianismo Bíblico Registro">
                </a>
                <div class="Header__event__info d-none d-sm-none d-md-none d-lg-block">
                    <p class="text-light m-0 pr-4 text-uppercase">Sexta Conferencia Anual de Cristianismo Bíblico<br/><span class="colorbase2">La Gloria de Cristo en los Profetas Menores</span></p>
                </div>
            </div>
            <!--Menu for user logged active class 'd-flex' -->
            <c:choose>
                <c:when test="${not empty currentUser}">
                    <div class="d-flex justify-content-end align-items-center">
                        <div class="client__menu">
                            <button class="btn btn-success dropdown-account d-flex align-items-center" type="button"
                                id="dropdown-account">
                                <i class="icon-account_circle"></i> <span>${currentUser}</span>
                                <i class="icon-keyboard_arrow_down"></i>
                            </button>
                            
                            <div class="dropdown-account__menu">
                                <ul>
                                    <li><a href="/perfil-cuenta-page">Mi registro</a></li>                                    
                                    <sec:authorize access="hasRole('LODGE_ATTENDANT') or hasRole('ADMIN')">
                                        <li><a href="/guest-details-page">Asignar cuarto</a></li>
                                    </sec:authorize>
                                    <sec:authorize access="hasRole('DESK_ATTENDANT') or hasRole('ADMIN')">
                                        <li><a href="/guest-checkin-page">Check-in</a></li>
                                    </sec:authorize>
                                    <sec:authorize access="hasRole('ADMIN') or hasRole('KITCHEN_ATTENDANT')">
                                        <li><a href="/meals-report-page">Comedor</a></li>
                                    </sec:authorize>
                                    <sec:authorize access="hasRole('ADMIN')">
                                        <li><a href="/dashboard">Estatus general</a></li>
                                    </sec:authorize>
                                    <sec:authorize access="hasRole('LIASON') or hasRole('ADMIN')">
                                        <li><a href="/liason/registro-pagos-page">Registro de pagos</a></li>                                        
                                    </sec:authorize>                                                                                                            
                                    
                                    <li><a href="/logout">Cerrar sesión</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <!--End Menu for user logged-->

                    <!--Menu public deactive class 'd-none' -->
                    <div class="d-flex justify-content-end align-items-center">
                        <a href="/crea-cuenta-page" class="btn btn-warning m-2 d-flex align-items-center"><i
                                class="icon-mode_edit"></i> Regístrate</a>
                        <a href="/login" class="btn btn-success m-2 d-flex align-items-center"><i
                                class="icon-lock"></i> Inicio de sesi&oacute;n</a>
                    </div>
                </c:otherwise>
            </c:choose>
            <!--End Menu public -->
        </div>
    </div>
</header>