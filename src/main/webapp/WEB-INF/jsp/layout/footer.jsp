<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<footer class="Footer">
    <div class="Footer__top d-none d-sm-none d-md-block">
        <div class="container">
            <div class="row m3">
                <div class="col-sm-12 d-flex">
                    <div class="Footer__info">
                        <a href="/inicio" class="Footer__logo">
                            <img src="/img/cb-logo.png" alt="Cristianismo Bíblico">
                        </a><br>
                    </div>
                    <div class="Footer__limit">
                        <p><span class="colorbase2"><strong>Fin de registro:</strong></span><br><strong>1<sup>o</sup> de Nov. 2019</strong><br>¡Cupo Limitado!</p>
                    </div>
                    <div class="Footer__limit">
                        <p><span class="colorbase2"><strong>Lugar:</strong></span><br><a href="https://goo.gl/maps/uH3MmgyU3BvUEXMk8" target="_blank">Centro Cristiano de<br> Los Altos Cristo Vive,<br> Municipio de Arandas, Jal.</a></p>
                    </div>
                    <div class="Footer__limit">
                        <p>
                            <span class="colorbase2"><strong>Correo:</strong></span><br><a href="mailto:conferenciacb@cristianismobiblico.com">conferenciacb@cristianismobiblico.com</a><br>
                            <span class="colorbase2"><strong>Teléfono:</strong></span><br>(55) 7788-8286<br>
                        </p>
                    </div>
                    <div class="Footer__limit">
                            <p>
                                <span class="colorbase2"><strong>Horario de atención:</strong></span><br>Lun-Sáb 9:00 - 17:00 hrs.<br>
                            </p>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <div class="Footer__bottom">
        <div class="container">
            <div class="row m3">
                <div class="col-sm-12">
                    <p class="text-center Footer__copy">&copy; 2019 Conferencias Cristianismo B&iacute;blico. Todos los derechos reservados.</p>
                </div>
            </div>
        </div>
    </div>
    <a href="#Header" class="Footer__up d-flex align-items-center justify-content-center">
        <i class="icon-arrow_upward"></i>
    </a>
</footer>

<script src="/webjars/jquery/1.9.1/jquery.min.js"></script>
<script src="/js/popper.min.js"></script>
<script src="/webjars/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="/js/main.js"></script>
<script src="/js/jquery.validate.min.js"></script>
<link rel="stylesheet" type="text/css" href="/js/datatables.min.css"/>
<script type="text/javascript" src="/js/datatables.min.js"></script>

