<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>${param.title} | Sexta Conferencia Anual de Cristianismo B&iacute;blico</title>
<meta name="description" content="${param.description}" />
<link href="/webjars/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
<link href="/css/main.css" rel="stylesheet">