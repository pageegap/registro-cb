<html>
<head>
    <%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
    <jsp:include page="layout/head-elements.jsp">
        <jsp:param name="title" value="Información" />
        <jsp:param name="description" value="Información sobre la Sexta Conferencia Anual de Cristianismo Bíblico" />
    </jsp:include>
</head>
<body class="home">
    <jsp:include page="layout/header.jsp" />
    <div class="page__container">
        <div class="container">
            <div class="page__head">
                <h1 class="page__head__title p-4 bg-secondary text-light">Información General del Evento</h1>
            </div>
        </div>
    </div>
    <jsp:include page="layout/footer.jsp" />
</body>
</html>