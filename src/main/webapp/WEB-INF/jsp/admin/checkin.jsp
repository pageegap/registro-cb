<html>

<head>
    <%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
    <jsp:include page="../layout/head-elements.jsp">
        <jsp:param name="title" value="Registro de perfil" />
        <jsp:param name="description" value="Alta de información de perfil Servicios e Invitados" />
    </jsp:include>
</head>

<body>
    <jsp:include page="../layout/header.jsp" />

    <div class="page__container">
        <div class="container">
            <div class="page__head">
                <h1 class="page__head__title p-4">Check-in
                    <div class="badge badge-info" style="background-color: #5b102e;float:right">
                        contador : <span id="totalCheckedIn"></span>
                    </div>
                </h1>
            </div>

            <div class="page__content page__content--fixed bordered mb-5">
                <div class="table__header">
                    <div class="table__header__content container">
                        <div class="row mr-3 ml-3 mt-3 mb-5">
                            <div class="col-lg-2 d-none d-lg-block">
                                <h5>Filtrar por: </h5>
                            </div>
                            <div class="col-md-4 col-lg-2">
                                <div class="input-group">
                                    <input type="text" id="search-by-id" class="form-control" placeholder="ID (cuenta)"
                                        aria-label="Recipient's username" aria-describedby="button-addon2">
                                </div>
                            </div>
                            <div class="col-md-8 col-lg-3">
                                <div class="input-group ">
                                    <input type="text" id="search-by-other" class="form-control"
                                        placeholder="Otros datos" aria-label="Recipient's username"
                                        aria-describedby="button-addon2">
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="d-flex justify-content-end mt-1">
                                    <button type="button" id="activate-checkins"
                                        class="btn btn-primary d-flex flex-row mr-4"><i class="icon-add"></i> Activar
                                        <span class="d-none d-sm-block">Check-In</span></button>
                                    <button type="button" id="deactivate-checkins"
                                        class="btn btn-primary d-flex flex-row"><i class="icon-close"></i> Desactivar
                                        <span class="d-none d-sm-block">Check-In</span></button>
                                </div>
                            </div>
                        </div>
                        <div class="table__header__head">
                            <table class="table" id="guest-table-copy">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col" class="text-center">ID (C)</th>
                                        <th scope="col" class="text-center">ID (P)</th>
                                        <th scope="col" class="text-center">Nombre</th>
                                        <th scope="col" class="text-center">Notas</th>
                                        <th scope="col" class="text-center">Pagado</th>
                                        <th scope="col" class="text-center">Ciudad</th>
                                        <th scope="col" class="text-center">Iglesia</th>
                                        <th scope="col" class="text-center">Comidas</th>
                                        <th scope="col" class="text-center">Cuarto</th>
                                        <th scope="col" class="text-center">Check-in</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row m-3">
                    <div class="col-sm-12">
                        <div class="guest__list">
                            <table id="guest-table" class="table table-checkin table-hover table-responsive">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col" class="text-center">ID (C)</th>
                                        <th scope="col" class="text-center">ID (P)</th>
                                        <th scope="col" class="text-center">Nombre</th>
                                        <th scope="col" class="text-center">Notas</th>
                                        <th scope="col" class="text-center">Pagado</th>
                                        <th scope="col" class="text-center">Ciudad</th>
                                        <th scope="col" class="text-center">Iglesia</th>
                                        <th scope="col" class="text-center">Comidas</th>
                                        <th scope="col" class="text-center">Cuarto</th>
                                        <th scope="col" class="text-center" style="width: 128px">Check-in</th>
                                    </tr>
                                </thead>
                                <tbody class="dinamicListGuest tableDinamicList" id="all-guests">
                                    <!--Load dinamic -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="activateCheckinModal" tabindex="-1" role="dialog" aria-labelledby="modalGuestLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title subtitle--small">Registro Check In</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form name="activate_checkin_form" id="activateCheckinForm" action="" method="POST">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="" class="font-weight-bold">Introduce los ID's</label>
                                    <input type="text" name="checkin_list_activate"
                                        class="form-control checkin-list-input" id="checkin-list-activate">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="modal-footer-buttons">
                            <div class="d-flex justify-content-end">
                                <button type="button" class="btn btn-secondary"
                                    data-dismiss="modal">Cancelar</button>&nbsp;
                                <button type="button" class="btn btn-primary d-flex" id="activateCheckinBtn"><i
                                        class="icon-mode_edit"></i>&nbsp;Actualiza Check In</button>
                            </div>
                        </div>
                        <div class="modal-footer-load">
                            <div class="lds-ellipsis">
                                <div></div>
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="deactivateCheckinModal" tabindex="-1" role="dialog"
            aria-labelledby="modalGuestLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title subtitle--small">Deshacer Check In</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form name="deactivate_checkin_form" id="deactivateCheckinForm" action="" method="POST">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="" class="font-weight-bold">Introduce los ID's</label>
                                    <input type="text" name="checkin_list_deactivate"
                                        class="form-control checkin-list-input" id="checkin-list-deactivate">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="modal-footer-buttons">
                            <div class="d-flex justify-content-end">
                                <button type="button" class="btn btn-secondary"
                                    data-dismiss="modal">Cancelar</button>&nbsp;
                                <button type="button" class="btn btn-primary d-flex" id="deactivateCheckinBtn"><i
                                        class="icon-delete"></i>&nbsp;Eliminar Check In</button>
                            </div>
                        </div>
                        <div class="modal-footer-load">
                            <div class="lds-ellipsis">
                                <div></div>
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="toastActionActive" class="toast" role="alert" aria-live="assertive" aria-atomic="true"
            data-delay="5000" data-animation="true">
            <div class="toast-header">
                <strong class="mr-auto">Registrar Check In</strong>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body">
                Se ha hecho check-in de los IDs introducidos.
            </div>
        </div>
        <div id="toastActionDeactive" class="toast" role="alert" aria-live="assertive" aria-atomic="true"
            data-delay="5000" data-animation="true">
            <div class="toast-header">
                <strong class="mr-auto">Eliminar Check In</strong>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body">
                Se ha desactivado el check-in de los IDs introducidos.
            </div>
        </div>
        <jsp:include page="../layout/footer.jsp" />
        <script>
            function compareGuests(a, b) {
                return (a.guestId > b.guestId) ? 1 : ((b.guestId > a.guestId) ? -1 : 0);
            }

            window.onload = function () {
                getAllGuests();

            }

            function getAllGuests() {
                $.ajax({
                    url: "/recupera-guest-info",
                    type: "GET",
                    success: function (response) {
                        setGuestDetailsList(response);
                    }
                });
            }

            //prints Guests in List
            function setGuestDetailsList(guests) {
                var tmpListGuest = "";

                let keys = Object.keys(guests);
                let totalGuests = keys.length;                
                let totalCheckedIn = 0;
                
                for (var key in keys) {
                    //sort members of account by id
                    let accountArrayGuests = guests[key].sort(compareGuests);

                    accountArrayGuests.forEach(guest => {

                        let name = guest.firstName + " " + guest.lastName;

                        let meals = hasAllMeals(guest);
                        let isChecked = guest.checkin == "-" ? "<span class='text-danger h4'><i class='icon-person'></i></span>" : "<span class='text-success h4'><i class='icon-person'></i></span>&nbsp;<span class='text-sucess'>" + guest.checkin + "</span>";
                        if (guest.checkin != "-") {
                            totalCheckedIn++;
                        }
                        let paid = guest.settleDebt === '-' ? "<span class='text-danger h5'><i class='icon-minus'></i>&nbsp;<i class='icon-attach_money'></i></span>" : "<span class='text-success h5'><i class='icon-done'></i>&nbsp;<i class='icon-attach_money'></i></span>";
                        let allMeals = meals ? "<span class='text-success h5'><i class='icon-done'></i>&nbsp;<i class='icon-spoon-knife'></i></span>" : "<span class='text-danger h5'><i class='icon-minus'></i>&nbsp;<i class='icon-spoon-knife'></i></span>";
                        let room = guest.room ? guest.room : "";
                        roomValue = room.toLowerCase();
                        let htmlNote = "";

                        if (guest.note != "" && guest.note != null) {
                            htmlNote = "<button style='margin: 5px 0px;' type='button' class='btn btn-sm btn-warning' data-toggle='popover' title='Comentarios' data-content='" + guest.note + "'><i class='icon-help'></i>&nbsp;NOTA</button>";
                        }
                        if (roomValue == "null") {
                            roomValue = "";
                        }

                        tmpListGuest = tmpListGuest +
                            "<tr><td class='text-center'><strong>" + guest.userId + "</strong></td>" +
                            "<td class='text-center'><strong>" + guest.guestId + "</strong></td>" +
                            "<td>" + name + "</td>" +
                            "<td>" + htmlNote + "</td>" +
                            "<td class='text-center'>" + paid + "</td>" +
                            "<td hidden class='text-center'>" + guest.country + "</td>" +
                            "<td class='text-center'>" + guest.city + "</td>" +
                            "<td hidden class='text-center'>" + guest.state + "</td>" +
                            "<td class='text-center'>" + guest.church + "</td>" +
                            "<td class='text-center'> " + allMeals + "</td>" +
                            "<td class='text-center'><i class='icon-location_city'></i>&nbsp;" + roomValue + "</td>" +
                            "<td class='text-center'>" + isChecked + "</td>" +
                            "</tr>";
                    });

                }
                $(".dinamicListGuest").html(tmpListGuest);
                $('[data-toggle="popover"]').popover({
                    trigger: 'focus'
                });                
                $('#totalCheckedIn').html(totalCheckedIn+"/"+totalGuests);
            }



            function hasAllMeals(guest) {
                let f1 = guest.foodFriday1;
                let f2 = guest.foodMonday1;
                let f3 = guest.foodSaturday1;
                let f4 = guest.foodSaturday2;
                let f5 = guest.foodSaturday3;
                let f6 = guest.foodSunday1;
                let f7 = guest.foodSunday2;
                let f8 = guest.foodSunday3;
                return (f1 && f2 && f3 && f4 && f5 && f6 && f7 && f8);
            }

            function updateCheckIn(payload) {

                $.ajax({
                    url: "/update-checkin",
                    type: "POST",
                    data: JSON.stringify(payload),
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    success: function (response) {
                        $(".modal-footer-buttons").show();
                        $(".modal-footer-load").hide();
                        $("#activateCheckinModal").modal("hide");
                        $("#deactivateCheckinModal").modal("hide");
                        $('#toastActionActive').toast('show');
                        getAllGuests(); //commented to prevent query all in each update
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("there was an error " + thrownError);
                        $(".modal-footer-buttons").show();
                        $(".modal-footer-load").hide();
                        $("#activateCheckinModal").modal("hide");
                        $("#deactivateCheckinModal").modal("hide");
                    }
                });

            }

            $(document).ready(function () {
                $("#search-by-id").on("keyup", function () {
                    var value = $(this).val().toLowerCase();
                    $("#all-guests tr").filter(function (i, row) {
                        let td1 = $(row).find("td:nth-child(1)").text()
                        if (value === "") {
                            $(this).toggle(true)
                        } else {
                            $(this).toggle(td1.toLowerCase() === value)
                        }
                    });
                });

                $("#search-by-other").on("keyup", function () {
                    var value = $(this).val().toLowerCase();
                    $("#all-guests tr").filter(function (i, row) {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                });

                $('#activate-checkins').on('click', function () {
                    $("#activateCheckinModal").modal("show");
                });

                $('#deactivate-checkins').on('click', function () {
                    $("#deactivateCheckinModal").modal("show");
                });

                $('#activateCheckinBtn').on('click', function () {
                    if (validateCheckinListActivate("#activateCheckinForm")) {
                        let checkinListActivate = $("#checkin-list-activate").val();

                        let ids = checkinListActivate.split(",");
                        let payload = {};
                        ids.forEach((elem) => {
                            if (elem != "" && elem != " ") {
                                payload[elem] = true;
                            }

                        });
                        updateCheckIn(payload);

                        $(".modal-footer-buttons").hide();
                        $(".modal-footer-load").show();

                    }
                });

                $('#deactivateCheckinBtn').on('click', function () {
                    if (validateCheckinListDeactivate("#deactivateCheckinForm")) {
                        let checkinListDeactivate = $("#checkin-list-deactivate").val();

                        $(".modal-footer-buttons").hide();
                        $(".modal-footer-load").show();

                        let ids = checkinListDeactivate.split(",");
                        let payload = {};
                        ids.forEach((elem) => {
                            if (elem != "" && elem != " ") {
                                payload[elem] = false;
                            }
                        });
                        updateCheckIn(payload);
                    }
                });

                $("#activateCheckinForm").submit(function (e) {
                    e.preventDefault();
                });

                $("#deactivateCheckinForm").submit(function (e) {
                    e.preventDefault();
                });

                //Clean Guest Form when close modal
                $('.modal').on('hide.bs.modal', function (e) {
                    $(".checkin-list-input").val("");
                });


                $(window).scroll(function (event) {

                    var st = $(this).scrollTop();
                    var startPoint = $(".page__content--fixed").offset().top;

                    if (st > startPoint) {
                        $("#guest-table-copy th:nth-child(1)").css('width', $("#guest-table th:nth-child(1)").width());
                        $("#guest-table-copy th:nth-child(2)").css('width', $("#guest-table th:nth-child(2)").width());
                        $("#guest-table-copy th:nth-child(3)").css('width', $("#guest-table th:nth-child(3)").width());
                        $("#guest-table-copy th:nth-child(4)").css('width', $("#guest-table th:nth-child(4)").width());
                        $("#guest-table-copy th:nth-child(5)").css('width', $("#guest-table th:nth-child(5)").width());
                        $("#guest-table-copy th:nth-child(6)").css('width', $("#guest-table th:nth-child(6)").width());
                        $("#guest-table-copy th:nth-child(7)").css('width', $("#guest-table th:nth-child(7)").width());
                        $("#guest-table-copy th:nth-child(8)").css('width', $("#guest-table th:nth-child(8)").width());
                        $("#guest-table-copy th:nth-child(9)").css('width', $("#guest-table th:nth-child(9)").width());
                        $("#guest-table-copy th:nth-child(10)").css('width', $("#guest-table th:nth-child(10)").width());

                        $(".table__header").addClass("fixed");
                        $(".table-checkin").addClass("fixed");
                    } else {
                        $(".table__header").removeClass("fixed");
                        $(".table-checkin").removeClass("fixed");
                    }

                });
            });

            //Validation Function
            function validateCheckinListActivate(idselector) {
                var selector = $(idselector);
                selector.validate({
                    rules: {
                        checkin_list_activate: {
                            required: true,
                            nospacesonly: true,
                            regex: /^(([0-9](,)?)*)+$/
                        }
                    },
                    messages: {
                        checkin_list_activate: {
                            required: "No has ingresado la lista",
                            nospacesonly: "No has ingresado la lista",
                            regex: "Deben ser ids separados por comas sin espacios"
                        }
                    }
                });
                return selector.valid();
            }
            //Validation Function
            function validateCheckinListDeactivate(idselector) {
                var selector = $(idselector);
                selector.validate({
                    rules: {
                        checkin_list_deactivate: {
                            required: true,
                            nospacesonly: true,
                            regex: /^(([0-9](,)?)*)+$/
                        }
                    },
                    messages: {
                        checkin_list_deactivate: {
                            required: "No has ingresado la lista",
                            nospacesonly: "No has ingresado la lista",
                            regex: "Deben ser ids separados por comas sin espacios"
                        }
                    }
                });
                return selector.valid();
            }
        </script>
</body>

</html>