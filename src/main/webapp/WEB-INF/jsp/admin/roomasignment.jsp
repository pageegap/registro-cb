<html>

<head>
    <%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
    <jsp:include page="../layout/head-elements.jsp">
        <jsp:param name="title" value="Registro de perfil" />
        <jsp:param name="description" value="Alta de información de perfil Servicios e Invitados" />
    </jsp:include>
</head>

<body class="fullHeight">
    <jsp:include page="../layout/header.jsp" />
    
    <div class="page__container">
        <div class="container">
            <div class="page__head">
                <h1 class="page__head__title p-4">Asignación de cuartos</h1>
            </div>
            <div class="page__content page__content--fixed bordered mb-5">
                <div class="profile__guests__list">
                    <div class="table__header">
                        <div class="table__header__content container">
                            <div class="row mr-3 ml-3 mt-3 mb-1">
                                <div class="col-lg-2 d-none d-lg-block">
                                    <h5>Filtrar por: </h5>
                                </div>
                                <div class="col-sm-4 col-md-3 col-lg-2">
                                    <div class="input-group ">
                                        <input type="text" id="search-by-id" class="form-control" placeholder="ID (cuenta)"
                                            aria-label="Recipient's username" aria-describedby="button-addon2">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-5">

                                    <div class="input-group ">
                                        <input type="text" id="search-by-other" class="form-control" placeholder="Otros datos"
                                            aria-label="Recipient's username" aria-describedby="button-addon2">
                                    </div>
                                </div>
                                <div class="col-sm-2 col-md-5 col-lg-3">
                                    <div class="d-flex justify-content-end">
                                        <button type="button" id="download"
                                            class="guest__add__btn btn btn-primary d-flex flex-row"><i class="icon-get_app"></i>&nbsp;<span class="d-none d-md-block">Descargar Excel</span></span></button>
        
                                    </div>

                                </div>
                            </div>
                        
                            <div class="row mr-3 ml-3 mt-2 mb-2">
                                <div class="col-md-8 col-lg-9">
                                    <div class="alert alert-warning" role="alert">
                                        Haz clic sobre la celda de <b>cuarto</b> para editar su valor, después de asignar los cuartos en estas celdas haz clic en <b>Actualizar Lista</b> para guardar tu información.
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-3">
                                    <div class="d-flex justify-content-end mt-3">
                                        <button type="button" id="assign-room"
                                            class="guest__add__btn btn btn-primary"><i class="icon-mode_edit"></i>&nbsp;Actualizar Lista</button>
                                        <div class="table__header__load">
                                            <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="table__header__head">
                                <table class="table" id="guest-table-copy">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col" class="text-center">ID (cuenta)</th>
                                            <th scope="col" class="text-center">ID (invitado)</th>
                                            <th scope="col" class="text-center">Nombre</th>
                                            <th scope="col" class="text-center">País</th>
                                            <th scope="col" class="text-center">Ciudad</th>
                                            <th scope="col" class="text-center">Sexo</th>
                                            <th scope="col" class="text-center">Iglesia</th>
                                            <th scope="col" class="text-center">Cuarto</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row m-3">
                        <div class="col-sm-12">
                            <div class="guest__list">
                                <table id="guest-table" class="table table-rooms table-hover table-responsive">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col" class="text-center">ID (cuenta)</th>
                                            <th scope="col" class="text-center">ID (invitado)</th>
                                            <th scope="col" class="text-center">Nombre</th>
                                            <th scope="col" class="text-center">País</th>
                                            <th scope="col" class="text-center">Ciudad</th>
                                            <th scope="col" class="text-center">Sexo</th>
                                            <th scope="col" class="text-center">Iglesia</th>
                                            <th scope="col" class="text-center">Cuarto</th>
                                        </tr>
                                    </thead>
                                    <tbody class="dinamicListGuest tableDinamicList" id="all-guests">
                                        <!--Load dinamic -->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="toastAction" class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-delay="5000" data-animation="true">
            <div class="toast-header">
                <strong class="mr-auto">Lista de cuartos</strong>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body">
                Tu lista de asignación de cuartos ha sido actualizada.
            </div>
        </div>

        <jsp:include page="../layout/footer.jsp" />   
        <script src="/js/xlsx.full.min.js"></script>

        <script>
            let createXLSLFormatObj = [];
            //let dataTable;

            window.onload = function () {
                getAllGuestsRooms();

            }

            function getAllGuestsRooms() {
                $.ajax({
                    url: "/recupera-guests-details",
                    type: "GET",
                    success: function (response) {
                        setGuestDetailsList(response);
                    }
                });
            }
            
            function compareGuests(a, b) {
                return (a.guestId > b.guestId) ? 1 : ((b.guestId > a.guestId) ? -1 : 0);
            }
            //prints Guests in List
            function setGuestDetailsList(guests) {
                var tmpListGuest = "";
                console.log("clean old spreadsheet");
                $(".dinamicListGuest").html(tmpListGuest);
                //define excel header
                var xlsHeader = ["ID(cuenta)", "ID(invitado)", "Nombre", "Edad", "Sexo","Iglesia", "Ciudad", "Fecha de Pago", "Observaciones","Cuarto"];
                createXLSLFormatObj = [];
                createXLSLFormatObj.push(xlsHeader);

                let innerData = [];
                let keys = Object.keys(guests);
                for (var key in keys) {
                    //sort members of account by id
                    let accountArrayGuests = guests[key].sort(compareGuests);
                    accountArrayGuests.forEach(guest => {

                        let name = guest.firstName + " " + guest.lastName;
                        let excelRow = [];
                        let htmlComments = '';
                        let htmlNote = "";                        
                        let roomValue = guest.room == null || guest.room =="null" ? "": guest.room;                        
                        excelRow.push(guest.userId);
                        excelRow.push(guest.guestId + "");
                        excelRow.push(name);
                        excelRow.push(guest.age);
                        excelRow.push(guest.gender);
                        excelRow.push(guest.church);
                        excelRow.push(guest.city);                                         excelRow.push(guest.settleDebt);
                        excelRow.push(guest.note);
                        excelRow.push(guest.room);
                        createXLSLFormatObj.push(excelRow);

                        if(guest.note != "" && guest.note != null){
                            htmlNote = "<br><button style='margin: 5px 0px;' type='button' class='btn btn-sm btn-warning' data-toggle='popover' title='Comentarios' data-content='"+guest.note+"'><i class='icon-help'></i>&nbsp;NOTA</button>";
                        }
                       

                        tmpListGuest = tmpListGuest +
                            "<tr><td class='text-center'><strong>" + guest.userId + "</strong></td>" +
                            "<td class='text-center'><strong>" + guest.guestId + "</strong></td>" +
                            "<td>" + name + htmlNote + "</td>" +
                            "<td hidden class='text-center'>" + guest.settleDebt + "</td>" +                            
                            "<td class='text-center'>" + guest.country + "</td>" +
                            "<td class='text-center'>" + guest.city + "</td>" +
                            "<td class='text-center'>" + guest.gender + "</td>" +
                            "<td class='text-center'>" + guest.church + "</td>" +
                            "<td class='contenteditable text-center' contenteditable='true'>" +roomValue +"</td>";
                    });

                }
                $(".dinamicListGuest").html(tmpListGuest);
                $('[data-toggle="popover"]').popover({
                    trigger: 'focus'
                });
            }
            

            $('#assign-room').on('click', function () {

                let table = $("#all-guests tr");
                payload = {};

                $("#all-guests tr").each((i, row) => {
                    var id = $(row).find("td:nth-child(2)").text();
                    var room = $(row).find("td:nth-child(9)").text();
                    payload[id] = room;

                });

                $('#assign-room').hide();
                $(".table__header__load").show();

                $.ajax({
                    url: "/assign-room",
                    type: "POST",
                    data: JSON.stringify(payload),
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    success: function (guests) {
                        setGuestDetailsList(guests);
                        $('#toastAction').toast('show');  
                        
                        $('#assign-room').show();
                        $(".table__header__load").hide();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("there was an error " + thrownError);
                        $('#assign-room').show();
                        $(".table__header__load").hide();
                    }
                });
            });

            $('#download').on('click', function () {
                downloadExcel(createXLSLFormatObj);
            });

            $(document).ready(function () {
                $("#search-by-id").on("keyup", function () {
                    var value = $(this).val().toLowerCase();
                    $("#all-guests tr").filter(function (i, row) {
                        let td1 = $(row).find("td:nth-child(1)").text()
                        if (value === "") {
                            $(this).toggle(true)
                        } else {
                            $(this).toggle(td1.toLowerCase() === value)
                        }
                    });
                });

                $("#search-by-other").on("keyup", function () {
                    var value = $(this).val().toLowerCase();
                    $("#all-guests tr").filter(function (i, row) {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                });

                $(window).scroll(function (event) {

                    var st = $(this).scrollTop();
                    var startPoint = $(".page__content--fixed").offset().top;

                    if (st > startPoint) {
                        $("#guest-table-copy th:nth-child(1)").css('width', $("#guest-table th:nth-child(1)").width());
                        $("#guest-table-copy th:nth-child(2)").css('width', $("#guest-table th:nth-child(2)").width());
                        $("#guest-table-copy th:nth-child(3)").css('width', $("#guest-table th:nth-child(3)").width());
                        $("#guest-table-copy th:nth-child(4)").css('width', $("#guest-table th:nth-child(4)").width());
                        $("#guest-table-copy th:nth-child(5)").css('width', $("#guest-table th:nth-child(5)").width());
                        $("#guest-table-copy th:nth-child(6)").css('width', $("#guest-table th:nth-child(6)").width());
                        $("#guest-table-copy th:nth-child(7)").css('width', $("#guest-table th:nth-child(7)").width());
                        $("#guest-table-copy th:nth-child(8)").css('width', $("#guest-table th:nth-child(8)").width());
                        
                        $(".table__header").addClass("fixed");
                        $(".table-rooms").addClass("fixed");
                    }else {
                        $(".table__header").removeClass("fixed");
                        $(".table-rooms").removeClass("fixed");
                    }
                });

            });

            function downloadExcel(createXLSLFormatObj) {                
                var filename = "asignacion_cuartos.xlsx";                
                var ws_name = "cuartos";
                var wb = XLSX.utils.book_new(),
                    ws = XLSX.utils.aoa_to_sheet(createXLSLFormatObj);

                XLSX.utils.book_append_sheet(wb, ws, ws_name);
                XLSX.writeFile(wb, filename);
            }
            
        </script>
</body>

</html>