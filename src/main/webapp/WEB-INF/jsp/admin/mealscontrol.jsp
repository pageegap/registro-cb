<html>

<head>
    <%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
    <jsp:include page="../layout/head-elements.jsp">
        <jsp:param name="title" value="Registro de perfil" />
        <jsp:param name="description" value="Alta de información de perfil Servicios e Invitados" />
    </jsp:include>
</head>

<body>
    <jsp:include page="../layout/header.jsp" />

    <div class="page__container">
        <div class="container">
            <div class="page__head">
                <h1 class="page__head__title p-4">Comedor</h1>
            </div>
            <div class="page__content bordered mb-5">
                <div class="row m-3">
                    <div class="col-sm-12">
                        <!-- Tab links -->
                        <div class="tab">
                            <button class="tablinks btn-success mr-2 mt-2" onclick="openCity('Reporte')">Personas en comedor</button>
                            <button class="tablinks btn-success mt-2" onclick="openCity('Escaneo')">Validar Comida</button>
                        </div>
                    </div>
                </div>
                <!-- Tab content -->
                <div id="Reporte" class="tabcontent">
                    <div class="page__form">
                        <form name="mealForm" id="mealForm" method="POST">
                            <div class="row m-3">
                                <div class="col-sm-12">
                                    <p class="h6 p-2 colorbase1"><strong>Selecciona el tipo de comida del día</strong></p>
                                </div>
                            </div>
                            <div class="row m-3 mb-1">
                                <div class="col-sm-2 col-md-1"><i class="icon-spoon-knife"></i></div>
                                <div class="col-sm-10 col-md-4">
                                    <select name="mealCode" id="mealCode">
                                        <option value="-1"> Tipo de comida</option>
                                        <option value="700"> Cena Viernes</option>
                                        <option value="200">Desayuno Sábado</option>
                                        <option value="500">Comida Sábado</option>
                                        <option value="800">Cena Sábado</option>
                                        <option value="300">Desayuno Domingo</option>
                                        <option value="600">Comida Domingo</option>
                                        <option value="900">Cena Domingo</option>
                                        <option value="400">Desayuno Lunes</option>
                                    </select>
                                </div>
                                <div class="col-sm-12 col-md-5">
                                    <input type="button" value="Consultar" class="btn btn-primary btn-lg float-right mt-2 mb-2" id="queryMeal">
                                </div>
                            </div>
                            <div class="row m-3">
                                <div class="col-sm-12">
                                    <p class="h6 p-2 colorbase1"><strong>Introduce la cantidad de comensales que entran al comedor por edades</strong></p>
                                </div>
                            </div>
                            <div class="row m-3">
                                <div class="col-sm-2 col-md-1"><i class="icon-person"></i></div>
                                <div class="col-sm-12 col-md-3"><input class="form-control" id="infantes"
                                        name="infantes" placeholder="# Infantes"></div>
                                <div class="col-sm-12 col-md-3"><input class="form-control" id="ninios" name="ninios"
                                        placeholder="# Niños"></div>
                                <div class="col-sm-12 col-md-3"><input class="form-control" id="adultos" name="adultos"
                                        placeholder="# Adultos"></div>
                            </div>
                            <div class="row m-3">
                                <div class="col-sm-2 col-md-1"><i class="icon-local_offer"></i></div>
                                <div class="col-sm-10 col-md-9"><input class="form-control" id="comment" name="comment"
                                        placeholder="Comentarios"></div>

                            </div>
                            <div class="row m-3 mt-3 mb-5">
                                <div class="col-sm-12 col-md-10">
                                    <div class="d-flex justify-content-end">
                                        <input type="button" value="Añadir cantidades" class="btn btn-primary btn-lg m-2"
                                        id="registerMeal">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="row m-3">
                        <div class="col-sm-12 col-md-3">
                            <div class="dashboard__card dashboard__card--fullwidth">
                                <div class="dashboard__card__title bg-primary"></i>Comida
                                </div>
                                <div class="dashboard__card__info">
                                    <div id="mealType" class="h3 text-center text-success font-weight-bold">
                                        ${mealType}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3">
                            <div class="dashboard__card dashboard__card--fullwidth">
                                <div class="dashboard__card__title bg-primary">Infantes
                                </div>
                                <div class="dashboard__card__info">
                                    <div id="totalInfants" class="h3 text-center text-dark font-weight-bold">
                                        ${totalInfants}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3">
                            <div class="dashboard__card dashboard__card--fullwidth">
                                <div class="dashboard__card__title bg-primary">Niños
                                </div>
                                <div class="dashboard__card__info">
                                    <div id="totalChildren" class="h3 text-center text-dark font-weight-bold">
                                        ${totalChildren}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3">
                            <div class="dashboard__card dashboard__card--fullwidth">
                                <div class="dashboard__card__title bg-primary">Adultos
                                </div>
                                <div class="dashboard__card__info">
                                    <div id="totalAdults" class="h3 text-center text-dark font-weight-bold">
                                        ${totalAdults}</div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <div id="Escaneo" class="tabcontent">
                    <div class="page__form">
                        <form name="mealFormIndividual" id="mealFormIndividual" method="POST">
                            <div class="row m-3">
                                <div class="col-sm-2 col-md-1"><i class="icon-spoon-knife"></i></div>
                                <div class="col-sm-10 col-md-4">
                                    <select name="mealCodeScan" id="mealCodeScan">
                                        <option value="-1"> Tipo de comida</option>
                                        <option value="700"> Cena Viernes</option>
                                        <option value="200">Desayuno Sábado</option>
                                        <option value="500">Comida Sábado</option>
                                        <option value="800">Cena Sábado</option>
                                        <option value="300">Desayuno Domingo</option>
                                        <option value="600">Comida Domingo</option>
                                        <option value="900">Cena Domingo</option>
                                        <option value="400">Desayuno Lunes</option>
                                    </select>
                                </div>
                                <div class="col-sm-2 col-md-1"><i class="icon-person"></i></div>
                                <div class="col-sm-10 col-md-6"><input class="form-control" id="guestIdInput" name="guestIdInput"
                                        placeholder="Introduce el ID del Invitado">
                                </div>      
                            </div>    
                            <div class="row m-3"> 
                                <div class="col-sm-12">
                                    <div class="d-flex justify-content-end">
                                        <input type="button" value="Validar" class="btn btn-primary btn-lg m-2"
                                        id="validateMealBtn">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div id="hasMealIcon">${hasMealIcon}</div>   
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal" id="modalConfirmMealRegistration" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Confirma registro</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Comida: <strong><span id="confirmMealType">${confirmMealType}</span></strong></p>
                        <p>Infantes: <strong><span id="confirmTotalInfants">${confirmTotalInfants}</span></strong></p>
                        <p>Niños: <strong><span id="confirmTotalChildren">${confirmTotalChildren}</span></strong></p>
                        <p>Adultos: <strong><span id="confirmTotalAdults">${confirmTotalAdults}</span></strong></p>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary" id="confirmPaymentButton">Registar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="../layout/footer.jsp" />

        <div id="toastAction" class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-delay="5000" data-animation="true">
            <div class="toast-header">
                <strong class="mr-auto">Comensales en comedor</strong>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body">
                Verifica las cantidades actualizadas
            </div>
        </div>

        <script>

            let totalInfants = $('#totalInfants').html("0");
            let totalChildren = $('#totalChildren').html("0");
            let totalAdults = $('#totalAdults').html("0");
            let mealType = $('#mealType').html("0");

            // this was taken from www schools etc
            function openCity(cityName) {

                
                // Declare all variables
                var i, tabcontent, tablinks;

                // Get all elements with class="tabcontent" and hide them
                tabcontent = document.getElementsByClassName("tabcontent");
                for (i = 0; i < tabcontent.length; i++) {
                    tabcontent[i].style.display = "none";
                }

                // Get all elements with class="tablinks" and remove the class "active"
                tablinks = document.getElementsByClassName("tablinks");
                for (i = 0; i < tablinks.length; i++) {
                    tablinks[i].className = tablinks[i].className.replace(" active", "");
                }

                // Show the current tab, and add an "active" class to the button that opened the tab
                document.getElementById(cityName).style.display = "block";
                
            }




            window.onload = function () {
                
                openCity('Reporte');
            }

            

            $('#registerMeal').on("click", function () {
                if (!validateForm()) {
                    return;
                }

                let mealCode = $('#mealCode').val();
                let infantes = $('#infantes').val();
                let ninios = $('#ninios').val();
                let adultos = $('#adultos').val();
                let comentario = $('#comment').val();
                let mealType = getMealTypeName(mealCode);

                let confirmTotalInfants = $('#confirmTotalInfants').html(infantes);
                let confirmTotalChildren = $('#confirmTotalChildren').html(ninios);
                let confirmTotalAdults = $('#confirmTotalAdults').html(adultos);
                let confirmMealType = $('#confirmMealType').html(mealType);

                $('#modalConfirmMealRegistration').modal("show");

            });
            $('#confirmPaymentButton').on('click', function () {
                let mealCode = $('#mealCode').val();
                let infantes = $('#infantes').val();
                let ninios = $('#ninios').val();
                let adultos = $('#adultos').val();
                let comentario = $('#comment').val();
                registerMealsRecord(mealCode, infantes, ninios, adultos, comentario);
            });

            $('#queryMeal').on('click', function () {
                if (!validateForm()) {
                    $('#totalInfants').html("-");
                    $('#totalChildren').html("-");
                    $('#totalAdults').html("-");
                    $('#mealType').html("-");
                    return;
                }
                let mealCode = $('#mealCode').val();
                registerMealsRecord(mealCode, 0, 0, 0, 0);
            });

            function registerMealsRecord(mealCode, infantes, ninios, adultos, comentario) {

                payload = { code: mealCode, infants: infantes, children: ninios, adults: adultos, note: comentario };
                $.ajax({
                    url: "/add-meal-report",
                    type: "POST",
                    data: JSON.stringify(payload),
                    contentType: "application/json; charset=utf-8",
                    success: function (response) {
                        console.log("all good", response);
                        clearForm();
                        totalInfants = $('#totalInfants').html(response.infants);
                        totalChildren = $('#totalChildren').html(response.children);
                        totalAdults = $('#totalAdults').html(response.adults);
                        mealType = $('#mealType').html(getMealTypeName(response.code));
                        $('#modalConfirmMealRegistration').modal("hide");
                        $('#toastAction').toast('show');  
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("there was an error " + thrownError);
                    }
                });
            }
            function queryHasMeal(code, id) {
                $.ajax({
                    url: "/check-has-meal?code="+code+"&id="+id,
                    type: "GET",
                    success: function (result) {      
                        console.log(result);
                        openCity('Escaneo');
                        $('#guestIdInput').val("");
                        if(result.message) { 
                            $('#hasMealIcon').html("<div class='hasMealUser'><b>["+id+"]</b> "+result.name+"</div><span class='hasMealSuccess d-flex justify-content-center align-items-center'><i class='icon-done'></i></span>");
                        } else {
                            $('#hasMealIcon').html("<div class='hasMealUser'>Invitado: "+id+"</div><span class='hasMealError d-flex justify-content-center align-items-center'><i class='icon-close'></i></span>");
                        }                        
                    },
                    error: function (error) {                        
                        alert("error "+ error.statusText);
                    }
                });

            }

            $("#guestIdInput").on('keyup', function (e) {
                if (e.keyCode === 13) {       
                    if (!validateMealForm()) {
                        return;
                    }             
                    let mealCode = $('#mealCodeScan').val();;
                    let guestId = $('#guestIdInput').val();                    
                    queryHasMeal(mealCode,guestId);
                }
            });

            $("#validateMealBtn").on('click', function (e) {
                if (!validateMealForm()) {
                    return;
                }
                let mealCode = $('#mealCodeScan').val();;
                let guestId = $('#guestIdInput').val();                    
                queryHasMeal(mealCode,guestId);
            });

            $("#mealForm").submit(function(e){
                e.preventDefault();
            });

            $("#mealFormIndividual").submit(function(e){
                e.preventDefault();
            });

            function getMealTypeName(code) {

                let mealName = "Error";
                if (code == 700) {
                    mealName = "Cena Viernes"
                } else if (code == 200) {
                    mealName = "Desayuno Sábado";
                } else if (code == 500) {
                    mealName = "Comida Sábado";
                } else if (code == 800) {
                    mealName = "Cena Sábado";
                } else if (code == 300) {
                    mealName = "Desayuno Domingo";
                } else if (code == 600) {
                    mealName = "Comida Domingo";
                } else if (code == 900) {
                    mealName = "Cena Domingo";
                } else if (code == 400) {
                    mealName = "Desayuno Lunes";
                }

                return mealName;
            }

            function clearForm() {                
                //$('#mealForm').trigger("reset");
                $('#infantes').val("");
                $('#ninios').val("");
                $('#adultos').val("");
                $('#comment').val("");
            }

            $('#mealCode').on('change', function () {
                $('#infantes').val("");
                $('#ninios').val("");
                $('#adultos').val("");
                $('#comment').val("");
            });


            function validateForm() {
                var selector = $("#mealForm");
                var validator = selector.validate({
                    rules: {
                        mealCode: {
                            required: true,
                            valueNotEquals: "-1"
                        },
                        infantes : {
                            digits: true,
                            min: 1
                        },
                        ninios : {
                            digits: true,
                            min: 1
                        },
                        adultos : {
                            digits: true,
                            min: 1
                        },
                    },
                    messages: {
                        mealCode: {
                            required: "Es requerido",
                            valueNotEquals: "Seleccione el tipo de comida"
                        },
                        infantes : {
                            digits: "Debe ser un número sin fracción",
                            min: "Debe ser un número mayor a 0"
                        },
                        ninios : {
                            digits: "Debe ser un número sin fracción",
                            min: "Debe ser un número mayor a 0"
                        },
                        adultos : {
                            digits: "Debe ser un número sin fracción",
                            min: "Debe ser un número mayor a 0"
                        },
                    }
                });
                console.log("selector valid =>", validator.errors())
                return selector.valid();
            }

            function validateMealForm() {
                var selector = $("#mealFormIndividual");
                var validator = selector.validate({
                    rules: {
                        mealCodeScan: {
                            required: true,
                            valueNotEquals: "-1"
                        },
                        guestIdInput : {
                            required: true,
                            digits: true,
                            min: 1
                        }
                    },
                    messages: {
                        mealCodeScan: {
                            required: "Es requerido",
                            valueNotEquals: "Seleccione el tipo de comida"
                        },
                        guestIdInput : {
                            required: "Es requerido",
                            digits: "Debe ser un número sin fracción",
                            min: "Debe ser un número mayor a 0"
                        }
                    }
                });
                return selector.valid();
            }
        </script>
</body>

</html>