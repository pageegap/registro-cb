<html>

<head>
    <%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
    <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
    <%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
    <jsp:include page="../layout/head-elements.jsp">
        <jsp:param name="title" value="Dashboard" />
        <jsp:param name="description" value="Estatus General del Evento" />
    </jsp:include>
</head>

<body class="dashboard">
    <jsp:include page="../layout/header.jsp" />

    <div class="page__container">
        <div class="container">
            <div class="page__head">
                <h1 class="page__head__title p-4">Estatus General del Evento</h1>
            </div>
            <div class="page__content bordered pt-5 pb-5">
                <div class="dashboard__list">
                    <div class="row">
                        <div class="col-sm-12 col-lg-8">
                            <div class="dashboard__card">
                                <div class="dashboard__card__title bg-primary"><i class="icon-person"></i> Asistentes</div>
                                <div class="dashboard__card__info d-flex">
                                    <table class="table table-bordered dashboard__table dashboard__table--small table-responsive">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col" class="text-center">Total</th>
                                                <th scope="col" class="text-center t__column__secondary">Adultos</th>
                                                <th scope="col" class="text-center t__column__secondary">Niños</th>
                                                <th scope="col" class="text-center t__column__secondary">Infantes</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="confirmTable--null">${register.all}</div><hr>Confirmados<br>
                                                    <div class="confirmTable">${confirmedRegister.allConfirmed}</div>
                                                </td>
                                                <td>
                                                    <div class="confirmTable--null">${register.adults}</div> <hr>Confirmados<br>
                                                    <div class="confirmTable">${confirmedRegister.adultsConfirmed}</div>
                                                </td>
                                                <td>
                                                    <div class="confirmTable--null">${register.children}</div><hr>Confirmados<br>
                                                    <div class="confirmTable">${confirmedRegister.childrenConfirmed}</div>
                                                </td>
                                                <td>
                                                    <div class="confirmTable--null">${register.infants}</div> <hr>Confirmados<br>
                                                    <div class="confirmTable">${confirmedRegister.infantsConfirmed}</div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-4">
                            <div class="dashboard__card">
                                <div class="dashboard__card__title bg-primary"><i class="icon-location_city"></i> Hospedaje</div>
                                <div class="dashboard__card__info d-flex">
                                    <table class="table table-bordered dashboard__table dashboard__table--small table-responsive">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col" class="text-center">Solteros</th>
                                                <th scope="col" class="text-center">Familiar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><span class="confirmTable--null">${lodging.singles}</span><hr>Confirmados<br><div class="confirmTable">${confirmedLodging.singles}</div></td>
                                                <td><span class="confirmTable--null">${lodging.shared}</span> <hr>Confirmados<br><div class="confirmTable">${confirmedLodging.shared}</div></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="dashboard__balance">
                    <div class="dashboard__card dashboard__card--fullwidth">
                        <div class="dashboard__card__title bg-primary"><i class="icon-spoon-knife"></i> Solcitud de Comidas</div>
                        <div class="dashboard__card__info d-flex justify-content-center">

                            <table class="table table-striped table-bordered dashboard__table dashboard__table--small table-responsive">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col" class="text-center" width="25%">Día</th>
                                        <th scope="col" class="text-center t__column__secondary"><i class="icon-person"></i> Adultos</th>
                                        <th scope="col" class="text-center t__column__secondary"><i class="icon-face"></i> Niños</th>
                                        <th scope="col" class="text-center t__column__secondary"><i class="icon-face"></i> Infantes</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-left">
                                            <strong>V3 - Viernes Cena</strong>
                                        </td>
                                        <td>
                                            <div class="d-flex justify-content-center align-items-center">
                                                <span class="confirmTable">${confirmedMeals.ffri3_a}</span>
                                                <span>de</span>
                                                <span class="confirmTable--null">${meals.ffri3_a}</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex justify-content-center align-items-center">
                                                <span class="confirmTable">${confirmedMeals.ffri3_n}</span>
                                                <span>de</span>
                                                <span class="confirmTable--null">${meals.ffri3_n}</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex justify-content-center align-items-center">
                                                <span class="confirmTable">${confirmedMeals.ffri3_i}</span>
                                                <span>de</span>
                                                <span class="confirmTable--null">${meals.ffri3_i}</span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">
                                            <strong>S1 - Sábado Desayuno</strong>
                                        </td>
                                        <td>
                                            <div class="d-flex justify-content-center align-items-center">
                                                <span class="confirmTable">${confirmedMeals.fsat1_a}</span>
                                                <span>de</span>
                                                <span class="confirmTable--null">${meals.fsat1_a}</span>
                                            </div>  
                                        </td>
                                        <td>  
                                            <div class="d-flex justify-content-center align-items-center">                                          
                                                <span class="confirmTable">${confirmedMeals.fsat1_n}</span>
                                                <span>de</span>
                                                <span class="confirmTable--null">${meals.fsat1_n}</span>
                                            </div>  
                                        </td>
                                        <td>
                                            <div class="d-flex justify-content-center align-items-center"> 
                                                <span class="confirmTable">${confirmedMeals.fsat1_i}</span>
                                                <span>de</span>
                                                <span class="confirmTable--null">${meals.fsat1_i}</span>
                                            </div>  
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">
                                            <strong>S2 - Sábado Comida</strong>
                                        </td>
                                        <td>
                                            <div class="d-flex justify-content-center align-items-center">
                                                <span class="confirmTable">${confirmedMeals.fsat2_a}</span>
                                                <span>de</span>
                                                <span class="confirmTable--null">${meals.fsat2_a}</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex justify-content-center align-items-center"> 
                                                <span class="confirmTable">${confirmedMeals.fsat2_n}</span>
                                                <span>de</span>
                                                <span class="confirmTable--null">${meals.fsat2_n}</span> 
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex justify-content-center align-items-center">
                                                <span class="confirmTable">${confirmedMeals.fsat2_i}</span>
                                                <span>de</span>
                                                <span class="confirmTable--null">${meals.fsat2_i}</span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">
                                            <strong>S3 - Sábado Cena</strong>
                                        </td>
                                        <td>
                                            <div class="d-flex justify-content-center align-items-center">
                                                <span class="confirmTable">${confirmedMeals.fsat3_a}</span>
                                                <span>de</span>
                                                <span class="confirmTable--null">${meals.fsat3_a}</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex justify-content-center align-items-center">
                                                <span class="confirmTable">${confirmedMeals.fsat3_n}</span>
                                                <span>de</span>
                                                <span class="confirmTable--null">${meals.fsat3_n}</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex justify-content-center align-items-center">
                                                <span class="confirmTable">${confirmedMeals.fsat3_i}</span>
                                                <span>de</span>
                                                <span class="confirmTable--null">${meals.fsat3_i}</span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">
                                            <strong>D1 - Domingo Desayuno</strong>
                                        </td>
                                        <td>
                                            <div class="d-flex justify-content-center align-items-center">
                                                <span class="confirmTable">${confirmedMeals.fsun1_a}</span>
                                                <span>de</span>
                                                <span class="confirmTable--null">${meals.fsun1_a}</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex justify-content-center align-items-center">
                                                <span class="confirmTable">${confirmedMeals.fsun1_n}</span>
                                                <span>de</span>
                                                <span class="confirmTable--null">${meals.fsun1_n}</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex justify-content-center align-items-center">
                                                <span class="confirmTable">${confirmedMeals.fsun1_i}</span>
                                                <span>de</span>
                                                <span class="confirmTable--null">${meals.fsun1_i}</span> 
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">
                                            <strong>D2 - Domingo Comida</strong>
                                        </td>
                                        <td>
                                            <div class="d-flex justify-content-center align-items-center">
                                                <span class="confirmTable">${confirmedMeals.fsun2_a}</span>
                                                <span>de</span>
                                                <span class="confirmTable--null">${meals.fsun2_a}</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex justify-content-center align-items-center">
                                                <span class="confirmTable">${confirmedMeals.fsun2_n}</span>
                                                <span>de</span>
                                                <span class="confirmTable--null">${meals.fsun2_n}</span>  
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex justify-content-center align-items-center">
                                                <span class="confirmTable">${confirmedMeals.fsun2_i}</span>
                                                <span>de</span>
                                                <span class="confirmTable--null">${meals.fsun2_i}</span> 
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">
                                            <strong>D3 - Domingo Cena</strong>
                                        </td>
                                        <td>
                                            <div class="d-flex justify-content-center align-items-center">
                                                <span class="confirmTable">${confirmedMeals.fsun3_a}</span>
                                                <span>de</span>
                                                <span class="confirmTable--null">${meals.fsun3_a}</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex justify-content-center align-items-center">
                                                <span class="confirmTable">${confirmedMeals.fsun3_n}</span> 
                                                <span>de</span> 
                                                <span class="confirmTable--null">${meals.fsun3_n}</span> 
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex justify-content-center align-items-center">
                                                <span class="confirmTable">${confirmedMeals.fsun3_i}</span>
                                                <span>de</span> 
                                                <span class="confirmTable--null">${meals.fsun3_i}</span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">
                                            <strong>L1 - Lunes Desayuno</strong>
                                        </td>
                                        <td>
                                            <div class="d-flex justify-content-center align-items-center">
                                                <span class="confirmTable">${confirmedMeals.fmon1_a}</span>
                                                <span>de</span>
                                                <span class="confirmTable--null">${meals.fmon1_a}</span>   
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex justify-content-center align-items-center">
                                                <span class="confirmTable">${confirmedMeals.fmon1_n}</span>
                                                <span>de</span>
                                                <span class="confirmTable--null">${meals.fmon1_n}</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex justify-content-center align-items-center">
                                                <span class="confirmTable">${confirmedMeals.fmon1_i}</span>
                                                <span>de</span>
                                                <span class="confirmTable--null">${meals.fmon1_i}</span> 
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="dashboard__balance">
                    <div class="dashboard__card dashboard__card--fullwidth">
                        <div class="dashboard__card__title bg-primary"><i class="icon-attach_money"></i>Resumen de
                            cuentas</div>
                        <div class="dashboard__card__info">
                            <div class="dashboard__balance__global">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6 text-center">
                                        <div class="dashboard__card__type">Total Cobrado</div>
                                        <div class="dashboard__card__total">$
                                            <fmt:formatNumber value="${liason.totalCollected}" minFractionDigits="2"/>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6 text-center">
                                        <div class="dashboard__card__type">Total en Cortesía</div>
                                        <div class="dashboard__card__total bg-warning">$
                                            <fmt:formatNumber value="${liason.totalDiscounted}" minFractionDigits="2"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex justify-content-center">
                                <div class="dashboard__balance__list">
                                    <table class="table table-striped table-bordered dashboard__table table-responsive">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col" class="text-center">Encargado de cobro</th>
                                                <th scope="col" class="text-center">Total Cobrado</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${liason.liasonsNormal}" var="liasonlist">
                                                <tr>
                                                    <td>${liasonlist.name}</td>
                                                    <td class="text-right">$
                                                        <fmt:formatNumber value="${liasonlist.amount}" minFractionDigits="2"/>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                                <c:if test="${fn:length(liason.liasonsDiscount) > 0 }">
                                    <div class="dashboard__balance__list">
                                        <table class="table table-striped table-bordered dashboard__table">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th scope="col" class="text-center">Encargado de cobro</th>
                                                    <th scope="col" class="text-center">Total en cortesía</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach items="${liason.liasonsDiscount}" var="liasonlist">
                                                    <tr>
                                                        <td>${liasonlist.name}</td>
                                                        <td class="text-right">$ 
                                                            <fmt:formatNumber value="${liasonlist.amount}" minFractionDigits="2"/>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <jsp:include page="../layout/footer.jsp" />

</body>

</html>