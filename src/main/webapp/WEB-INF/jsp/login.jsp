<html>

<head>
    <%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
    <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
    <jsp:include page="layout/head-elements.jsp">
        <jsp:param name="title" value="Ingreso" />
        <jsp:param name="description" value="Ingresa tus datos para ingresar" />
    </jsp:include>
</head>

<body class="login">
    <jsp:include page="layout/header.jsp" />

    <div class="page__container">
        <div class="container">
            <div class="page__head">
                <h1 class="page__head__title p-4">Ingresa a tu cuenta</h1>
            </div>
            <div class="page__content bordered">
                <div class="page__form">
                    <form name="loginForm" id="loginForm" action="/login" method="POST">
                        <sec:csrfInput />

                        <c:if test="${error}">
                            <div class="row m-3">
                                <div class="col-sm-12">
                                    <div class="alert alert-danger justify-content-center" role="alert">
                                        Usuario y/o contraseña incorrectas
                                    </div>
                                </div>
                            </div>
                        </c:if>
                        <c:if test="${messageCorreoConfirmado}">
                            <div class="row m-3">
                                <div class="col-sm-12">
                                    <div class="alert alert-success justify-content-center" role="alert">
                                        <b>¡Bienvenido!</b> Tu registro ha sido confirmado.<br>Te acabamos de enviar un correo electrónico con instrucciones para agregar a tus invitados y solicitar tus servicios.
                                    </div>
                                </div>
                            </div>
                        </c:if>
                        <c:if test="${param.logout != null}">
                            <div class="row m-3"> 
                                <div class="col-sm-12">
                                    <div class="alert alert-warning justify-content-center" role="alert">
                                        Terminaste sesión exitosamente
                                    </div>
                                </div>
                            </div>
                        </c:if>
                        <c:if test="${not empty message}">
                            <div class="row m-3"> 
                                <div class="col-sm-12">
                                    <div class="alert alert-success justify-content-center" role="alert">
                                        ${message}
                                    </div>
                                </div>
                            </div>
                        </c:if>
                        <div class="row m-3 d-flex align-items-start">
                            
                            <div class="col-sm-12 col-md-5 d-flex align-items-center"><i class="icon-mail_outline"></i> Correo electrónico: </div>
                            <div class="col-sm-12 col-md-7"><input type="text" name="username"
                                    placeholder="Ingresa el correo registrado" class="form-control"></div>
                        </div>
                        <div class="row m-3 d-flex align-items-start">
                        
                            <div class="col-sm-12 col-md-5 d-flex align-items-center"><i class="icon-lock_outline"></i>Contraseña: </div>
                            <div class="col-sm-12 col-md-7"><input type="password" name="password" placeholder="********"
                                    class="form-control"></div>
                        </div>
                        <div class="row m-3">
                            <div class="col-sm-12 d-flex justify-content-between align-items-center">
                                <a class="page__link" href="/password/recupera-contrasena-page">¿Olvidaste tu contraseña?</a>
                                <input type="button" value="Ingresar" class="btn btn-primary btn-lg m-2" id="sendLogin">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <jsp:include page="layout/footer.jsp" />

    <script>
        $(document).ready(function () {

            $("#sendLogin").on("click", function(){
                if(validateLogin()){
                    $("#loginForm").submit();
                }
            });

            //Function ValidateLogin
            function validateLogin(){
                var selector = $("#loginForm");
                selector.validate({
                    rules: {
                        username: {
                            required: true,
                            email: true
                        },
                        password: {
                            required: true,
                            nospacesonly: true,
                            minlength: 8
                        }
                    },
                    messages: {
                        username: {
                            required: "El correo es obligatorio.",
                            email: "El formato de correo no es válido"
                        },
                        password: {
                            required: "La contraseña es obligatoria.",
                            nospacesonly: "Sólo espacios vacios no es permitido",
                            minlength: "La contraseña debe tener al menos 8 caracteres."
                        }
                    }
                });
                return selector.valid();
            }
        });
    </script>
</body>

</html>

