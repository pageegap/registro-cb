<html>

<head>
    <%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
    <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
    <jsp:include page="../layout/head-elements.jsp">
        <jsp:param name="title" value="Registro de Usuario" />
        <jsp:param name="description" value="Alta de información de usuario" />
    </jsp:include>
</head>

<body class="signup">
    <jsp:include page="../layout/header.jsp" />
    <div class="page__container">
        <div class="container">
            <div class="page__head">
                <h1 class="page__head__title p-4">Registro de usuario</h1>
            </div>
            <div class="page__content bordered">
                <div class="page__form">
                    <c:if test="${verifyEmailMessage}">
                        <div class="row m-3">
                            <div class="col-sm-12">
                                <div class="alert alert-success justify-content-center" role="alert">
                                    <b>¡Estás a un paso de activar tu cuenta!</b><br> Haz clic en el enlace enviado a tu
                                    correo electrónico para completar tu registro y poder acceder al sistema.
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${error}">
                        <div class="row m-3">
                            <div class="col-sm-12">
                                <div class="alert alert-danger justify-content-center" role="alert">
                                    ${errorMessage}
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <form name="register_form" id="registerForm" action="/crea-cuenta" method="POST">
                        <sec:csrfInput />
                        <div class="row m-3">
                            <div class="col-sm-12">
                                <p class="h6 p-2 colorbase1"><strong>IMPORTANTE:</strong> Necesitas una cuenta de correo
                                    electrónico personal, asegúrate de poder enviar y recibir correos ya que te
                                    estaremos enviando información importante sobre tu registro a la conferencia a
                                    través de este medio.</p>
                            </div>
                        </div>
                        <div class="row m-3">

                            <div class="col-sm-1 col-md-1 d-none d-sm-block"><i class="icon-face"></i></div>
                            <div class="col-sm-5 col-md-5"><input type="text" name="firstName" class="form-control"
                                    placeholder="Nombre(s)"></div>

                            <div class="col-sm-6 col-md-6"><input type="text" name="lastName" class="form-control"
                                    placeholder="Apellido(s)"></div>
                        </div>
                        <div class="row m-3">

                            <div class="col-sm-1 col-md-1 d-none d-sm-block"><i class="icon-mail_outline"></i></div>
                            <div class="col-sm-11 col-md-5"><input type="text" name="email" class="form-control"
                                    placeholder="Correo electrónico"></div>
                            <div class="col-sm-1 d-md-none">&nbsp;</div>
                            <div class="col-sm-11 col-md-6" class="select__small">                                
                                <select name="church" >
                                    <option value="Iglesia">Iglesia</option>
                                    <option value="Iglesia del Centro">Iglesia del Centro</option>
                                    <option value="Comunidad Bíblica de la Gracia">Comunidad Bíblica de la Gracia</option>
                                    <option value="Gozo Soberano">Gozo Soberano</option>
                                    <option value="Iglesia Bíblica Sólo por Gracia">Iglesia Bíblica Sólo por Gracia (Toluca)</option>
                                    <option value="Iglesia Bíblica por Gracia"> Iglesia Bíblica por Gracia (Saltillo)</option>
                                    <option value="Iglesia Bíblica Gracia Salvadora"> Iglesia Bíblica Gracia Salvadora</option>
                                    <option value="Otra">Otra</option>
                                </select>
                            </div>
                        </div>
                        <div class="row m-3">

                            <div class="col-sm-1 col-md-1 d-none d-sm-block"><i class="icon-folder_shared"></i></div>
                            <div class="col-sm-3 col-md-3">
                                <select name="country" >
                                    <option value="País"> País</option>
                                    <option value="México">México</option>
                                    <option value="Estados Unidos">Estados Unidos</option>
                                    <option value="Colombia">Colombia</option>
                                    <option value="Otro">Otro</option>
                                </select>

                            </div>

                            <div class="col-sm-3 col-md-3">
                                <select name="state" class="select__small">
                                    <option value="Estado"> Estado</option>
                                    <option value="Aguascalientes">Aguascalientes</option>
                                    <option value="Baja California">Baja California</option>
                                    <option value="Baja California Sur">Baja California Sur</option>
                                    <option value="Chihuahua">Chihuahua</option>
                                    <option value="Coahuila">Coahuila</option>
                                    <option value="Colima">Colima</option>
                                    <option value="Durango">Durango</option>
                                    <option value="Estado de México">Estado de México</option>
                                    <option value="Guerrero">Guerrero</option>
                                    <option value="Hidalgo">Hidalgo</option>
                                    <option value="Jalisco">Jalisco</option>
                                    <option value="Michoacán">Michoacán</option>
                                    <option value="Morelos">Morelos</option>
                                    <option value="Nayarit">Nayarit</option>
                                    <option value="Nuevo León">Nuevo León</option>
                                    <option value="Oaxaca">Oaxaca</option>
                                    <option value="Puebla">Puebla</option>
                                    <option value="Querétaro">Querétaro</option>
                                    <option value="Quintana Roo">Quintana Roo</option>
                                    <option value="San Luis Potosí">San Luis Potosí</option>
                                    <option value="Sinaloa">Sinaloa</option>
                                    <option value="Sonora">Sonora</option>
                                    <option value="Tabasco">Tabasco</option>
                                    <option value="Tamaulipas">Tamaulipas</option>
                                    <option value="Tlaxcala">Tlaxcala</option>
                                    <option value="Veracruz">Veracruz</option>
                                    <option value="Yucatán">Yucatán</option>
                                    <option value="Zacatecas">Zacatecas</option>
                                    <option value="otro">Otro</option>
                                </select>
                            </div>
                            <div class="col-sm-5 col-md-5"><input list="ciudades" type="text" name="city"
                                    class="form-control" placeholder="Ciudad">
                                <datalist id="ciudades">
                                    <option>D.F.</option>
                                    <option>Guadalajara</option>
                                    <option>Querétaro</option>
                                    <option>Toluca</option>
                                    <option>Monterrey</option>
                                    <option>Saltillo</option>
                                    <option>Tepic</option>
                                    <option>Otro</option>
                                </datalist>
                            </div>
                        </div>

                        <div class="row m-3">

                            <div class="col-sm-1 col-md-1 d-none d-sm-block"><i class="icon-lock_outline"></i></div>
                            <div class="col-sm-5 col-md-5"><input type="password" name="password" class="form-control"
                                    id="password" placeholder="Contraseña"></div>
                            <div class="col-sm-6 col-md-6"><input type="password" class="form-control"
                                    name="confirmPassword" placeholder="Repetir contraseña"></div>
                        </div>
                        <div class="row m-3 mt-5">
                            <div class="col-sm-12 d-flex justify-content-end">
                                <input type="button" value="Registrarme" class="btn btn-primary btn-lg m-2"
                                    id="sendSignup">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <jsp:include page="../layout/footer.jsp" />

    <script>
        $(document).ready(function () {

            $("#sendSignup").on("click", function () {
                if (validateSignup()) {
                    $("#registerForm").submit();
                }
            });

            //Function validateSignup
            function validateSignup() {
                var selector = $("#registerForm");
                selector.validate({
                    rules: {
                        firstName: {
                            required: true,
                            nospacesonly: true,
                            minlength: 2
                        },
                        lastName: {
                            required: true,
                            nospacesonly: true,
                            minlength: 2
                        },
                        email: {
                            required: true,
                            email: true
                        },
                        church: {
                            required: true,
                            nospacesonly: true,
                            valueNotEquals: "Iglesia"
                        },
                        country: {
                            required: true,
                            nospacesonly: true,
                            valueNotEquals: "País"
                        },
                        state: {
                            required: true,
                            nospacesonly: true,
                            valueNotEquals: "Estado"
                        },
                        city: {
                            required: true,
                            nospacesonly: true,
                        },
                        password: {
                            required: true,
                            nospace: true,
                            regex: /^(?=.*\d)(?=.*[#$^+=!*()@%&])./,
                            minlength: 8,
                            maxlength: 12
                        },
                        confirmPassword: {
                            required: true,
                            equalTo: "#password"
                        }
                    },
                    messages: {
                        firstName: {
                            required: "Nombre(s) es requerido",
                            nospacesonly: "Sólo espacios no es permitido",
                            minlength: "El nombre parece muy corto"
                        },
                        lastName: {
                            required: "Apellidos(s) es requerido",
                            nospacesonly: "Sólo espacios no es permitido",
                            minlength: "El Apellido parece muy corto"
                        },
                        email: {
                            required: "El correo es requerido",
                            email: "El formato de correo no es válido"
                        },
                        church: {
                            required: "Iglesia es requerida",
                            nospacesonly: "Sólo espacios no es permitido",
                            valueNotEquals: "Seleccione Iglesia"
                        },
                        country: {
                            required: "País es requerido",
                            nospacesonly: "Sólo espacios no es permitido",
                            valueNotEquals: "Seleccione País"
                        },
                        state: {
                            required: "Estado es requerido",
                            nospacesonly: "Sólo espacios no es permitido",
                            valueNotEquals: "Seleccione Estado"
                        },
                        city: {
                            required: "Ciudad es requerida",
                            nospacesonly: "Sólo espacios no es permitido"
                        },
                        password: {
                            required: "La contraseña es requerida",
                            nospace: "No se aceptan espacios",
                            minlength: "Ingrese mínimo 8 caracteres",
                            maxlength: "Ingrese máximo 12 caracteres",
                            regex: "Requiere al menos 1 número, y 1 caracter especial #$^+=!*()@%&"
                        },
                        confirmPassword: {
                            required: "Confirmación de Contraseña es requerido",
                            equalTo: "Las contraseñas no coinciden"
                        }
                    }
                });
                return selector.valid();
            }
        });
    </script>
</body>

</html>