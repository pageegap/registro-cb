<html>

<head>
    <%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
    <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
    <sec:csrfInput />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Error</title>
    <link href="/css/main.css" rel="stylesheet">
    <link href="webjars/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
    <jsp:include page="../layout/header.jsp" />
    <div class="container">
        <div class="register__content">
            <div class="register__form border">
                <div class="register__head border-bottom">
                    <h1 class="register__head__title p-4 bg-secondary text-light">Registro de usuario</h1>
                </div>
                <c:if test="${error}">
                    <div class="alert alert-danger justify-content-center" role="alert">
                        Usuario y/o contraseña incorrectas
                    </div>
                </c:if>
                ERROR!!!!!!!!
            </div>
        </div>
    </div>
    <jsp:include page="../layout/footer.jsp" />
</body>

</html>