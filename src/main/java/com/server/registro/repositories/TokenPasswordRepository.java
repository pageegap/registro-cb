package com.server.registro.repositories;

import com.server.registro.entitites.TokenPassword;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenPasswordRepository extends CrudRepository<TokenPassword, Long> {
    TokenPassword findByConfirmationTokenPassword(String confirmationTokenPassword);

}