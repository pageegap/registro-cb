package com.server.registro.repositories;


import java.util.Set;
import com.server.registro.entitites.LiasonAmount;
import com.server.registro.entitites.Payment;
import com.server.registro.entitites.User;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentRepository extends CrudRepository<Payment, Long> {
    
    Set<Payment> findByDebtor(User debtor);
    Set<Payment> findByLiason(User liason);
    @Query(value = "SELECT new com.server.registro.entitites.LiasonAmount(user.email, SUM(p.amount)) FROM Payment p JOIN p.liason user  JOIN p.paymentType pt WHERE pt.paymentTypeId=?1 GROUP BY  user.email")
    Set<LiasonAmount> getCollectedByLiason(long paymentType);
}