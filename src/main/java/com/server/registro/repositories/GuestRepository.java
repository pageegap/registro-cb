package com.server.registro.repositories;

import com.server.registro.entitites.Guest;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import javax.transaction.Transactional;
import java.util.Set;


@Repository
public interface GuestRepository extends CrudRepository<Guest, Long> {
    public Guest findGuestByGuestId(long guestId);
    
    @Modifying
    @Transactional
    @Query(value="delete from Guest g where g.guestId = ?1")
    void deleteGuestById(Long id);

    @Query(value="SELECT guest from Guest guest JOIN guest.owner user WHERE user.isValid=true and guest.age >=?1 and guest.age <=?2")
    Set<Guest> getAllValidGuestsByAgeRange(int startingAge, int endAge);

    
    @Query(value="SELECT guest from Guest guest JOIN guest.owner user WHERE user.isValid=true and user.settleDebt != null and guest.age >=?1 and guest.age <=?2")
    Set<Guest> getAllValidConfirmedGuestsByAgeRange(int startingAge, int endAge);
}