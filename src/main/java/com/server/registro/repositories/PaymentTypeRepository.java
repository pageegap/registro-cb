package com.server.registro.repositories;

import com.server.registro.entitites.PaymentType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentTypeRepository extends CrudRepository<PaymentType, Long> {  
    PaymentType findPaymentTypeByDescription(String description);
}