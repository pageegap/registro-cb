package com.server.registro.repositories;

import java.util.Set;

import javax.transaction.Transactional;
import com.server.registro.entitites.Purchase;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PurchaseRepository extends CrudRepository<Purchase, Long> {  
    
    @Modifying
    @Transactional
    @Query(value="delete from purchase where guest_id = ?1", nativeQuery = true)
    void deleteAllUserPurchase(Long id);

    @Query(value=" SELECT p from Purchase p JOIN p.guest g where p.code=?1 and g.age >=?2 and g.age <=?3")
    Set<Purchase> getPurchasesByCodeAndAgeRange(int code, int startAge, int endAge);
    
    @Query(value=" SELECT p from Purchase p JOIN p.guest g JOIN g.owner user where  user.settleDebt != null AND p.code=?1 AND g.age >=?2 AND g.age <=?3")
    Set<Purchase> getConfirmedPurchasesByCodeAndAgeRange(int code, int startAge, int endAge);

    //TODO replace for better query
    @Query(value=" SELECT p from Purchase p JOIN p.guest g WHERE p.code =?1 AND g.guestId=?2")
    Purchase getPurchaseByIdAndCode(int code, long id);    
    
}