package com.server.registro.repositories;

import com.server.registro.entitites.Token;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenRepository extends CrudRepository<Token, Long> {  
    Token findByConfirmationToken(String confirmationToken);
}