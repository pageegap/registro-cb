
package com.server.registro.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.server.registro.entitites.MealsReport;

@Repository
public interface MealsReportRepository extends CrudRepository<MealsReport, Long> {

    @Query(value=" SELECT SUM(r.amount) from MealsReport r WHERE r.code=?1 AND r.type=?2")
    Integer getCountByCodeAndAge(int code, int type);

}