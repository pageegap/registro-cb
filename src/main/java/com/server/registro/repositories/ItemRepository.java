package com.server.registro.repositories;

import com.server.registro.entitites.Item;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends CrudRepository<Item, Long> {  
        
    @Query(value = "SELECT item  FROM Item item WHERE item.type = ?1 AND item.initialAge <= ?2  AND item.endAge >= ?2")
    public Item findItemByTypeAndAge(int type, int age);
}