package com.server.registro.repositories;

import java.util.Set;

import com.server.registro.entitites.User;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    @Query("SELECT user FROM User user  WHERE user.isValid=false")
    public Set<User> findAllNonValidatedUsers();

    @Query("SELECT user FROM User user WHERE user.email=?1 and user.isValid=true")
    public User findActiveUserByEmail(String email);

    public User findUserByEmail(String email);

    public User findUserByUserId(Long id);

    @Query("SELECT user FROM User user WHERE user.city=?1 and user.isValid=true")
    public Set<User> findValidUsersByCity(String city);

    @Query("SELECT user FROM User user WHERE user.church=?1 and user.isValid=true")
    public Set<User> findValidUsersByChurch(String church);

    public Set<User> findByIsValid(boolean active);

    @Query(value = "SELECT user FROM User user JOIN  user.guests g JOIN g.items items WHERE user.isValid=true AND items.code = 1000")
    public Set<User> findValidUsersRequestingLodging();

    @Query(value = "SELECT user FROM User user JOIN  user.guests g JOIN g.items items WHERE user.isValid=true AND user.settleDebt != null AND items.code = 1000")
    public Set<User> findConfirmedValidUsersRequestingLodging();

    public Set<User> findAllByIsValidAndSettleDebtIsNotNullOrderBySettleDebtAsc(boolean active);
    
    public Set<User> findAllByIsValid(boolean active);
}