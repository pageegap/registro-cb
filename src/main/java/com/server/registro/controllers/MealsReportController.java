package com.server.registro.controllers;

import java.util.Map;

import com.server.registro.controllers.network.request.MealReportRequest;
import com.server.registro.controllers.network.response.MealsReportResponse;
import com.server.registro.services.MealsReportService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

@RestController
public class MealsReportController {

    private static final Logger LOG = LoggerFactory.getLogger(GuestController.class);

    public MealsReportService mealsReportService;

    public MealsReportController(MealsReportService mealsReportService) {
        this.mealsReportService = mealsReportService;
    }

    @PostMapping(value = "/add-meal-report")
    public MealsReportResponse addGuest(@RequestBody MealReportRequest request) {
        
        return this.mealsReportService.registerReport(request);

    }

    @GetMapping(value = "/check-has-meal")
    public Map<String, Object> checkIfService(@RequestParam(value = "code", required = true) int code,
            @RequestParam(value = "id", required = true) long guestId) {                    
        
        return this.mealsReportService.hasMeal(code, guestId);
        
    }
}