package com.server.registro.controllers;

import com.server.registro.exceptions.NotValidGuestException;
import com.server.registro.exceptions.NotValidPaymentTypeException;
import com.server.registro.exceptions.NotValidUserException;
import javax.mail.MessagingException;
import com.server.registro.exceptions.NotFoundGuestException;
import com.server.registro.exceptions.NotFoundItemException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ControllerAdvice
public class RegistroControllerAdviser {
    private static final Logger LOG = LoggerFactory.getLogger(PaymentController.class);

    @ExceptionHandler({ RuntimeException.class })
    public RegistroRestError handleRunTimeException(RuntimeException e) {
        LOG.info("controller adviser runtime xception");
        e.printStackTrace();
        RegistroRestError response = new RegistroRestError(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        return response;
    }

    @ExceptionHandler(NotValidUserException.class)
    public RegistroRestError handleNotValidUserException(NotValidUserException e) {
        e.printStackTrace();
        LOG.info("notvaliduser exception");
        RegistroRestError response = new RegistroRestError(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        return response;
    }

    @ExceptionHandler(NotValidGuestException.class)
    public RegistroRestError handleNotValidGuestException(NotValidGuestException e) {
        e.printStackTrace();
        LOG.info("notvalidguestexception");
        RegistroRestError response = new RegistroRestError(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        return response;
    }

    @ExceptionHandler(NotFoundItemException.class)
    public RegistroRestError handleNotFoundItemException(NotFoundItemException e) {
        e.printStackTrace();
        LOG.info("notfoundexception");
        RegistroRestError response = new RegistroRestError(HttpStatus.NOT_FOUND, e.getMessage(), e);
        return response;
    }

    @ExceptionHandler(NotFoundGuestException.class)
    public RegistroRestError handleNotFoundItemException(NotFoundGuestException e) {
        e.printStackTrace();
        LOG.info("notfoundguestexception");
        RegistroRestError response = new RegistroRestError(HttpStatus.NOT_FOUND, e.getMessage(), e);
        return response;
    }

    @ExceptionHandler(NotValidPaymentTypeException.class)
    public RegistroRestError handleNotValidPaymentTypeException(NotValidPaymentTypeException e) {
        e.printStackTrace();
        LOG.info("notvalidpayment");
        RegistroRestError response = new RegistroRestError(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        return response;
    }

    @ExceptionHandler(MessagingException.class)
    public RegistroRestError handleSendingEmailFailed(MessagingException e) {
        e.printStackTrace();
        LOG.info("messaginexception");
        RegistroRestError response = new RegistroRestError(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        return response;
    }
}