package com.server.registro.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.server.registro.controllers.network.request.GuestItemRequest;
import com.server.registro.controllers.network.response.GuestSummaryResponse;
import com.server.registro.controllers.network.response.SummaryAccountResponse;
import com.server.registro.services.GuestService;
import com.server.registro.services.UserService;

import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

@RestController
public class GuestController {
    private static final Logger LOG = LoggerFactory.getLogger(GuestController.class);

    private GuestService guestService;
    private UserService userService;
    @Autowired
    public GuestController(GuestService guestService, UserService userService) {
        this.guestService = guestService;
        this.userService = userService;
    }

    @PostMapping(value = "addGuest")
    public SummaryAccountResponse addGuest(@RequestBody GuestItemRequest guest) {
        
        return guestService.addGuest(guest);
    }

    @PostMapping(value = "updateGuest")
    public SummaryAccountResponse updateGuest(@RequestBody GuestItemRequest guest) {
        
        return guestService.updateGuest(guest);
    }

    @DeleteMapping(value = "{id}")
    @Transactional
    public SummaryAccountResponse deleteGuest(@PathVariable(value = "id") long id) {
        return guestService.handleDeleteGuest(id);
    }

    @PostMapping(value="/assign-room")
    public Map<Long,Set<GuestSummaryResponse>> assignRoom2Guest(@RequestBody Map<Long,String> guestRoom){
        guestService.assignRoom2Guest(guestRoom);        
        return getGuestsSortedByPaymentDate();
    }

    
    @PostMapping(value="/update-checkin")
    public Map<Long,Set<GuestSummaryResponse>> updateCheckIn(@RequestBody Map<Long,Boolean> guestCheckIn){
        guestService.updateCheckIn(guestCheckIn);        
        return getGuestsSortedByPaymentDate();
    }
    /**
     * We can do this later on without using userService
     */
    @GetMapping(value = "/recupera-guests-details")    
    public Map<Long,Set<GuestSummaryResponse>> getGuestsSortedByPaymentDate(){
        return userService.getAllValidUsersAndGuests();
    }

    /**
     * We can do this later on without using userService
     */
    @GetMapping(value = "/recupera-guest-info")    
    public Map<Long,Set<GuestSummaryResponse>> getAllGuestInfo(){
        return userService.getAllGuests();
    }
   
}