package com.server.registro.controllers;

import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;



import org.slf4j.Logger;

@Controller
public class GlobalController{
    private static final Logger LOG = LoggerFactory.getLogger(GlobalController.class);
    
    @GetMapping(value = "/inicio")
    public String info(){
        //TODO Load general info about Event 
        return "home";
    }
}