package com.server.registro.controllers;

import java.util.Set;
import javax.mail.MessagingException;
import com.server.registro.controllers.network.request.PaymentRequest;
import com.server.registro.controllers.network.response.PaymentResponse;
import com.server.registro.entitites.User;
import com.server.registro.entitites.CbUser;
import com.server.registro.services.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
public class PaymentController {

    private PaymentService paymentService;
    private static final Logger LOG = LoggerFactory.getLogger(PaymentController.class);

    @Autowired
    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @GetMapping(value = "/liason/registro-pagos-page")
    public String paymentsRegistration(Model model) {
        UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) SecurityContextHolder
                .getContext().getAuthentication();
        User liasonUser = ((CbUser) authentication.getPrincipal()).getUserDetails();
        model.addAttribute("liasonEmail", liasonUser.getEmail());
        return "payments/payments";
    }

    @PostMapping(value = "/liason/registrar-pago")
    @ResponseStatus(value = HttpStatus.OK)
    public void registerPayment(@RequestBody PaymentRequest paymentRequest, Model model) throws MessagingException {
        UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) SecurityContextHolder
                .getContext().getAuthentication();
        User loggedInUser = ((CbUser) authentication.getPrincipal()).getUserDetails();
        paymentRequest.setLiasonEmail(loggedInUser.getEmail());

        paymentService.registerPayment(paymentRequest);
    }

    @GetMapping(value = "/liason/trae-pagos")
    @ResponseBody
    public Set<PaymentResponse> getAllPayments(
            @RequestParam(value = "debtorEmail", required = true) String debtorEmail) {
        LOG.info("received {}", debtorEmail);
        return paymentService.getAllPaymentsByDebtor(debtorEmail);
    }

    @GetMapping(value = "/liason/trae-my-cobros")
    @ResponseBody
    public Set<PaymentResponse> getAllCobros(@RequestParam(value = "liasonEmail", required = true) String liasonEmail) {

        Set<PaymentResponse> paymentResponse = null;
        paymentResponse = paymentService.getAllPaymentsByLiason(liasonEmail);
        return paymentResponse;
    }

}