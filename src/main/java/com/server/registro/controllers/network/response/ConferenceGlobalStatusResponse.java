package com.server.registro.controllers.network.response;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class ConferenceGlobalStatusResponse implements Serializable {

    public Map<String, Integer> register = new HashMap<>();
    public Map<String, Integer> meals = new HashMap<>();
    public Map<String, Integer> lodging = new HashMap<>();
    public Map<String, Object> liasons = new HashMap<>();
    
    // users that have already paid for their requested services
    public Map<String, Integer> confirmedLodging = new HashMap<>();
    public Map<String,Integer> confirmedRegister = new HashMap<>();
    public Map<String,Integer> confirmedMeals = new HashMap<>();

    public ConferenceGlobalStatusResponse(Map<String, Integer> register, Map<String, Integer> meals,
            Map<String, Integer> lodging, Map<String, Object> liasons) {
        this.register = register;
        this.meals = meals;
        this.lodging = lodging;
        this.liasons = liasons;
    }

    public ConferenceGlobalStatusResponse() {
    }
}