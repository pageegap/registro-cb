package com.server.registro.controllers.network.response;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.TimeZone;

import com.server.registro.entitites.Guest;
import com.server.registro.entitites.Payment;
import com.server.registro.entitites.User;



public class PaymentResponse implements Serializable {

    public float amount;
    public String date;    
    public String liason;
    public String type;
    public String debtorName;
    public long idDebtor;
    public String titular;
    public long paymentId;

    public PaymentResponse(Payment payment) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        TimeZone tz = TimeZone.getTimeZone("America/Mexico_City");
        sdf.setTimeZone(tz);
        
        this.amount = payment.getAmount();
        this.date = (payment.getDate() != null) ? sdf.format(payment.getDate()) : "-";
        this.liason = payment.getLiason().getEmail();
        this.idDebtor = payment.getDebtor().getUserId();
        this.type = payment.getPaymentType().getDescription();        
        this.paymentId = payment.getPaymentId();
        User debtor = payment.getDebtor();

        Set<Guest> guests = debtor.getGuests();
        //TODO remove this code repetition!!!
        for(Guest g : guests) {
            if( g.getRelation().equalsIgnoreCase("titular")){
                debtorName =  (g.getFirstName() + " " + g.getLastName());
            }
        }

        debtorName = debtorName + "(" + payment.getDebtor().getUserId()+")";
    }
}