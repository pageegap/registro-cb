package com.server.registro.controllers.network.response;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import com.server.registro.entitites.Guest;
import com.server.registro.entitites.Payment;
import com.server.registro.entitites.User;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import java.util.TimeZone;
/**
 * This class sends all info to the frontedn of the modified account
 */
public class SummaryAccountResponse implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(SummaryAccountResponse.class);
    String pattern = "yyyy-MM-dd";
    
    SimpleDateFormat sdf = new SimpleDateFormat(pattern);                

    public long modifiedGuestId;
    public float total;
    public float amountPaid;
    public float totalDiscount;
    public String email;
    public long ownerId;
    public long userId;
    public String phoneNumber;
    public String city;
    public String state;
    public String country;
    public String church;
    public String settledDebt;

    public Map<Long, GuestSummaryResponse> guests = new HashMap<>();

    public SummaryAccountResponse(User account, Long addedGuestId) {
        this.userId = account.getUserId();
        this.modifiedGuestId = addedGuestId;
        
        this.city = account.getCity();
        this.state = account.getState();
        this.church = account.getChurch();
        this.country = account.getCountry();
        this.phoneNumber = account.getPhoneNumber();
        TimeZone tz = TimeZone.getTimeZone("America/Mexico_City");
        sdf.setTimeZone(tz);
        this.settledDebt = (account.getSettleDebt() != null) ? sdf.format(account.getSettleDebt()): "-";

        total = 0f;
        LOG.info("reading guests {}", account.getGuests().size());
        for (Guest guest : account.getGuests()) {

            if (guest.isOwner()) {
                ownerId = guest.getGuestId();
            }

            GuestSummaryResponse guestSummary = new GuestSummaryResponse(guest);
            this.guests.put(guest.getGuestId(), guestSummary);
            total += guestSummary.total;
        }
        email = account.getEmail();

        Set<Payment> payments = account.getPayments();
        amountPaid = 0f;
        LOG.info("has done {} payments", payments.size());
        for (Payment payment : payments) {
            LOG.info("adding up {} ", payment.getAmount());
            amountPaid += payment.getAmount();
        }
        getTotalPaidAndDiscount(account);               
    }

    public SummaryAccountResponse(User account) {
        total = 0f;
        this.userId = account.getUserId();
        this.city = account.getCity();
        this.state = account.getState();
        this.church = account.getChurch();
        this.country = account.getCountry();
        this.phoneNumber = account.getPhoneNumber();
        TimeZone tz = TimeZone.getTimeZone("America/Mexico_City");
        sdf.setTimeZone(tz);
        this.settledDebt = (account.getSettleDebt() != null) ? sdf.format(account.getSettleDebt()): "-";
        
        for (Guest guest : account.getGuests()) {

            if (guest.isOwner()) {
                ownerId = guest.getGuestId();
            }

            GuestSummaryResponse guestSummary = new GuestSummaryResponse(guest);
            this.guests.put(guest.getGuestId(), guestSummary);
            total += guestSummary.total;
        }
        email = account.getEmail();

        getTotalPaidAndDiscount(account);
        
    }

    private void getTotalPaidAndDiscount(User account) {
        Set<Payment> payments = account.getPayments();
        amountPaid = 0f;
        totalDiscount = 0f;
        

        for (Payment payment : payments) {            
            
            if(payment.getPaymentType().getDescription().equalsIgnoreCase("normal")){
                amountPaid += payment.getAmount();
            }else if(payment.getPaymentType().getDescription().equalsIgnoreCase("cortesia")) {
                totalDiscount += payment.getAmount();
            }
        }

    }
}