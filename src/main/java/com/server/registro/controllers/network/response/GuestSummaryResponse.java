package com.server.registro.controllers.network.response;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Set;
import java.util.TimeZone;
import com.server.registro.entitites.Guest;
import com.server.registro.entitites.Purchase;
import com.server.registro.entitites.User;

public class GuestSummaryResponse implements Serializable {

    public long guestId;
    public String firstName;
    public String lastName;
    public String phoneNumber;
    public String gender;
    public int age;
    public String maritalStatus;
    public String note;
    public String relation;
    public boolean isPregnant = false;
    public boolean isMedicated = false;
    public boolean hasDisability = false;
    public boolean samePhoneNumberAsOwner = false;
    public boolean isOwner = false;
    public boolean lodging = false;
    public boolean foodFriday1 = false;
    public boolean foodSaturday1 = false;
    public boolean foodSaturday2 = false;
    public boolean foodSaturday3 = false;
    public boolean foodSunday1 = false;
    public boolean foodSunday2 = false;
    public boolean foodSunday3 = false;
    public boolean foodMonday1 = false;
    public boolean transportationFriday = false;
    public boolean transportationSaturday = false;
    public boolean transportationMonday = false;
    public float total = 0f;
    public String room;
    public String checkin;
    public long userId;
    public String userEmail;
    public String settleDebt;
    public String church;
    public String city;
    public String state;
    public String country;

    public GuestSummaryResponse(Guest guest) {

        guestId = guest.getGuestId();
        firstName = guest.getFirstName();
        lastName = guest.getLastName();
        gender = guest.getGender();
        age = guest.getAge();
        maritalStatus = guest.getMaritalStatus();
        note = guest.getNote();
        relation = guest.getRelation();
        isPregnant = guest.isPregnant();
        isMedicated = guest.isMedicated();
        hasDisability = guest.isHasDisability();
        isOwner = guest.isOwner();
        setItemsByCode(guest.getItems());

        for (Purchase purchase : guest.getItems()) {
            total += purchase.getCost();
        }
    }

    // This is a workaround. We need to find a better way to do this
    private void setItemsByCode(Set<Purchase> purchases) {

        for (Purchase p : purchases) {
            int code = p.getCode();
            switch (code) {
            case 200:
                foodSaturday1 = true;
                break;

            case 300:
                foodSunday1 = true;
                break;

            case 400:
                foodMonday1 = true;
                break;

            case 500:
                foodSaturday2 = true;
                break;

            case 600:
                foodSunday2 = true;
                break;

            case 700:
                foodFriday1 = true;
                break;

            case 800:
                foodSaturday3 = true;
                break;

            case 900:
                foodSunday3 = true;
                break;

            case 1000:
                lodging = true;
                break;

            case 1100:
                transportationFriday = true;
                break;

            case 1200:
                transportationSaturday = true;
                break;

            case 1300:
                transportationMonday = true;
                break;
            }
        }
    }

    public GuestSummaryResponse() {

    }

    public static GuestSummaryResponse createGuestSummary4Lodging(Guest guest, User user) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        TimeZone tz = TimeZone.getTimeZone("America/Mexico_City");
        sdf.setTimeZone(tz);

        GuestSummaryResponse summary = new GuestSummaryResponse(guest);

        summary.guestId = guest.getGuestId();
        summary.firstName = guest.getFirstName();
        summary.lastName = guest.getLastName();
        summary.gender = guest.getGender();
        summary.age = guest.getAge();
        summary.maritalStatus = guest.getMaritalStatus();
        summary.note = guest.getNote();
        summary.relation = guest.getRelation();
        summary.isPregnant = guest.isPregnant();
        summary.isMedicated = guest.isMedicated();
        summary.hasDisability = guest.isHasDisability();
        summary.isOwner = guest.isOwner();
        summary.room = guest.getRoom();
        
        summary.userId = user.getUserId();
        summary.userEmail = user.getEmail();
        summary.settleDebt = (user.getSettleDebt() != null) ? sdf.format(user.getSettleDebt()) : "-";
        summary.checkin = (guest.getCheckinDate() != null) ? sdf.format(guest.getCheckinDate()) : "-";
        summary.church = user.getChurch();
        summary.country = user.getCountry();
        summary.state = user.getState();
        summary.city = user.getCity();
        //setItemsByCode(guest.getItems());
        
        return summary;
    }

    @Override
    public String toString() {
        return "GuestSummaryResponse [age=" + age + ", checkin=" + checkin + ", firstName=" + firstName
                + ", foodFriday1=" + foodFriday1 + ", foodMonday1=" + foodMonday1 + ", foodSaturday1=" + foodSaturday1
                + ", foodSaturday2=" + foodSaturday2 + ", foodSaturday3=" + foodSaturday3 + ", foodSunday1="
                + foodSunday1 + ", foodSunday2=" + foodSunday2 + ", foodSunday3=" + foodSunday3 + ", gender=" + gender
                + ", guestId=" + guestId + ", hasDisability=" + hasDisability + ", isMedicated=" + isMedicated
                + ", isOwner=" + isOwner + ", isPregnant=" + isPregnant + ", lastName=" + lastName + ", lodging="
                + lodging + ", maritalStatus=" + maritalStatus + ", note=" + note + ", phoneNumber=" + phoneNumber
                + ", relation=" + relation + ", room=" + room + ", samePhoneNumberAsOwner=" + samePhoneNumberAsOwner
                + ", settleDebt=" + settleDebt + ", total=" + total + ", transportationFriday=" + transportationFriday
                + ", transportationMonday=" + transportationMonday + ", transportationSaturday="
                + transportationSaturday + ", userEmail=" + userEmail + ", userId=" + userId + "]";
    }

   

}