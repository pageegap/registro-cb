package com.server.registro.controllers.network.request;

import java.io.Serializable;


/**
 * We use this bean to request adding a new 'service'
 */
public class GuestItemRequest implements Serializable {

    //account or owner
    private String ownerEmail;
    // personal guest information 
    private long guestId;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String gender;
    private String age;
    private String maritalStatus;
    private String city;
    private String state;
    private String country;
    private String church;
    private String note;
    private String relation;
    private boolean pregnant;
    private boolean isMedicated;
    private boolean hasDisability;
    private boolean samePhoneNumberAsOwner;
    private boolean owner;
    // services requested for this guest
    private boolean lodging;

    private boolean foodFriday1;
    private boolean foodSaturday1;
    private boolean foodSaturday2;
    private boolean foodSaturday3;
    private boolean foodSunday1;
    private boolean foodSunday2;
    private boolean foodSunday3;
    private boolean foodMonday1;

    private boolean transportationFriday;
    private boolean transportationSaturday;
    private boolean transportationMonday;
    
    
    

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

   
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getChurch() {
        return church;
    }

    public void setChurch(String church) {
        this.church = church;
    }

   
  

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public boolean isLodging() {
        return lodging;
    }

    public void setLodging(boolean lodging) {
        this.lodging = lodging;
    }

    public boolean isFoodFriday1() {
        return foodFriday1;
    }

    public void setFoodFriday1(boolean foodFriday1) {
        this.foodFriday1 = foodFriday1;
    }

    public boolean isFoodSaturday1() {
        return foodSaturday1;
    }

    public void setFoodSaturday1(boolean foodSaturday1) {
        this.foodSaturday1 = foodSaturday1;
    }

    public boolean isFoodSaturday2() {
        return foodSaturday2;
    }

    public void setFoodSaturday2(boolean foodSaturday2) {
        this.foodSaturday2 = foodSaturday2;
    }

    public boolean isFoodSaturday3() {
        return foodSaturday3;
    }

    public void setFoodSaturday3(boolean foodSaturday3) {
        this.foodSaturday3 = foodSaturday3;
    }

    public boolean isFoodSunday1() {
        return foodSunday1;
    }

    public void setFoodSunday1(boolean foodSunday1) {
        this.foodSunday1 = foodSunday1;
    }

    public boolean isFoodSunday2() {
        return foodSunday2;
    }

    public void setFoodSunday2(boolean foodSunday2) {
        this.foodSunday2 = foodSunday2;
    }

    public boolean isFoodSunday3() {
        return foodSunday3;
    }

    public void setFoodSunday3(boolean foodSunday3) {
        this.foodSunday3 = foodSunday3;
    }

    public boolean isFoodMonday1() {
        return foodMonday1;
    }

    public void setFoodMonday1(boolean foodMonday1) {
        this.foodMonday1 = foodMonday1;
    }

    public boolean isTransportationFriday() {
        return transportationFriday;
    }

    public void setTransportationFriday(boolean transportationFriday) {
        this.transportationFriday = transportationFriday;
    }

    public boolean isTransportationSaturday() {
        return transportationSaturday;
    }

    public void setTransportationSaturday(boolean transportationSaturday) {
        this.transportationSaturday = transportationSaturday;
    }

    public boolean isTransportationMonday() {
        return transportationMonday;
    }

    public void setTransportationMonday(boolean transportationMonday) {
        this.transportationMonday = transportationMonday;
    }

    public long getGuestId() {
        return guestId;
    }

    public void setGuestId(long id) {
        this.guestId = id;
    }

    public boolean isPregnant() {
        return pregnant;
    }

    public void setPregnant(boolean pregnant) {
        this.pregnant = pregnant;
    }

    public boolean isMedicated() {
        return isMedicated;
    }

    public void setIsMedicated(boolean isMedicated) {
        this.isMedicated = isMedicated;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public boolean isHasDisability() {
        return hasDisability;
    }

    public void setHasDisability(boolean hasDisability) {
        this.hasDisability = hasDisability;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    public boolean isSamePhoneNumberAsOwner() {
        return samePhoneNumberAsOwner;
    }

    public void setSamePhoneNumberAsOwner(boolean samePhoneNumberAsOwner) {
        this.samePhoneNumberAsOwner = samePhoneNumberAsOwner;
    }

    public boolean isOwner() {
        return owner;
    }

    public void setOwner(boolean owner) {
        this.owner = owner;
    }


    public void setMedicated(boolean isMedicated) {
        this.isMedicated = isMedicated;
    }

    
}