package com.server.registro.controllers.network.request;

import java.io.Serializable;

public class PaymentRequest implements Serializable { 
    
    private String debtorEmail;
    private String liasonEmail;
    private float amount;
    private String typeDesc;

    public String getDebtorEmail() {
        return debtorEmail;
    }

    public void setDebtorEmail(String debtorEmail) {
        this.debtorEmail = debtorEmail;
    }

    public String getLiasonEmail() {
        return liasonEmail;
    }

    public void setLiasonEmail(String liasonEmail) {
        this.liasonEmail = liasonEmail;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getTypeDesc() {
        return typeDesc;
    }

    public void setTypeDesc(String typeDesc) {
        this.typeDesc = typeDesc;
    }

    @Override
    public String toString() {
        return "PaymentRequest [amount=" + amount + ", debtorEmail=" + debtorEmail + ", liasonEmail=" + liasonEmail
                + ", typeDesc=" + typeDesc + "]";
    }

    

}