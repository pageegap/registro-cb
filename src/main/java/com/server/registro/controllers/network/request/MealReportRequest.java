package com.server.registro.controllers.network.request;

import java.io.Serializable;

public class MealReportRequest implements Serializable {

   public  int code;
   public  int infants;
   public int adults; 
   public int children;
   public String note;

    @Override
    public String toString() {
        return "MealReportRequest [adults=" + adults + ", children=" + children + ", code=" + code + ", infants="
                + infants + ", note=" + note + "]";
    }
   

}