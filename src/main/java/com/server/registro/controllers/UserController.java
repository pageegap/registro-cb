package com.server.registro.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.server.registro.services.ChangePasswordService;
import com.server.registro.services.GuestService;
import com.server.registro.services.UserService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.mail.MessagingException;
import javax.servlet.http.HttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import com.server.registro.controllers.network.request.*;
import com.server.registro.controllers.network.response.ConferenceGlobalStatusResponse;
import com.server.registro.controllers.network.response.GuestSummaryResponse;
import com.server.registro.controllers.network.response.SummaryAccountResponse;
import com.server.registro.entitites.User;
import com.server.registro.entitites.CbUser;
import com.server.registro.exceptions.FailedToInstantiateUserException;
import com.server.registro.exceptions.FailedToResetPasswordException;
import com.server.registro.exceptions.NotValidTokenException;
import com.server.registro.exceptions.NotValidUserException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;

@Controller
public class UserController {

    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

    private UserService userService;
    private GuestService guestService;
    private ChangePasswordService changePasswordService;

    @ExceptionHandler(MessagingException.class)
    public ModelAndView handleError(HttpServletRequest req, Exception ex) {
        LOG.error("Request: " + req.getRequestURL() + " raised " + ex);

        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", ex);
        mav.addObject("url", req.getRequestURL());
        mav.addObject("error", true);
        mav.addObject("errorMessage", "Falló el envio de correo");
        mav.setViewName("signup/signupForm");
        return mav;
    }

    @Autowired
    public UserController(UserService userService, GuestService guestService,
            ChangePasswordService changePasswordService) {
        this.userService = userService;
        this.guestService = guestService;
        this.changePasswordService = changePasswordService;
    }

    @GetMapping(value = "/crea-cuenta-page")
    public String register() {
        return "signup/signupForm";
    }

    @PostMapping(value = "/crea-cuenta")
    public String createPreUser(RegistrationRequest newUserForm, Model model) throws MessagingException {

        try {
            // LOG.info("what i got from UI {}", newUserForm);
            userService.handleRegistration(newUserForm);
            model.addAttribute("verifyEmailMessage", true);

        } catch (NotValidUserException | FailedToInstantiateUserException e) {
            e.printStackTrace();
            LOG.info(e.getMessage());
            model.addAttribute("error", true);
            model.addAttribute("errorMessage", e.getMessage());
        }
        return "signup/signupForm";
    }

    @GetMapping(value = "/confirma-email")
    public String validateToken(@RequestParam(value = "token", required = true) String token, Model model)
            throws MessagingException {
        String page = "login";

        try {
            userService.validateUserToken(token);
            model.addAttribute("messageCorreoConfirmado", true);
        } catch (NotValidTokenException | FailedToInstantiateUserException e) {
            e.printStackTrace();
            model.addAttribute("error", true);
            model.addAttribute("errorMessage", e.getMessage());
            return "signup/signupForm";
        }

        return page;
    }

    @GetMapping(value = "/recupera-invitados")
    @ResponseBody
    public SummaryAccountResponse getAccount(@RequestParam(value = "email", required = true) String email) {
        return this.userService.allGuests(email);
    }

    @GetMapping(value = "/perfil-cuenta-page")
    public String getProfileHomePage(Model model) {
        UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) SecurityContextHolder
                .getContext().getAuthentication();
        User loggedInUser = ((CbUser) authentication.getPrincipal()).getUserDetails();
        LOG.info(" logged user is {}", loggedInUser.getEmail());
        return "profiles/profileForm";
    }

    @GetMapping(value = "/recupera-resumen-cuentas")
    @ResponseBody
    public Set<SummaryAccountResponse> getAllAccounts(@RequestParam(value = "filter", required = false) String filter,
            @RequestParam(value = "value", required = false) String value) {

        Set<SummaryAccountResponse> result = new HashSet<>();

        if (filter.equalsIgnoreCase("email") || filter.equalsIgnoreCase("id")) {
            result = userService.getAccountByFilter(filter, value);
        } else {
            LOG.info("using {}", filter);
            result = userService.getAccountsByFilter(filter, value);
        }

        return result;

    }

    @GetMapping(value = "/login")
    public String testLogin() {
        return "login";
    }

    @GetMapping(value = "/loginFailed")
    public String loginError(Model model) {
        model.addAttribute("error", true);
        model.addAttribute("errorMessage", "Usuario y/o contraseñas no válidos");
        return "login";
    }

    @PostMapping(value = "/postLogin")
    public String postLogin(Model model, HttpSession session) {
        UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) SecurityContextHolder
                .getContext().getAuthentication();
        User loggedInUser = ((CbUser) authentication.getPrincipal()).getUserDetails();
        session.setAttribute("currentUser", loggedInUser.getEmail());
        return "profiles/profileForm";
    }

    @GetMapping(value = "/edita-perfil")
    public String editaPerfil(Model model, HttpSession session,
            @RequestParam(value = "email", required = false) String email) {
        UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) SecurityContextHolder
                .getContext().getAuthentication();
        User loggedInUser = ((CbUser) authentication.getPrincipal()).getUserDetails();
        session.setAttribute("currentUser", loggedInUser.getEmail());
        model.addAttribute("userEmail", email);

        return "profiles/edition";
    }

    @GetMapping(value = "/dashboard")
    public String getConferenceStatus(Model model) {

        ConferenceGlobalStatusResponse globalInfo = guestService.getConferenceStatus();
        model.addAttribute("register", globalInfo.register);
        model.addAttribute("meals", globalInfo.meals);
        model.addAttribute("lodging", globalInfo.lodging);
        model.addAttribute("liason", globalInfo.liasons);
        // already paid services
        model.addAttribute("confirmedRegister", globalInfo.confirmedRegister);
        model.addAttribute("confirmedMeals", globalInfo.confirmedMeals);
        model.addAttribute("confirmedLodging", globalInfo.confirmedLodging);

        return "admin/dashboard";
    }

    @GetMapping(value = "/password/recupera-contrasena-page")
    public String passwordRecovery() {
        return "profiles/passwordRecovery";
    }

    @PostMapping(value = "/password/solicita-nueva-contrasena")
    public String passwordRecoveryRequest(@RequestParam(value = "email", required = true) String email, Model model) {

        try {
            LOG.info(" solicitar nueva contrasena {} ", email);
            changePasswordService.CreateTokenPassword(email);
            model.addAttribute("email", email);
            model.addAttribute("message",
                    "Acabamos de enviarte un mensaje de confirmación a tu correo electrónico. Haz clic en el enlace enviado para crear tu nueva contraseña.");
        } catch (MessagingException | NotValidUserException e) {

            LOG.info(e.getMessage());
            model.addAttribute("error", true);
            model.addAttribute("errorMessage", e.getMessage());
            LOG.info("error !!! {}", e);
            e.printStackTrace();
        }
        return "profiles/passwordRecovery";

    }

    @GetMapping(value = "/password/confirma-new-password")
    public String validateTokenPassword(@RequestParam(value = "tknp", required = true) String token, Model model) {

        try {
            changePasswordService.validateUserTokenPassword(token);
            // we send back the token.
            model.addAttribute("token", token);

        } catch (MessagingException | NotValidTokenException e) {
            LOG.info("error !!! {}", e);
            model.addAttribute("error", e.getMessage());
            e.printStackTrace();
            return "login";
        }
        return "profiles/passwordReset";
    }

    @PostMapping(value = "/password/cambia-contrasena")
    public String passwordResetSave(ChangingPasswordRequest newPasswordReq, Model model) throws NotValidTokenException {

        try {
            LOG.info("changin password {}", newPasswordReq);
            changePasswordService.handlePasswordReset(newPasswordReq);
            model.addAttribute("message", "Tu contraseña ha sido actualizada");
        } catch (MessagingException | NotValidUserException | FailedToResetPasswordException e) {
            model.addAttribute("errorMessage", e.getMessage());
            e.printStackTrace();
        }
        return "login";
    }

    @GetMapping(value = "/edita-cuenta")
    @ResponseBody
    public SummaryAccountResponse getAccount(@RequestParam(value = "criteria", required = true) String criteria,
            @RequestParam(value = "user", required = true) String user) {
        if (criteria.equalsIgnoreCase("email")) {
            return this.userService.allGuests(user);
        } else if (criteria.equalsIgnoreCase("id")) {
            return this.userService.allGuests(Long.parseLong(user));
        }
        return null;
    }

    @GetMapping(value = "/guest-details-page")
    public String getAllGuestDetails() {
        return "admin/roomasignment";
    }

    @GetMapping(value="/guest-checkin-page")
    public String getAllGuestsInfo(){
        return "admin/checkin";
    }

    @GetMapping(value="/meals-report-page")
    public String getMealsReport(){
        return "admin/mealscontrol";
    }
}
