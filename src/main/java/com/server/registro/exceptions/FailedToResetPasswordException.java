package com.server.registro.exceptions;

public class FailedToResetPasswordException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public FailedToResetPasswordException(String message) {
        super(message);
    }
}