package com.server.registro.exceptions;


public class NotFoundGuestException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    
    public NotFoundGuestException(String message){
        super(message);
    }
}