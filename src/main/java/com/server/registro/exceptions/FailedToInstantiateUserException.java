package com.server.registro.exceptions;


public class FailedToInstantiateUserException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    
    public FailedToInstantiateUserException(String message){
        super(message);
    }
}