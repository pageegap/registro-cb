package com.server.registro.exceptions;

public class NotValidPaymentTypeException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    public NotValidPaymentTypeException(String message){
        super(message);
    }

}