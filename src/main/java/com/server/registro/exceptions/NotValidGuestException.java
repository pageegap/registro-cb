package com.server.registro.exceptions;


public class NotValidGuestException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    
    public NotValidGuestException(String message){
        super(message);
    }
}