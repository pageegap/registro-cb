package com.server.registro.exceptions;


public class NotValidTokenException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    
    public NotValidTokenException(String message){
        super(message);
    }
}