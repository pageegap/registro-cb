package com.server.registro.exceptions;


public class NotFoundItemException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    
    public NotFoundItemException(String message){
        super(message);
    }
}