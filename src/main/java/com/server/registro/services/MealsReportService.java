package com.server.registro.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

import com.server.registro.controllers.network.request.MealReportRequest;
import com.server.registro.controllers.network.response.MealsReportResponse;
import com.server.registro.entitites.MealsReport;
import com.server.registro.entitites.Purchase;
import com.server.registro.repositories.MealsReportRepository;
import com.server.registro.repositories.PurchaseRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class MealsReportService {

    private static final Logger LOG = LoggerFactory.getLogger(PaymentService.class);
    public MealsReportRepository mealsReportRepository;
    public PurchaseRepository purchaseRepository;

    @Autowired
    public MealsReportService(MealsReportRepository mealsReportRepository, PurchaseRepository purchaseRepository) {
        this.mealsReportRepository = mealsReportRepository;
        this.purchaseRepository = purchaseRepository;
    }


    public Map<String, Object> hasMeal(int code , long id) {
        Map<String,Object> result = new HashMap<>();
        Purchase purchase = this.purchaseRepository.getPurchaseByIdAndCode(code, id);        
        boolean hasMeal = purchase == null? false : true;
        if (purchase != null) {
            String name = purchase.getGuest().getFirstName() + purchase.getGuest().getLastName();
            result.put("name",name);
        }        
        result.put("message", hasMeal);
        return result;
    }
    
    public MealsReportResponse registerReport(MealReportRequest reportReq) {
        MealsReportResponse response = new MealsReportResponse();
        int code = reportReq.code;
        if (reportReq.code != -1 && (reportReq.adults != 0 || reportReq.infants != 0 || reportReq.children != 0)) {

            MealsReport reportInfants = new MealsReport();
            reportInfants.setAmount(reportReq.infants);
            reportInfants.setCode(code);
            reportInfants.setNote(reportReq.note);
            reportInfants.setType(0);// for infants

            MealsReport reportChildren = new MealsReport();
            reportChildren.setAmount(reportReq.children);
            reportChildren.setCode(code);
            reportChildren.setNote(reportReq.note);
            reportChildren.setType(1);// for children

            MealsReport reportAdults = new MealsReport();
            reportAdults.setAmount(reportReq.adults);
            reportAdults.setCode(code);
            reportAdults.setNote(reportReq.note);
            reportAdults.setType(2);// for adults

            mealsReportRepository.save(reportInfants);
            mealsReportRepository.save(reportChildren);
            mealsReportRepository.save(reportAdults);

        }

        int codeQuery = code == -1 ? 700 : code;

        Integer totalInfants = mealsReportRepository.getCountByCodeAndAge(codeQuery, 0);
        Integer totalChildren = mealsReportRepository.getCountByCodeAndAge(codeQuery, 1);
        Integer totalAdults = mealsReportRepository.getCountByCodeAndAge(codeQuery, 2);

        response.adults = totalAdults != null ? totalAdults : 0;
        response.infants = totalInfants != null ? totalInfants : 0;
        response.children = totalChildren != null ? totalChildren : 0;
        response.code = codeQuery;        

        return response;
    }
}