package com.server.registro.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

@Service("emailSenderService")
public class EmailSenderService {

    private static final Logger LOG = LoggerFactory.getLogger(EmailSenderService.class);

    // constant values
    private String EMAIL_HOST = "mail.smtp.host";
    private String EMAIL_PORT = "mail.smtp.port";
    private String EMAIL_AUTH = "mail.smtp.auth";
    private String EMAIL_SOCKET_FACTORY_PORT = "mail.smtp.socketFactory.port";
    private String EMAIL_SOCKET_FACTORY_CLASS = "mail.smtp.socketFactory.class";
    private String SSL_SOCKET_FACTORY = "javax.net.ssl.SSLSocketFactory";

    // sender email parameters
    @Value("${spring.mail.username}")
    private String fromEmail;
    @Value("${spring.mail.password}")
    private String password;
    @Value("${spring.mail.host}")
    private String smptHost;
    @Value("${spring.mail.port}")
    private String port;
    @Value("${spring.mail.properties.mail.smtp.auth}")
    private String isAuth;
    @Value("${server.service.email}")
    private String isServiceEmail;

    @Autowired
    public EmailSenderService() {

    }

    public void sendEmail(String targetEmail, String subject, String msg) throws MessagingException {
        
        
        if(isServiceEmail.equalsIgnoreCase("false")){ 
            LOG.info("**********  Email service is down!!!");
            return;
        }

        Properties prop = new Properties();
        prop.put(EMAIL_HOST, smptHost.trim());
        prop.put(EMAIL_PORT, port.trim());
        prop.put(EMAIL_AUTH, isAuth.trim());
        prop.put(EMAIL_SOCKET_FACTORY_PORT, port.trim());
        prop.put(EMAIL_SOCKET_FACTORY_CLASS, SSL_SOCKET_FACTORY);

        Session session = Session.getInstance(prop, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(fromEmail, password);
            }
        });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(fromEmail));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(targetEmail));
            message.setSubject(subject);
            message.setContent(msg, "text/html; charset=utf-8");
            Transport.send(message);

        } catch (MessagingException e) {
            e.printStackTrace();
            throw e;
        }

        LOG.debug("sent message to {} containing {}", targetEmail, msg);
    }
}