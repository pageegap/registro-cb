package com.server.registro.services;

import com.server.registro.entitites.Payment;
import com.server.registro.entitites.Token;
import com.server.registro.entitites.TokenPassword;
import com.server.registro.entitites.User;

public class EmailMessagesBuilder {

    public static final String SUBJECT_RECIBIMOS_PAGO = "Notificación de Pago recibido ";
    public static final String SUBJECT_VALIDA_EMAIL = "Activa tu cuenta - Validación de correo";
    public static final String SUBJECT_BIENVENIDO = "Bienvenido";

    public static String getConfirmationEmailMessage(Token token, String serverAddress) {
        StringBuilder sb = new StringBuilder();
        sb.append(
                "<p><img src=\"http://www.cristianismobiblico.com/wp-content/uploads/2015/01/C2C-reducido-3.jpg\" alt=\"CBlogo\" /></p>");
        sb.append("<hr></hr>");
        sb.append("<div><div>¡Hola!</div><div>&nbsp;</div>");
        sb.append(
                "<div>Alguien usó este correo para registrarse en el sistema de conferencias de <strong>Cristianismo Bíblico</strong>.&nbsp;Para completar tu registro y acceder al sistema, da click en el siguiente enlace:<br>");
        sb.append("<a href=\"https://"+serverAddress+"/confirma-email?token="+token.getConfirmationToken()+"\">"); 
        sb.append("https://");
        sb.append(serverAddress);        
        sb.append("/confirma-email?token=");
        sb.append(token.getConfirmationToken());
        sb.append("</a>");
        sb.append("</div><div>&nbsp;</div>");
        sb.append("<p><span style=\"color: #0000ff;\">Este enlace será válido únicamente por 12 horas.</span></p></div>");
        sb.append("<p><span style=\"color: #0000ff;\">Si tu no solicitaste el registro, ignora este correo.</span></p></div>");
        return sb.toString();
    }

    public static String getWelcomeMessage(User user) {
        StringBuilder sb = new StringBuilder();
        sb.append(
                "<p><img src=\"http://www.cristianismobiblico.com/wp-content/uploads/2015/01/C2C-reducido-3.jpg\" alt=\"CBlogo\" /></p>");
        sb.append("<hr></hr>");
        sb.append(
                "<p><strong>Felicidades</strong>, te has registrado existosamente&nbsp; en el sistema de conferencias de <b>Cristianismo Bíblico</b>.</p>");
        sb.append("<p><strong>Tu nombre de usuario para ingresar al sistema es:&nbsp;[ " + user.getEmail()
                + " ]</strong></p>");
        sb.append("<p><strong>Tu <b>ID de usuario</b> es:&nbsp;[ " + user.getUserId() + " ]</strong><br>Con este número de identificación podrás registrar pagos o dar seguimiento a tu registro.</p>");
        sb.append("<p>Ahora podrás dar de alta invitados y solicitar servicios ");
        sb.append("para lo cual te agradecemos que atiendas a las siguientes indicaciones:</p>");
        sb.append("<ul>");
        sb.append("<li><b>No se registre en grupo</b>, realícelo de manera individual a menos que asista como familia y requiere compartir habitación.</li>");
        sb.append("<li>Solteros deben registrarse individualmente</li>");
        sb.append("<li><b>No está asegurado tu lugar, comida, ni estadía hasta no haber efectuado el pago total de tu registro</b>. Si solicitó servicios de estadía la habitación será asignada hasta que se haya cubierto el <b>pago total</b> de tu registro.</li>");
        sb.append("<li>Para pagar puedes contactar al <b>Encargado de cobro</b> más cercano a ti. <b>Ten a la mano tu número de identificación (ID de usuario)</b> para proporcionárselo. La lista con la información de estos contactos la encontrarás en la página de Registro.</li>");
        sb.append("<li>Una vez realizado el <b>PAGO TOTAL</b> de su cuenta no podrá realizar cambios a su registro de asistentes. En este caso deberá contactar a su <b>Encargado de cobro</b> más cercano en caso de requerir modificaciones.</li>");
        sb.append("<li>El ingreso recaudado por inscripciones y servicios será usado en su totalidad para cubrir los gastos de la conferencia.</li>");
        sb.append("<li><b>Cristianismo Bíblico</b> no persigue ningún fin de lucro.</li>");
        sb.append("<li><b>En caso de cancelación por causas de la magnitud y logistica del evento no se hará devolución de los pagos ya efectuados.</b> Si por algún motivo o circunstancia <b>NO PODRÁ ASISTIR</b> a la conferencia y ya cubrió parcial o totalmente el costo, es <b>NECESARIO</b> que por favor NOTIFIQUE a su encargado de cobro</li>");
        sb.append("</ul><p>¡Gracias!</p>");
        return sb.toString();
    }

    public static String paymentConfirmation(Payment payment, User user, float balance) {
        StringBuilder sb = new StringBuilder();
        String paymentType = payment.getPaymentType().getDescription();
        
        sb.append(
                "<p><img src=\"http://www.cristianismobiblico.com/wp-content/uploads/2015/01/C2C-reducido-3.jpg\" alt=\"CBlogo\" /></p>");
        sb.append("<hr></hr>");        
        sb.append("<p>Se abonó a tu cuenta:  $<b>" + payment.getAmount() +"</b></p>");
        sb.append("<p>Tipo de abono: "+ paymentType+"</p>");
        sb.append("<p>Folio para seguimiento: [ <b>"+ payment.getPaymentId() +"</b> ]</p>");
        sb.append("<p>Saldo: $"+ balance +"</p>");
        sb.append("<p><span style=\"color: #0000ff;\">Para cualquier aclaración de tus pagos ten a la mano tu número de Folio y tu ID de Usuario</span></p></div>");
        return sb.toString();
    }

    public static String getEmailMessageTokenPassword(TokenPassword tokenPassword, String serverAddress) {
        StringBuilder sb = new StringBuilder();
        sb.append(
                "<p><img src=\"http://www.cristianismobiblico.com/wp-content/uploads/2015/01/C2C-reducido-3.jpg\" alt=\"CBlogo\" /></p>");
        sb.append("<hr></hr>");
        sb.append("<p>¡Hola!</p>");
        sb.append("<p>Para cambiar tu contrase&ntilde;a en el sistema de registro de conferencias de <strong>Cristianismo B&iacute;blico</strong> da click en el siguiente enlace:</p>");
        sb.append("<a href=\"https://"+serverAddress+"/password/confirma-new-password?tknp="+tokenPassword.getConfirmationTokenPassword()+"\">");
        sb.append("https://");
        sb.append(serverAddress);        
        sb.append("/password/confirma-new-password?tknp=");
        sb.append(tokenPassword.getConfirmationTokenPassword());
        sb.append("</a>");
        sb.append("<p><span style=\"color: #0000ff;\">Este enlace será válido únicamente por 12 horas.</span></p></div>");
        sb.append("<p><span style=\"color: #0000ff;\">Si tu no solicitaste la recuperación de tu contraseña, ignora este correo.</span></p>");
        return sb.toString();
    }
}