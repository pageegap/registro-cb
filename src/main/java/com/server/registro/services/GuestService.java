package com.server.registro.services;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.transaction.Transactional;
import com.server.registro.controllers.network.request.GuestItemRequest;
import com.server.registro.controllers.network.response.ConferenceGlobalStatusResponse;
import com.server.registro.controllers.network.response.SummaryAccountResponse;
import com.server.registro.entitites.Guest;
import com.server.registro.entitites.Purchase;
import com.server.registro.entitites.User;
import com.server.registro.exceptions.NotFoundGuestException;
import com.server.registro.exceptions.NotFoundItemException;
import com.server.registro.exceptions.NotValidGuestException;
import com.server.registro.repositories.GuestRepository;
import com.server.registro.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

@Service
public class GuestService {

    private static final Logger LOG = LoggerFactory.getLogger(GuestService.class);

    private GuestRepository guestRepository;
    private UserRepository userRepository;
    private PurchaseService purchaseService;
    private PaymentService paymentService;

    @Autowired
    public GuestService(GuestRepository guestRepository, UserRepository userRepository, PurchaseService purchaseService,
            PaymentService paymentService) {
        this.guestRepository = guestRepository;
        this.userRepository = userRepository;
        this.purchaseService = purchaseService;
        this.paymentService = paymentService;

    }

    /**
     * adds a new guest and requested items
     * 
     * @param guestRequest
     * @throws NotValidGuestException
     * @throws NotFoundItemException
     */
    @Transactional
    public SummaryAccountResponse addGuest(GuestItemRequest guestRequest)
            throws NotValidGuestException, NotFoundItemException {

        if (guestRequest == null) {
            throw new NotValidGuestException("La solicitud es nula ");
        }

        String ownerEmail = guestRequest.getOwnerEmail();

        if (ownerEmail == null || ownerEmail.isEmpty()) {
            throw new NotValidGuestException("user's email es null o vacia, corrige");
        }

        User account = userRepository.findActiveUserByEmail(ownerEmail);

        if (account == null) { // no valid account for this guest
            throw new NotValidGuestException("el email " + ownerEmail + "no corresponde a una cuenta valida");
        }

        if (guestRequest.isOwner()) {
            throw new NotValidGuestException("solo puede haber un titular por cuenta");
        }

        // we create a new guest
        Guest newGuest = new Guest();
        setGuestInfo(newGuest, guestRequest, account);
        newGuest.setOwner(account);
        // we save the guest to get an id
        Guest guest = guestRepository.save(newGuest);

        // we created a guest, we add its items
        purchaseService.addGuestItems(guest, guestRequest);

        // we return guest's id and account's summary
        account = userRepository.save(account);
        account.getGuests().add(guest);
        updateSettledDebtDate(account);
        return new SummaryAccountResponse(account, guest.getGuestId());
    }

    /**
     * Updates any existing guest.
     * 
     * @param guestRequest
     * @throws NotFoundGuestException
     * @throws NotValidGuestException
     * @throws NotFoundItemException
     */
    public SummaryAccountResponse updateGuest(GuestItemRequest guestRequest)
            throws NotFoundGuestException, NotValidGuestException, NotFoundItemException {

        // guest's id comes in request
        long guestId = guestRequest.getGuestId();
        Guest guest = guestRepository.findGuestByGuestId(guestId);

        if (guest == null) {// check guest is valid/active?
            throw new NotFoundGuestException(" el id  " + guestId + " no fue encontrado");
        }

        User account = guest.getOwner();
        setGuestInfo(guest, guestRequest, account);

        purchaseService.addGuestItems(guest, guestRequest);
        updateSettledDebtDate(account);
        account = userRepository.findActiveUserByEmail(account.getEmail());

        return new SummaryAccountResponse(account, guest.getGuestId());
    }

    private void setGuestInfo(Guest guest, GuestItemRequest guestRequest, User owner) throws NotValidGuestException {

        // this is important since determines the services we will add
        String ageReq = guestRequest.getAge();

        if (ageReq == null || ageReq.isEmpty()) {// will ovewrite/write with no correct value
            throw new NotValidGuestException("La edad del invitado no puede ser vacia o nula ");
        }

        int age = Integer.parseInt(ageReq);
        String firstName = guestRequest.getFirstName();
        String lastName = guestRequest.getLastName();
        String gender = guestRequest.getGender();

        String relation = guestRequest.getRelation();
        String note = guestRequest.getNote();

        boolean pregnant = guestRequest.isPregnant();
        boolean isMedicated = guestRequest.isMedicated();
        boolean hasDisability = guestRequest.isHasDisability();
        String maritalStatus = guestRequest.getMaritalStatus();
        String phoneNumber = guestRequest.getPhoneNumber();

        // user/account values
        String city = guestRequest.getCity().toUpperCase();

        String country = guestRequest.getCountry().toUpperCase();

        String state = guestRequest.getState().toUpperCase();
        String church = guestRequest.getChurch().toUpperCase();

        guest.setFirstName(firstName);
        guest.setLastName(lastName);
        guest.setAge(age);
        guest.setGender(gender);

        if (!guestRequest.isOwner()) {
            guest.setRelation(relation);
        }
        guest.setNote(note);

        guest.setPregnant(pregnant);
        guest.setMedicated(isMedicated);
        guest.setHasDisability(hasDisability);
        guest.setMaritalStatus(maritalStatus);

        // TODO validate these fields
        if (guestRequest.isOwner()) {
            LOG.info("updating owner's data {} {} {} {}", church, city, state, country);
            owner.setChurch(church);
            owner.setCity(city);
            owner.setState(state);
            owner.setCountry(country);

            if (phoneNumber != null && !phoneNumber.isEmpty()) {
                owner.setPhoneNumber(phoneNumber);

            }

        }

    }

    /**
     * To delete we just need the guest id
     * 
     * @param ownerEmail
     * @param guest
     * @throws NotFoundGuestException
     */
    @Transactional
    public SummaryAccountResponse handleDeleteGuest(Long guestId) throws NotFoundGuestException {

        Guest guest = guestRepository.findGuestByGuestId(guestId);
        LOG.info("found guest ? {}", (guest != null));
        if (guest == null) {
            throw new NotFoundGuestException(" El invitado " + guestId + " no existe, no se puede borrar");
        }

        String email = guest.getOwner().getEmail();
        // Spring Boot does the deletion at the end of transaction. We have to compute
        // this manually
        float balance = paymentService.getUserBalance(guest.getOwner(), guest);

        // to avoid constraint issue, delete all related items first
        purchaseService.deleteAllItems(guest);
        guestRepository.deleteGuestById(guestId);
        User account = userRepository.findActiveUserByEmail(email);

        if (balance <= 0 && account.getSettleDebt() == null) {
            account.setSettleDebt(new Date());
        } else if (balance > 0) {
            account.setSettleDebt(null);
        }
        userRepository.save(account);

        SummaryAccountResponse summary = new SummaryAccountResponse(account, guestId);
        return summary;
    }

    public void createTitular(User preUser, String firstName, String lastName) {

        Guest titular = new Guest();
        titular.setAge(18);
        titular.setFirstName(firstName);
        titular.setLastName(lastName);
        titular.setOwner(true);
        titular.setOwner(preUser);
        titular.setRelation("titular");

        guestRepository.save(titular);
    }

    private boolean wannaSharedRoom(Set<Guest> guests) {
        int howMany = 0;

        for (Guest g : guests) {
            for (Purchase p : g.getItems()) {
                if (p.getCode() == 1000) {// guest is requesting a room
                    howMany++;
                    break;
                }
            }
            if (howMany > 1) {
                return true;
            }
        }

        return false;
    }

    private void setSingleAndSharedRequests(Set<User> users, Map<String, Integer> lodgingReqs) {
        int singles = 0;
        int shared = 0;
        for (User u : users) {
            if (wannaSharedRoom(u.getGuests())) {
                shared++;
            } else {
                singles++;
            }
        }
        lodgingReqs.put("singles", singles);
        lodgingReqs.put("shared", shared);

    }

    public Map<String, Integer> getLodgingRequests() {
        Map<String, Integer> lodgingReqs = new HashMap<>();
        Set<User> usersReqLodging = userRepository.findValidUsersRequestingLodging();
        setSingleAndSharedRequests(usersReqLodging, lodgingReqs);
        return lodgingReqs;
    }

    public Map<String, Integer> getConfirmedLodgingRequests() {
        Map<String, Integer> lodgingReqs = new HashMap<>();
        Set<User> usersReqLodging = userRepository.findConfirmedValidUsersRequestingLodging();
        setSingleAndSharedRequests(usersReqLodging, lodgingReqs);
        return lodgingReqs;
    }

    private void updateSettledDebtDate(User account) {
        float balance = paymentService.getUserBalance(account);
        LOG.info("balance  account {} after adding is {}", account.getEmail(), balance);

        if (balance > 0) {
            account.setSettleDebt(null);
        } else if (balance <= 0 && account.getSettleDebt() == null) {
            account.setSettleDebt(new Date());
        }
        userRepository.save(account);
    }

    public ConferenceGlobalStatusResponse getConferenceStatus() {

        ConferenceGlobalStatusResponse status = new ConferenceGlobalStatusResponse();
        Map<String, Integer> registrationStatus = new HashMap<>();
        Map<String, Integer> confirmedRegistrationStatus = new HashMap<>();

        Set<Guest> allGuests = guestRepository.getAllValidGuestsByAgeRange(0, 300);
        Set<Guest> adults = guestRepository.getAllValidGuestsByAgeRange(18, 300);
        Set<Guest> eleven2eigtheen = guestRepository.getAllValidGuestsByAgeRange(11, 17);
        Set<Guest> five2ten = guestRepository.getAllValidGuestsByAgeRange(5, 10);
        Set<Guest> o2four = guestRepository.getAllValidGuestsByAgeRange(0, 4);
        // confirmed users since they alredy settled their debt
        Set<Guest> allConfirmedGuests = guestRepository.getAllValidConfirmedGuestsByAgeRange(0, 300);
        Set<Guest> confirmedAdults = guestRepository.getAllValidConfirmedGuestsByAgeRange(18, 300);
        Set<Guest> confirmedEleven2eigtheen = guestRepository.getAllValidConfirmedGuestsByAgeRange(11, 17);        
        Set<Guest> confirmedFive2Ten = guestRepository.getAllValidConfirmedGuestsByAgeRange(5, 10);
        Set<Guest> confirmedOh2Four = guestRepository.getAllValidConfirmedGuestsByAgeRange(0, 4);

        registrationStatus.put("infants", o2four.size());
        registrationStatus.put("children", five2ten.size());
        registrationStatus.put("adults", eleven2eigtheen.size() + adults.size());
        registrationStatus.put("all", allGuests.size());
        
        confirmedRegistrationStatus.put("infantsConfirmed",confirmedOh2Four.size());
        confirmedRegistrationStatus.put("childrenConfirmed",confirmedFive2Ten.size());
        confirmedRegistrationStatus.put("adultsConfirmed", confirmedEleven2eigtheen.size() + confirmedAdults.size());
        confirmedRegistrationStatus.put("allConfirmed", allConfirmedGuests.size());

        Map<String, Integer> mealRequests = purchaseService.getMealRequests();
        Map<String, Integer> confirmedMealRequests = purchaseService.getConfirmedMealRequests();

        Map<String, Integer> lodgingRequests = getLodgingRequests();
        Map<String, Integer> lodgingConfirmedRequests = getConfirmedLodgingRequests();

        Map<String, Object> liasons = paymentService.getCollectedByLiasonReport();

        status.register = registrationStatus;
        status.confirmedRegister = confirmedRegistrationStatus;

        status.meals = mealRequests;
        status.confirmedMeals = confirmedMealRequests;

        status.lodging = lodgingRequests;
        status.confirmedLodging = lodgingConfirmedRequests;

        status.liasons = liasons;

        return status;
    }

    /**
     * 
     * @param guestRoom
     */
    public void assignRoom2Guest(Map<Long, String> guestRoom) {

        for (Long guestId : guestRoom.keySet()) {
            Guest guest = guestRepository.findGuestByGuestId(guestId);
            String room = guestRoom.get(guestId);
            if (guest != null && !room.isEmpty()) {
                guest.setRoom(room);
                guestRepository.save(guest);
            }
        }
    }

    public void updateCheckIn(Map<Long, Boolean> guestDetail) {

        for (Long guestId : guestDetail.keySet()) {
            Guest guest = guestRepository.findGuestByGuestId(guestId);
            Boolean isCheckedIn = guestDetail.get(guestId);

            if (guest == null) {
                continue;
            }
            if (guest.getCheckinDate() != null && isCheckedIn) {
                continue;
            }

            if (guest.getCheckinDate() == null && !isCheckedIn) {
                continue;
            }

            Date checkInDate = isCheckedIn ? new Date() : null;

            guest.setCheckinDate(checkInDate);
            guestRepository.save(guest);

        }
    }
}