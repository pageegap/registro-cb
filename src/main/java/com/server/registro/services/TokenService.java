package com.server.registro.services;

import java.util.Calendar;
import java.util.Date;
import com.server.registro.entitites.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class TokenService {

    private static final Logger LOG = LoggerFactory.getLogger(TokenService.class);
    /** This value is customizable in properties and defines the token Lifetime */
    @Value("${token.lifetime.mins}")
    private String tokenAvailabilityMins;

    /**
     * This method checks if user's token has expired
     * @param user
     * @return true if user's token has expired
     */
    public boolean isUserTokenExpired(User user) {

        Calendar cal = Calendar.getInstance();
        int tokenAvailability = Integer.parseInt(tokenAvailabilityMins);
        Date expiringDate;
        Date currentDate;
        Date tokenCreationDate;
        boolean isTokenExpired;

        LOG.info("checking user's token {}", user.getEmail());

        tokenCreationDate = user.getValidationToken().getCreatedDate();

        cal.setTime(tokenCreationDate);
        cal.add(Calendar.MINUTE, tokenAvailability);

        expiringDate = cal.getTime();
        currentDate  = new Date();
        isTokenExpired = currentDate.after(expiringDate);

        LOG.info("current {} and expiring {}, has token expired {}", currentDate, expiringDate, isTokenExpired);

        return isTokenExpired;
    }

}