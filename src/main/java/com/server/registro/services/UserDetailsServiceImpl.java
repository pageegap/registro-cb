package com.server.registro.services;

import com.server.registro.entitites.CbUser;
import com.server.registro.entitites.User;
import com.server.registro.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private static final Logger LOG = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findActiveUserByEmail(email);

        if (user == null) {
            throw new UsernameNotFoundException("User: " + email + " not found.");
        }

        LOG.info("loadUserByUsername() : {}", email);
        return new CbUser(user);
    }

}