package com.server.registro.services;

import java.util.Calendar;
import java.util.Date;
import com.server.registro.entitites.TokenPassword;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class TokenPasswordService {

    private static final Logger LOG = LoggerFactory.getLogger(TokenService.class);
    /** This value is customizable in properties and defines the token Lifetime */
    @Value("${token.lifetime.mins}")
    private String tokenLifetimeMins;

    /**
     * This method checks if user's token has expired
     * @param tokenPassword
     * @return true if user's token has expired
     */
    public boolean isUserTokenPasswordExpired(TokenPassword tokenPassword) {

        Calendar cal = Calendar.getInstance();
        int tokenAvailability = Integer.parseInt(tokenLifetimeMins);
        Date expiringDate;
        Date currentDate;
        Date tokenCreationDate;
        boolean isTokenExpired;

        

        tokenCreationDate = tokenPassword.getCreatedDate();

        cal.setTime(tokenCreationDate);
        cal.add(Calendar.MINUTE, tokenAvailability);

        expiringDate = cal.getTime();
        currentDate  = new Date();
        isTokenExpired = currentDate.after(expiringDate);

       

        return isTokenExpired;
    }

}