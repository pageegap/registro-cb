package com.server.registro.services;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.transaction.Transactional;
import com.server.registro.controllers.network.request.GuestItemRequest;
import com.server.registro.entitites.Guest;
import com.server.registro.entitites.Purchase;
import com.server.registro.entitites.Item;
import com.server.registro.exceptions.NotFoundItemException;
import com.server.registro.repositories.PurchaseRepository;
import com.server.registro.repositories.GuestRepository;
import com.server.registro.repositories.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

@Service
public class PurchaseService {

    private static final Logger LOG = LoggerFactory.getLogger(Purchase.class);
    private PurchaseRepository purchaseRepository;
    private ItemRepository itemRepository;
    private GuestRepository guestRepository;

    @Autowired
    public PurchaseService(PurchaseRepository guestItemRepository, ItemRepository itemRepository,
            GuestRepository guestRepository) {
        this.purchaseRepository = guestItemRepository;
        this.itemRepository = itemRepository;
        this.guestRepository = guestRepository;
    }

    /**
     * Adds items to a given guest
     * 
     * @param guestId      guest id
     * @param itemsRequest items this guest is requesting
     * @throws NotFoundItemException if item requested does not exist
     */
    public void addGuestItems(Guest guest, GuestItemRequest itemsRequest) throws NotFoundItemException {

        Set<Purchase> items = new HashSet();

        // delete previous items selection
        deleteAllItems(guest);
        // add mandatory registration item
        items.add(addItem(guest, 100));

        // add lodging if requested
        if (itemsRequest.isLodging()) {
            items.add(addItem(guest, 1000));
        }

        // add meal items
        addMealItems(guest, itemsRequest, items);
        //add transportation items
        addTransportationItems(guest, itemsRequest, items);

        guest.setItems(items);

        // update guest
        guestRepository.save(guest);
    }

    /**
     * 
     * @param guest
     * @param items
     * @param itemsSet
     * @throws NotFoundItemException
     */
    private void addMealItems(Guest guest, GuestItemRequest items, Set<Purchase> itemsSet)
            throws NotFoundItemException {

        // breakfast
        if (items.isFoodSaturday1()) {
            itemsSet.add(addItem(guest, 200));
        }
        if (items.isFoodSunday1()) {
            itemsSet.add(addItem(guest, 300));
        }
        if (items.isFoodMonday1()) {
            itemsSet.add(addItem(guest, 400));
        }

        // lunch
        if (items.isFoodSaturday2()) {
            itemsSet.add(addItem(guest, 500));
        }
        if (items.isFoodSunday2()) {
            itemsSet.add(addItem(guest, 600));
        }

        // dinners
        if (items.isFoodFriday1()) {
            itemsSet.add(addItem(guest, 700));
        }
        if (items.isFoodSaturday3()) {
            itemsSet.add(addItem(guest, 800));
        }
        if (items.isFoodSunday3()) {
            itemsSet.add(addItem(guest, 900));
        }

    }

    /**
     * 
     * @param guest
     * @param items
     * @param itemsSet
     * @throws NotFoundItemException
     */
    private void addTransportationItems(Guest guest, GuestItemRequest items, Set<Purchase> itemsSet)
            throws NotFoundItemException {

        if (items.isTransportationFriday()) {
            itemsSet.add(addItem(guest, 1100));
        }
        if (items.isTransportationSaturday()) {
            itemsSet.add(addItem(guest, 1200));
        }
        if (items.isTransportationMonday()) {
            itemsSet.add(addItem(guest, 1300));
        }
    }

    /**
     * 
     * @param guest
     * @param type
     * @return
     * @throws NotFoundItemException
     */
    private Purchase addItem(Guest guest, int type) throws NotFoundItemException {

        Purchase purchase = new Purchase();

        Item item = itemRepository.findItemByTypeAndAge(type, guest.getAge());
        LOG.info("item found {}", item);
        if (item == null) {
            throw new NotFoundItemException(" item " + type + " was not found");
        }

        purchase.setAdded(new Date());
        purchase.setCode(type);
        purchase.setCost(item.getPrice());
        purchase.setDescription(item.getName());
        purchase.setGuest(guest);

        return purchase;
    }

    /**
     * 
     */
    @Transactional
    public void deleteAllItems(Guest guest) {
        purchaseRepository.deleteAllUserPurchase(guest.getGuestId());
    }

   
    public Map<String,Integer> getMealRequests(){
        Map<String, Integer> mealsRequests = new HashMap<>();

        // saturday breakfast 200 for ages in [11,300]
        Set<Purchase> adult200 = purchaseRepository.getPurchasesByCodeAndAgeRange(200,11,300);
        Set<Purchase> child200 = purchaseRepository.getPurchasesByCodeAndAgeRange(200,5,10);
        Set<Purchase> infants200 = purchaseRepository.getPurchasesByCodeAndAgeRange(200,0,4);
        mealsRequests.put("fsat1_a", adult200.size());
        mealsRequests.put("fsat1_n", child200.size());
        mealsRequests.put("fsat1_i", infants200.size());
        // sunday breakfast 300 for ages in [11,300]
        Set<Purchase> adult300 = purchaseRepository.getPurchasesByCodeAndAgeRange(300,11,300);
        Set<Purchase> child300 =  purchaseRepository.getPurchasesByCodeAndAgeRange(300,5,10);
        Set<Purchase> infants300 =  purchaseRepository.getPurchasesByCodeAndAgeRange(300,0,4);
        mealsRequests.put("fsun1_a", adult300.size());
        mealsRequests.put("fsun1_n", child300.size());
        mealsRequests.put("fsun1_i", infants300.size());
        // monday breakfast  400 for ages in [11,300]
        Set<Purchase> adult400 = purchaseRepository.getPurchasesByCodeAndAgeRange(400,11,300);
        Set<Purchase> child400 = purchaseRepository.getPurchasesByCodeAndAgeRange(400,5,10);
        Set<Purchase> infants400 = purchaseRepository.getPurchasesByCodeAndAgeRange(400,0,4);
        mealsRequests.put("fmon1_a", adult400.size());
        mealsRequests.put("fmon1_n", child400.size());
        mealsRequests.put("fmon1_i", infants400.size());
        // staruday lunch 500 for ages in [11,300]
        Set<Purchase> adult500 = purchaseRepository.getPurchasesByCodeAndAgeRange(500,11,300);
        Set<Purchase> child500 = purchaseRepository.getPurchasesByCodeAndAgeRange(500,5,10);
        Set<Purchase> infants500 = purchaseRepository.getPurchasesByCodeAndAgeRange(500,0,4);
        mealsRequests.put("fsat2_a", adult500.size());
        mealsRequests.put("fsat2_n", child500.size());
        mealsRequests.put("fsat2_i", infants500.size());
        // sunday lunch 600 for ages in [11,300]
        Set<Purchase> adult600 = purchaseRepository.getPurchasesByCodeAndAgeRange(600,11,300);
        Set<Purchase> child600 = purchaseRepository.getPurchasesByCodeAndAgeRange(600,5,10);
        Set<Purchase> infants600 = purchaseRepository.getPurchasesByCodeAndAgeRange(600,0,4);
        mealsRequests.put("fsun2_a", adult600.size());
        mealsRequests.put("fsun2_n", child600.size());
        mealsRequests.put("fsun2_i", infants600.size());
        //get friday dinner 700 for ages in [11,300]
        Set<Purchase> adult700 = purchaseRepository.getPurchasesByCodeAndAgeRange(700,11,300);        
        Set<Purchase> child700 = purchaseRepository.getPurchasesByCodeAndAgeRange(700,5,10);
        Set<Purchase> infants700 = purchaseRepository.getPurchasesByCodeAndAgeRange(700,0,4);
        mealsRequests.put("ffri3_a", adult700.size());
        mealsRequests.put("ffri3_n", child700.size());
        mealsRequests.put("ffri3_i", infants700.size());
        //get saturday dinner 800 for ages in [11,300]
        Set<Purchase> adult800 = purchaseRepository.getPurchasesByCodeAndAgeRange(800,11,300);
        Set<Purchase> child800 = purchaseRepository.getPurchasesByCodeAndAgeRange(800,5,10);
        Set<Purchase> infants800 = purchaseRepository.getPurchasesByCodeAndAgeRange(800,0,4);
        mealsRequests.put("fsat3_a", adult800.size());
        mealsRequests.put("fsat3_n", child800.size());
        mealsRequests.put("fsat3_i", infants800.size());
        //get sunday dinner 900 for ages in [11,300]
        Set<Purchase> adult900 = purchaseRepository.getPurchasesByCodeAndAgeRange(900,11,300);
        Set<Purchase> child900 = purchaseRepository.getPurchasesByCodeAndAgeRange(900,5,10);
        Set<Purchase> infants900 = purchaseRepository.getPurchasesByCodeAndAgeRange(900,0,4);
        mealsRequests.put("fsun3_a", adult900.size());
        mealsRequests.put("fsun3_n", child900.size());
        mealsRequests.put("fsun3_i", infants900.size());

        return mealsRequests;
    }

    public Map<String,Integer> getConfirmedMealRequests(){
        Map<String, Integer> mealsRequests = new HashMap<>();

        // saturday breakfast 200 for ages in [11,300]
        Set<Purchase> adult200 = purchaseRepository.getConfirmedPurchasesByCodeAndAgeRange(200,11,300);
        Set<Purchase> child200 = purchaseRepository.getConfirmedPurchasesByCodeAndAgeRange(200,5,10);
        Set<Purchase> infants200 = purchaseRepository.getConfirmedPurchasesByCodeAndAgeRange(200,0,4);
        mealsRequests.put("fsat1_a", adult200.size());
        mealsRequests.put("fsat1_n", child200.size());
        mealsRequests.put("fsat1_i", infants200.size());
        // sunday breakfast 300 for ages in [11,300]
        Set<Purchase> adult300 = purchaseRepository.getConfirmedPurchasesByCodeAndAgeRange(300,11,300);
        Set<Purchase> child300 =  purchaseRepository.getConfirmedPurchasesByCodeAndAgeRange(300,5,10);
        Set<Purchase> infants300 = purchaseRepository.getConfirmedPurchasesByCodeAndAgeRange(300,0,4);
        mealsRequests.put("fsun1_a", adult300.size());
        mealsRequests.put("fsun1_n", child300.size());
        mealsRequests.put("fsun1_i", infants300.size());
        // monday breakfast  400 for ages in [11,300]
        Set<Purchase> adult400 = purchaseRepository.getConfirmedPurchasesByCodeAndAgeRange(400,11,300);
        Set<Purchase> child400 = purchaseRepository.getConfirmedPurchasesByCodeAndAgeRange(400,5,10);
        Set<Purchase> infants400 = purchaseRepository.getConfirmedPurchasesByCodeAndAgeRange(400,0,4);
        mealsRequests.put("fmon1_a", adult400.size());
        mealsRequests.put("fmon1_n", child400.size());
        mealsRequests.put("fmon1_i", infants400.size());
        // staruday lunch 500 for ages in [11,300]
        Set<Purchase> adult500 = purchaseRepository.getConfirmedPurchasesByCodeAndAgeRange(500,11,300);
        Set<Purchase> child500 = purchaseRepository.getConfirmedPurchasesByCodeAndAgeRange(500,5,10);
        Set<Purchase> infants500 = purchaseRepository.getConfirmedPurchasesByCodeAndAgeRange(500,0,4);
        mealsRequests.put("fsat2_a", adult500.size());
        mealsRequests.put("fsat2_n", child500.size());
        mealsRequests.put("fsat2_i", infants500.size());
        // sunday lunch 600 for ages in [11,300]
        Set<Purchase> adult600 = purchaseRepository.getConfirmedPurchasesByCodeAndAgeRange(600,11,300);
        Set<Purchase> child600 = purchaseRepository.getConfirmedPurchasesByCodeAndAgeRange(600,5,10);
        Set<Purchase> infants600 = purchaseRepository.getConfirmedPurchasesByCodeAndAgeRange(600,0,4);
        mealsRequests.put("fsun2_a", adult600.size());
        mealsRequests.put("fsun2_n", child600.size());
        mealsRequests.put("fsun2_i", infants600.size());
        //get friday dinner 700 for ages in [11,300]
        Set<Purchase> adult700 = purchaseRepository.getConfirmedPurchasesByCodeAndAgeRange(700,11,300);        
        Set<Purchase> child700 = purchaseRepository.getConfirmedPurchasesByCodeAndAgeRange(700,5,10);
        Set<Purchase> infants700 = purchaseRepository.getConfirmedPurchasesByCodeAndAgeRange(700,0,4);
        mealsRequests.put("ffri3_a", adult700.size());
        mealsRequests.put("ffri3_n", child700.size());
        mealsRequests.put("ffri3_i", infants700.size());
        //get saturday dinner 800 for ages in [11,300]
        Set<Purchase> adult800 = purchaseRepository.getConfirmedPurchasesByCodeAndAgeRange(800,11,300);
        Set<Purchase> child800 = purchaseRepository.getConfirmedPurchasesByCodeAndAgeRange(800,5,10);
        Set<Purchase> infants800 = purchaseRepository.getConfirmedPurchasesByCodeAndAgeRange(800,0,4);
        mealsRequests.put("fsat3_a", adult800.size());
        mealsRequests.put("fsat3_n", child800.size());
        mealsRequests.put("fsat3_i", infants800.size());
        //get sunday dinner 900 for ages in [11,300]
        Set<Purchase> adult900 = purchaseRepository.getConfirmedPurchasesByCodeAndAgeRange(900,11,300);
        Set<Purchase> child900 = purchaseRepository.getConfirmedPurchasesByCodeAndAgeRange(900,5,10);
        Set<Purchase> infants900 = purchaseRepository.getConfirmedPurchasesByCodeAndAgeRange(900,0,4);
        mealsRequests.put("fsun3_a", adult900.size());
        mealsRequests.put("fsun3_n", child900.size());
        mealsRequests.put("fsun3_i", infants900.size());

        return mealsRequests;
    }
}