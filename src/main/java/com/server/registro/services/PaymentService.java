package com.server.registro.services;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.mail.MessagingException;

import com.server.registro.controllers.network.request.PaymentRequest;
import com.server.registro.controllers.network.response.PaymentResponse;
import com.server.registro.entitites.Guest;
import com.server.registro.entitites.LiasonAmount;
import com.server.registro.entitites.Payment;
import com.server.registro.entitites.PaymentType;
import com.server.registro.entitites.Purchase;
import com.server.registro.entitites.User;
import com.server.registro.exceptions.NotValidPaymentTypeException;
import com.server.registro.exceptions.NotValidUserException;
import com.server.registro.repositories.PaymentRepository;
import com.server.registro.repositories.PaymentTypeRepository;
import com.server.registro.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentService {
    private static final Logger LOG = LoggerFactory.getLogger(PaymentService.class);
    private PaymentRepository paymentRepository;
    private UserRepository userRepository;
    private EmailSenderService emailSenderService;
    private PaymentTypeRepository paymentTypeRepository;

    @Autowired
    public PaymentService(PaymentRepository paymentRepository, UserRepository userRepository,
            PaymentTypeRepository paymentTypeRepository, EmailSenderService emailSenderService) {
        this.paymentRepository = paymentRepository;
        this.userRepository = userRepository;
        this.paymentTypeRepository = paymentTypeRepository;
        this.emailSenderService = emailSenderService;
    }

    /**
     * 
     * @param debtorEmail
     * @param liasonEmail
     * @param amount
     * @param typeDesc
     * @throws NotValidUserException
     * @throws NotValidPaymentTypeException
     * @throws MessagingException
     */
    public void registerPayment(PaymentRequest paymentRequest)
            throws NotValidUserException, NotValidPaymentTypeException, MessagingException {

        String debtorEmail = paymentRequest.getDebtorEmail();
        String liasonEmail = paymentRequest.getLiasonEmail();
        float amount = paymentRequest.getAmount();
        String paymentDescription = paymentRequest.getTypeDesc();

        User debtor = userRepository.findActiveUserByEmail(debtorEmail);

        if (debtor == null) {
            throw new NotValidUserException("User " + debtorEmail + "  no existe");
        }

        User liason = userRepository.findActiveUserByEmail(liasonEmail);

        if (liason == null) {
            throw new NotValidUserException("User " + liasonEmail + "  no existe");
        }

        PaymentType paymentType = paymentTypeRepository.findPaymentTypeByDescription(paymentDescription);

        if (paymentType == null) {
            throw new NotValidPaymentTypeException("metodo de pago no valido " + paymentDescription);
        }
        if (amount == 0) {
            throw new NotValidPaymentTypeException("cantidad no valida de pago " + amount);
        }

        Payment payment = new Payment();

        payment.setDebtor(debtor);
        payment.setLiason(liason);
        payment.setAmount(amount);
        payment.setDate(new Date());
        payment.setPaymentType(paymentType);

        paymentRepository.save(payment);

        float balance = getUserBalance(debtor);

        if (balance <= 0) {
            debtor.setSettleDebt(new Date());
            userRepository.save(debtor);
        }

        // Not sure about this. What if sending email fails?
        String message = EmailMessagesBuilder.paymentConfirmation(payment, debtor, balance);
        emailSenderService.sendEmail(debtor.getEmail(), EmailMessagesBuilder.SUBJECT_RECIBIMOS_PAGO, message);

    }

    // TODO not added yet in view...
    public Set<PaymentResponse> getAllPaymentsByDebtor(String debtorEmail) throws NotValidUserException {

        User account = userRepository.findActiveUserByEmail(debtorEmail);
        if (account == null) {
            throw new NotValidUserException("la cuenta no existe " + debtorEmail);
        }

        Set<PaymentResponse> allPayments = new HashSet<>();
        Set<Payment> payments = paymentRepository.findByDebtor(account);

        for (Payment payment : payments) {
            allPayments.add(new PaymentResponse(payment));
        }

        return allPayments;
    }

    public Set<PaymentResponse> getAllPaymentsByLiason(String liasonEmail) throws NotValidUserException {

        User account = userRepository.findActiveUserByEmail(liasonEmail);
        if (account == null) {
            throw new NotValidUserException("la cuenta no existe " + liasonEmail);
        }

        Set<PaymentResponse> allPayments = new HashSet<>();
        Set<Payment> payments = paymentRepository.findByLiason(account);

        for (Payment payment : payments) {
            allPayments.add(new PaymentResponse(payment));
        }

        return allPayments;
    }

    public float getUserBalance(User debtor) {
        float totalPaid = 0f;
        for (Payment p : debtor.getPayments()) {
            totalPaid += p.getAmount();
        }

        float totalDue = 0f;

        for (Guest guest : debtor.getGuests()) {
            for (Purchase purchase : guest.getItems()) {
                totalDue += purchase.getCost();
            }
        }

        return (totalDue - totalPaid);
    }

    public float getUserBalance(User debtor, Guest deletedGuest) {
        float totalPaid = 0f;

        for (Payment p : debtor.getPayments()) {
            totalPaid += p.getAmount();
        }

        float totalDue = 0f;

        for (Guest guest : debtor.getGuests()) {
            if( guest.getGuestId() == deletedGuest.getGuestId()) {
                continue;
            }
            for (Purchase purchase : guest.getItems()) {
                totalDue += purchase.getCost();
            }
        }

        return (totalDue - totalPaid);
    }
    //TODO we need to improve this. Maybe by using a smart query
    public String getTitular(String email) {
        User user = userRepository.findActiveUserByEmail(email);
        Set<Guest> guests = user.getGuests();
        for(Guest g : guests) {
            if( g.getRelation().equalsIgnoreCase("titular")){
                return (g.getFirstName() + " " + g.getLastName());
            }
        }
        return email;
    }
    public Map<String, Object> getCollectedByLiasonReport() {
        Map<String, Object> allCollectedByLiason = new HashMap<>();
        Set<LiasonAmount> liasonNormal = paymentRepository.getCollectedByLiason(1);// normal payments
        Set<LiasonAmount> liasonDiscount = paymentRepository.getCollectedByLiason(2);// discount payments
        Double totalCollected = 0d;
        Double totalDiscounted = 0d;


        for (LiasonAmount l : liasonNormal) {
            totalCollected += l.amount;
            l.setName(getTitular(l.getEmail()));
        }

        for (LiasonAmount l : liasonDiscount) {
            totalDiscounted += l.getAmount();
            l.setName(getTitular(l.email));
        }

        allCollectedByLiason.put("totalCollected", totalCollected);
        allCollectedByLiason.put("totalDiscounted", totalDiscounted);
        allCollectedByLiason.put("liasonsNormal", liasonNormal);
        allCollectedByLiason.put("liasonsDiscount", liasonDiscount);

        return allCollectedByLiason;
    }
}