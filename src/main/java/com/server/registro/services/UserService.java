package com.server.registro.services;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.mail.MessagingException;
import com.server.registro.repositories.GuestRepository;
import com.server.registro.repositories.RoleRepository;
import com.server.registro.repositories.TokenRepository;
import com.server.registro.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.server.registro.controllers.network.request.*;
import com.server.registro.controllers.network.response.GuestSummaryResponse;
import com.server.registro.controllers.network.response.SummaryAccountResponse;
import com.server.registro.entitites.Guest;
import com.server.registro.entitites.Role;
import com.server.registro.entitites.Token;
import com.server.registro.entitites.User;
import com.server.registro.exceptions.FailedToInstantiateUserException;
import com.server.registro.exceptions.NotValidTokenException;
import com.server.registro.exceptions.NotValidUserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class UserService {

    private static final Logger LOG = LoggerFactory.getLogger(UserService.class);
    public Calendar cal = Calendar.getInstance();
    public Date creationDate = cal.getTime();

    UserRepository userRepository;
    EmailSenderService emailService;
    TokenRepository tokenRepository;
    GuestRepository guestRepository;
    RoleRepository roleRepository;
    TokenService tokenService;
    GuestService guestService;
    PurchaseService purchaseService;

    @Value("${serverAddress}")
    private String serverAddress;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, EmailSenderService emailSenderService,
            TokenRepository tokenRepository, GuestRepository guestRepository, RoleRepository roleRepository,
            TokenService tokenService, GuestService guestService, PurchaseService purchaseService) {

        this.userRepository = userRepository;
        this.emailService = emailSenderService;
        this.tokenRepository = tokenRepository;
        this.guestRepository = guestRepository;
        this.roleRepository = roleRepository;
        this.tokenService = tokenService;
        this.guestService = guestService;
        this.purchaseService = purchaseService;

    }

    public void validateUserToken(String inputToken)
            throws MessagingException, NotValidTokenException, FailedToInstantiateUserException {

        Token token = tokenRepository.findByConfirmationToken(inputToken);

        if (token == null) { // token does not exist
            throw new NotValidTokenException("El correo no existe en nuestro registro");
        } else {

            User user = token.getUser();

            if (tokenService.isUserTokenExpired(user)) {
                throw new NotValidTokenException(
                        "Tardaste demasiado en confirmar tu correo. Es necesario realizar su registro nuevamente.");
            }

            String email = user.getEmail();
            String message = EmailMessagesBuilder.getWelcomeMessage(user);
            Role guestRole = roleRepository.findByDescription("GUEST");

            if (guestRole == null) {
                throw new FailedToInstantiateUserException("Error interno al crear usuario. Contacte al administrdor");
            }

            Set<Role> roles = new HashSet<>();
            roles.add(guestRole);
            user.setRoles(roles);
            user.setValid(true);// enable user to log in
            this.userRepository.save(user);
            for (Guest guest : user.getGuests()) {
                purchaseService.addGuestItems(guest, new GuestItemRequest());
            }

            emailService.sendEmail(email, EmailMessagesBuilder.SUBJECT_BIENVENIDO, message);
        }
    }

    public void handleRegistration(RegistrationRequest newUserForm)
            throws MessagingException, NotValidUserException, FailedToInstantiateUserException {

        String email = newUserForm.getEmail();
        String password = newUserForm.getPassword();
        String city = newUserForm.getCity().toUpperCase();
        String church = newUserForm.getChurch().toUpperCase();
        String state = newUserForm.getState().toUpperCase();
        String country = newUserForm.getCountry().toUpperCase();
        // we associate one guest to this account
        String firstName = newUserForm.getFirstName();
        String lastName = newUserForm.getLastName();

        if (email == null || email.isEmpty()) {
            throw new NotValidUserException("Se debe proveer un correo electrónico");
        }

        if (password == null || password.isEmpty()) {
            throw new NotValidUserException("No se especificó una contraseña");
        }
        if (firstName == null || firstName.isEmpty() || lastName == null || lastName.isEmpty()) {
            throw new FailedToInstantiateUserException("Nombre o apellidos no fueron especificados");
        }

        // If user already exists and it is active
        User user = this.userRepository.findActiveUserByEmail(email);
        // someone used that email but did not validate it
        User noActiveUser = this.userRepository.findUserByEmail(email);

        if (user == null) { // new user

            if (noActiveUser != null) {
                // delete non-validated user
                this.userRepository.delete(noActiveUser);
            }

            // we define a pre-user
            User preUser = new User();
            preUser.setEmail(email);
            preUser.setPassword(passwordEncoder.encode(password));
            preUser.setValid(false);
            preUser.setChurch(church);
            preUser.setCity(city);
            preUser.setState(state);
            preUser.setCountry(country);
            userRepository.save(preUser);

            Token token = new Token(preUser);
            tokenRepository.save(token);

            guestService.createTitular(preUser, firstName, lastName);
            String message = EmailMessagesBuilder.getConfirmationEmailMessage(token, serverAddress);
            emailService.sendEmail(preUser.getEmail(), EmailMessagesBuilder.SUBJECT_VALIDA_EMAIL, message);

        } else {
            if (LOG.isDebugEnabled()) {
                LOG.debug("user already exists! ");
            }
            throw new NotValidUserException(
                    "Este correo ya fue registrado anteriormente. No se permite duplicar el correo para otro registro");
        }
    }

    public Set<Guest> retrieveUserGuests(String email) {
        User user = this.userRepository.findActiveUserByEmail(email);
        return user.getGuests();
    }

    public SummaryAccountResponse allGuests(Long id) throws NotValidUserException {
        User account = this.userRepository.findUserByUserId(id);
        return allGuests(account.getEmail());
    }

    public SummaryAccountResponse allGuests(String email) throws NotValidUserException {

        User account = this.userRepository.findActiveUserByEmail(email);

        if (account == null) {
            throw new NotValidUserException("User: " + email + " does not exist");
        }

        return new SummaryAccountResponse(account);
    }

    public Set<SummaryAccountResponse> getAllAccounts(String filter) {
        Iterable<User> allUsers = userRepository.findByIsValid(true);
        Set<SummaryAccountResponse> accountsSummary = new HashSet<>();

        for (User user : allUsers) {
            accountsSummary.add(new SummaryAccountResponse(user));
        }

        return accountsSummary;
    }

    public Set<SummaryAccountResponse> getAccountByFilter(String filter, String value) {

        Set<SummaryAccountResponse> accountsSummary = new HashSet<>();
        User user = null;

        switch (filter.toUpperCase()) {
        case "EMAIL":
            user = userRepository.findActiveUserByEmail(value);
            break;
        case "ID": // TODO replace
            user = userRepository.findUserByUserId(Long.parseLong(value));
            break;
        }

        if (user != null) { // user was found
            accountsSummary.add(new SummaryAccountResponse(user));
        }

        return accountsSummary;
    }

    public Set<SummaryAccountResponse> getAccountsByFilter(String filter, String value) {

        Set<SummaryAccountResponse> accountsSummary = new HashSet<>();
        Iterable<User> allUsers;
        LOG.info("searching for {} = value", filter);

        switch (filter.toUpperCase()) {
        case "CIUDAD":
            LOG.info("searching by city {}", value.toUpperCase());
            allUsers = userRepository.findValidUsersByCity(value.toUpperCase());
            break;
        case "IGLESIA":
            allUsers = userRepository.findValidUsersByChurch(value.toUpperCase());
            break;
        case "LIASON":
            allUsers = userRepository.findByIsValid(true);
            break;
        default:// return all users. This might be inneficient
            allUsers = userRepository.findByIsValid(true);

        }

        // no users found
        if (allUsers == null) {
            return accountsSummary;
        }

        for (User user : allUsers) {
            accountsSummary.add(new SummaryAccountResponse(user));
        }

        return accountsSummary;
    }

    /**
     * 
     * @return
     */
    public Map<Long, Set<GuestSummaryResponse>> getAllValidUsersAndGuests() {
        Set<User> allUsers = userRepository.findAllByIsValidAndSettleDebtIsNotNullOrderBySettleDebtAsc(true);
        Map<Long, Set<GuestSummaryResponse>> guests = new HashMap<>();

        Long pos = 0l;

        for (User user : allUsers) {            
            Set<GuestSummaryResponse> guestSummary = new HashSet<>();
            for (Guest guest : user.getGuests()) {
                GuestSummaryResponse guestResponse = GuestSummaryResponse.createGuestSummary4Lodging(guest, user);
                guestSummary.add(guestResponse);
            }
            guests.put(pos, guestSummary);
            pos++;
        }
        return guests;
    }

    public Map<Long, Set<GuestSummaryResponse>> getAllGuests() {
        Set<User> allValidUsers = userRepository.findAllByIsValid(true);
        Map<Long, Set<GuestSummaryResponse>> guests = new HashMap<>();

        Long pos = 0l;

        for (User user : allValidUsers) {   
                     
            Set<GuestSummaryResponse> guestSummary = new HashSet<>();
            for (Guest guest : user.getGuests()) {
                GuestSummaryResponse guestResponse = GuestSummaryResponse.createGuestSummary4Lodging(guest, user);
                guestSummary.add(guestResponse);
            }
            guests.put(pos, guestSummary);
            pos++;
        }
        return guests;
    }
}