package com.server.registro.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.server.registro.entitites.TokenPassword;
import com.server.registro.entitites.User;
import com.server.registro.exceptions.*;
import com.server.registro.repositories.*;
import javax.mail.MessagingException;
import com.server.registro.controllers.network.request.*;
import com.server.registro.services.EmailMessagesBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class ChangePasswordService {
    private static final Logger LOG = LoggerFactory.getLogger(UserService.class);

    @Autowired
    UserRepository userRepository;
    @Autowired
    TokenPasswordRepository tokenPasswordRepository;
    @Autowired
    PasswordEncoder passwordEncoder;

    @Value("${serverAddress}")
    private String serverAddress;
    

    @Autowired
    TokenPasswordService tokenService;
    @Autowired
    EmailSenderService emailService;
    

    public void validateUserTokenPassword(String inputTokenPassword) throws MessagingException, NotValidTokenException {

        TokenPassword tokenPassword = tokenPasswordRepository.findByConfirmationTokenPassword(inputTokenPassword);

        if (tokenPassword == null) { // token does not exist
            throw new NotValidTokenException("token " + inputTokenPassword + "is not valid");
        }

        if (tokenService.isUserTokenPasswordExpired(tokenPassword)) {
            // token exists but has expired
            tokenPasswordRepository.delete(tokenPassword);
            throw new NotValidTokenException("token has expired!");
        }
    }

    public void handlePasswordReset(ChangingPasswordRequest newPasswordForm)
            throws MessagingException, NotValidUserException, FailedToResetPasswordException, NotValidTokenException {

        String token = newPasswordForm.getToken();
        String newPassword = newPasswordForm.getNewPassword();

        TokenPassword tokenPassword = tokenPasswordRepository.findByConfirmationTokenPassword(token);
        // we validate the token is still valid!
        if (tokenService.isUserTokenPasswordExpired(tokenPassword)) {
            throw new NotValidTokenException("token " + tokenPassword.getConfirmationTokenPassword() + " has expired");
        }
        // now validate password
        if (newPassword == null || newPassword.isEmpty()) {
            throw new FailedToResetPasswordException("Nuevo password no fue proporcionado");
        }

        String email = tokenPassword.getUser().getEmail();

        User user = this.userRepository.findUserByEmail(email);


        if (user == null) {
            throw new NotValidUserException("La cuenta solicitada no existe ");
        }
        
        user.setPassword(passwordEncoder.encode(newPassword));
        userRepository.save(user);
    }

    public void CreateTokenPassword(String email) throws MessagingException, NotValidUserException {
        User user = userRepository.findUserByEmail(email);
        if (user == null) {
            throw new NotValidUserException("El email: "+ email+" no esta registrado");
        }

        TokenPassword oldToken = user.getValidationTokenPassword();
        TokenPassword tokenPassword = new TokenPassword(user);

        if (oldToken != null) {
            user.setValidationTokenPassword(null);
            tokenPasswordRepository.delete(oldToken);
        }

        tokenPasswordRepository.save(tokenPassword);
        String message = EmailMessagesBuilder.getEmailMessageTokenPassword(tokenPassword, serverAddress);
        emailService.sendEmail(email, "Modificar contraseña", message);
    }

}