package com.server.registro;

import com.server.registro.services.UserDetailsServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public UserDetailsService userDetailsService() {
        return new UserDetailsServiceImpl();
    }

    @Bean
    public static PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService());
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authenticationProvider());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
            .authorizeRequests()
                .antMatchers("/dashboard").hasRole("ADMIN")
                .antMatchers("/liason/**","/edita-perfil").access("hasRole('LIASON') or hasRole('ADMIN')")
                .antMatchers("/guest-details-page","/assign-room").access("hasRole('LODGE_ATTENDANT') or hasRole('ADMIN')")
                .antMatchers("/meals-control-page","/add-meal-report").access("hasRole('KITCHEN_ATTENDANT') or hasRole('ADMIN')")
                .antMatchers("/guest-checkin-page").access("hasRole('DESK_ATTENDANT') or hasRole('ADMIN')")
                .antMatchers("/crea-cuenta-page","/crea-cuenta", "/confirma-email", "/password/**","/error", "/static/**","/resources/**","/img/**","/webjars/**", "/css/**", "/js/**").permitAll()                
                .anyRequest().authenticated()
                .and()
            .formLogin()
                .loginPage("/login")
                .successForwardUrl("/postLogin")
                .failureUrl("/loginFailed")
                .permitAll()
                .and()
            .logout()
            .deleteCookies("JSESSIONID")
            .permitAll();

    }
    
}