package com.server.registro.entitites;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CbUser implements UserDetails {

    private static final Logger LOG = LoggerFactory.getLogger(CbUser.class);
    private String ROLE_PREFIX = "ROLE_";
    private User user;
    public CbUser(User user) {
        this.user = user;
    }
    
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {        
        List<GrantedAuthority> roles = new LinkedList<>();

        for ( Role role : user.getRoles()) {
            roles.add( new SimpleGrantedAuthority(ROLE_PREFIX + role.getDescription()));
        }
        
        LOG.info(" user {}  has roles => {}", user.getEmail() ,  roles);      
        return roles;
    }

    public long getId() {
        return user.getUserId();
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    @Override
    public boolean isEnabled() {
        return user.isValid();
    }
    public User getUserDetails() {
        return user;
    }

}