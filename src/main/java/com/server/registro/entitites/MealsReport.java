package com.server.registro.entitites;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

/**
 * This is a hot fix to have a report of the total people in the food court
 */
@Entity
@Table(name = "MealsReport")
public class MealsReport {
    private static final Logger LOG = LoggerFactory.getLogger(MealsReport.class);
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long mealReportId;
    int code;// one of the 8 meals
    int type;// 3= adult, 2=children, 1=infant
    int amount; // one bunch of ppl getting in
    @CreatedDate
    Date added; // when was added this row
    String note; // any note

    public MealsReport(){

    }

    public long getMealReportId() {
        return mealReportId;
    }

    public void setMealReportId(long mealReportId) {
        this.mealReportId = mealReportId;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Date getAdded() {
        return added;
    }

    public void setAdded(Date added) {
        this.added = added;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
    
}