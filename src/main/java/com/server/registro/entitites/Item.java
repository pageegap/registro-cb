package com.server.registro.entitites;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.GeneratedValue;

@Entity
@Table(name = "item")
public class Item {

    @Id    
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long itemId;
    private String name;
    private int type; //enum? 
    private Float price;
    private boolean isActive;
    private Date updateDate;
    private int initialAge;
    private int endAge;

    @ManyToOne
    @JoinColumn(name = "userId", nullable = true)
    private User updateUserId;

    public Item() {
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the price
     */
    public Float getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(Float price) {
        this.price = price;
    }

    /**
     * @return the isActive
     */
    public boolean getIsActive() {
        return isActive;
    }

    /**
     * @param isActive the isActive to set
     */
    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }



    /**
     * @param isActive the isActive to set
     */
    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    /**
     * @return the updateDate
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return the updateUserId
     */
    public User getUpdateUserId() {
        return updateUserId;
    }

    /**
     * @param updateUserId the updateUserId to set
     */
    public void setUpdateUserId(User updateUserId) {
        this.updateUserId = updateUserId;
    }

    /**
     * @return the initialAge
     */
    public int getInitialAge() {
        return initialAge;
    }

    /**
     * @param initialAge the initialAge to set
     */
    public void setInitialAge(int initialAge) {
        this.initialAge = initialAge;
    }

    /**
     * @return the endAge
     */
    public int getEndAge() {
        return endAge;
    }

    /**
     * @param endAge the endAge to set
     */
    public void setEndAge(int endAge) {
        this.endAge = endAge;
    }

    @Column(name="item_id")
    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Item [endAge=" + endAge + ", initialAge=" + initialAge + ", isActive=" + isActive + ", itemId=" + itemId
                + ", name=" + name + ", price=" + price + ", type=" + type + ", updateDate=" + updateDate
                + ", updateUserId=" + updateUserId + "]";
    }

}