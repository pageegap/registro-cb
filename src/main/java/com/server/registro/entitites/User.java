package com.server.registro.entitites;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "user")
public class User {

     @Id
     @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="user_generator")
     @SequenceGenerator(name="user_generator", initialValue=1, allocationSize=1, sequenceName = "user_seq")
     long userId;

     @NotNull
     @Column(unique = true)
     private String email;

     private String phoneNumber;

     @NotNull
     private String password;

     private boolean isValid;
     // date when user's debt goes down to zero
     private Date settleDebt;

     private String church;
     private String city;
     private String state;
     private String country;

     // many users may have the same (one) role. We restrict cascade to avoid
     // deletion of roles
     @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
     @JoinTable(name = "user_role", joinColumns = { @JoinColumn(name = "userId") }, inverseJoinColumns = {
               @JoinColumn(name = "roleId") })
     private Set<Role> roles;

     // each user has only one token and viceversa
     @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "user", orphanRemoval = true)
     private Token validationToken;

     @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "user", orphanRemoval = true)
     private TokenPassword validationTokenPassword;

     // one user has a set of payments. TODO figure right modifiers
     @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "debtor")
     private Set<Payment> payments = new HashSet<>();

     // one user has a set of cobros. TODO figure out right modifiers
     @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "liason")
     private Set<Payment> cobros = new HashSet<>();

     @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "owner", orphanRemoval = true)
     private Set<Guest> guests = new HashSet<>();

     public User() {
     }

     /**
      * @return the email
      */
     public String getEmail() {
          return email;
     }

     /**
      * @param email the email to set
      */
     public void setEmail(String email) {
          this.email = email;
     }

     /**
      * @return the password
      */
     public String getPassword() {
          return password;
     }

     /**
      * @param password the password to set
      */
     public void setPassword(String password) {
          this.password = password;
     }

     /**
      * @return the isValid
      */
     public boolean isValid() {
          return isValid;
     }

     /**
      * @param isValid the isValid to set
      */
     public void setValid(boolean isValid) {
          this.isValid = isValid;
     }

     /**
      * @return the validationToken
      */
     public Token getValidationToken() {
          return validationToken;
     }

     /**
      * @param validationToken the validationToken to set
      */
     public void setValidationToken(Token validationToken) {
          this.validationToken = validationToken;
     }

     /**
      * @return the userId
      */
     public long getUserId() {
          return userId;
     }

     /**
      * @param userId the userId to set
      */
     public void setUserId(long userId) {
          this.userId = userId;
     }

     /**
      * @return the payments
      */
     public Set<Payment> getPayments() {
          return payments;
     }

     /**
      * @param payments the payments to set
      */
     public void setPayments(Set<Payment> payments) {
          this.payments = payments;
     }

     /**
      * @return the cobros
      */
     public Set<Payment> getCobros() {
          return cobros;
     }

     /**
      * @param cobros the cobros to set
      */
     public void setCobros(Set<Payment> cobros) {
          this.cobros = cobros;
     }

     /**
      * @return the settleDebt
      */
     public Date getSettleDebt() {
          return settleDebt;
     }

     /**
      * @param settleDebt the settleDebt to set
      */
     public void setSettleDebt(Date settleDebt) {
          this.settleDebt = settleDebt;
     }

     /**
      * @return the phoneNumber
      */
     public String getPhoneNumber() {
          return phoneNumber;
     }

     /**
      * @param phoneNumber the phoneNumber to set
      */
     public void setPhoneNumber(String phoneNumber) {
          this.phoneNumber = phoneNumber;
     }

     /**
      * @return the guests
      */
     public Set<Guest> getGuests() {
          return guests;
     }

     /**
      * @param guests the guests to set
      */
     public void setGuests(Set<Guest> guests) {
          this.guests = guests;
     }

     @Override
     public String toString() {
          return "User [cobros=" + cobros + ", email=" + email + ", guests=" + guests + ", isValid=" + isValid
                    + ", password=" + password + ", payments=" + payments + ", phoneNumber=" + phoneNumber + ", role="
                    + roles + ", settleDebt=" + settleDebt + ", userId=" + userId + ", validationToken="
                    + validationToken + "]";
     }

     public String getChurch() {
          return church;
     }

     public void setChurch(String church) {
          this.church = church;
     }

     public String getCity() {
          return city;
     }

     public void setCity(String city) {
          this.city = city;
     }

     public String getState() {
          return state;
     }

     public void setState(String state) {
          this.state = state;
     }

     public Set<Role> getRoles() {
          return roles;
     }

     public void setRoles(Set<Role> roles) {
          this.roles = roles;
     }

     public String getCountry() {
          return country;
     }

     public void setCountry(String country) {
          this.country = country;
     }

     public TokenPassword getValidationTokenPassword() {
          return validationTokenPassword;
     }

     public void setValidationTokenPassword(TokenPassword validationTokenPassword) {
          this.validationTokenPassword = validationTokenPassword;
     }

}
