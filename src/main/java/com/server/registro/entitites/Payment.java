package com.server.registro.entitites;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.springframework.data.annotation.CreatedDate;


@Entity
@Table(name = "payment")
@SequenceGenerator(name="payment_seq", initialValue=1)
public class Payment {
   

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="payment_seq")
    private long paymentId;
    private float amount;

    @CreatedDate
    private Date date;
   
    
    @ManyToOne
    @JoinColumn(name = "paymentTypeId", nullable = false)
    private PaymentType paymentType;    
        
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "debtorId", nullable = false)
    private User debtor;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "liasonId", nullable = false)
    private User liason;

    public Payment(){
        
    }

    /**
     * @return the paymentId
     */
    public long getPaymentId() {
        return paymentId;
    }

    /**
     * @param paymentId the paymentId to set
     */
    public void setPaymentId(long paymentId) {
        this.paymentId = paymentId;
    }

    /**
     * @return the amount
     */
    public float getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(float amount) {
        this.amount = amount;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return the paymentType
     */
    public PaymentType getPaymentType() {
        return paymentType;
    }

    /**
     * @param paymentType the paymentType to set
     */
    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    /**
     * @return the debtor
     */
    public User getDebtor() {
        return debtor;
    }

    /**
     * @param debtor the debtor to set
     */
    public void setDebtor(User debtor) {
        this.debtor = debtor;
    }

    /**
     * @return the liason
     */
    public User getLiason() {
        return liason;
    }

    /**
     * @param liason the liason to set
     */
    public void setLiason(User liason) {
        this.liason = liason;
    }

    
}