package com.server.registro.entitites;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.UpdateTimestamp;



@Entity
@Table(name = "guest")
public class Guest {

     private static final long serialVersionUID = 1L;

     @Id
     @Column(name="guest_id")
     @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="guest_generator")
     @SequenceGenerator(name="guest_generator", initialValue=1, allocationSize=1,sequenceName = "guest_seq")
     private long guestId;

     private String firstName;
     private String lastName;
     private int age;
     private String gender;
     private String relation;
     private String note;
     private String room;
     private Date checkinDate;
     
     @UpdateTimestamp
     private Date lastUpdate;
     
     private boolean isPregnant;
     private boolean isOwner;
     private boolean isMedicated;
     private String phoneNumber;
     private boolean hasDisability;
     private String maritalStatus;
     

     @ManyToOne(fetch = FetchType.LAZY, optional = false, cascade = CascadeType.ALL)
     @JoinColumn(name = "userId", nullable = false)
     private User owner;

     @OneToMany(fetch = FetchType.LAZY, mappedBy = "guest", cascade = CascadeType.ALL)
     Set<Purchase> items = new HashSet<>();

     public Guest() {
     }

     /**
      * @return the serialversionuid
      */
     public static long getSerialversionuid() {
          return serialVersionUID;
     }

     /**
      * @return the guestId
      */
     public long getGuestId() {
          return guestId;
     }

     /**
      * @param guestId the guestId to set
      */
     public void setGuestId(long guestId) {
          this.guestId = guestId;
     }

    
     /**
      * @return the firstName
      */
     public String getFirstName() {
          return firstName;
     }

     /**
      * @param firstName the firstName to set
      */
     public void setFirstName(String firstName) {
          this.firstName = firstName;
     }

     /**
      * @return the lastName
      */
     public String getLastName() {
          return lastName;
     }

     /**
      * @param lastName the lastName to set
      */
     public void setLastName(String lastName) {
          this.lastName = lastName;
     }

     /**
      * @return the age
      */
     public int getAge() {
          return age;
     }

     /**
      * @param age the age to set
      */
     public void setAge(int age) {
          this.age = age;
     }

     /**
      * @return the gender
      */
     public String getGender() {
          return gender;
     }

     /**
      * @param gender the gender to set
      */
     public void setGender(String gender) {
          this.gender = gender;
     }
     

     /**
      * @return the relation
      */
     public String getRelation() {
          return relation;
     }

     /**
      * @param relation the relation to set
      */
     public void setRelation(String relation) {
          this.relation = relation;
     }

     /**
      * @return the note
      */
     public String getNote() {
          return note;
     }

     /**
      * @param note the note to set
      */
     public void setNote(String note) {
          this.note = note;
     }

     /**
      * @return the room
      */
     public String getRoom() {
          return room;
     }

     /**
      * @param room the room to set
      */
     public void setRoom(String room) {
          this.room = room;
     }

     /**
      * @return the checkinDate
      */
     public Date getCheckinDate() {
          return checkinDate;
     }

     /**
      * @param checkinDate the checkinDate to set
      */
     public void setCheckinDate(Date checkinDate) {
          this.checkinDate = checkinDate;
     }

     /**
      * @return the lastUpdate
      */
     public Date getLastUpdate() {
          return lastUpdate;
     }

     /**
      * @param lastUpdate the lastUpdate to set
      */
     public void setLastUpdate(Date lastUpdate) {
          this.lastUpdate = lastUpdate;
     }

     /**
      * @return the isPregnant
      */
     public boolean isPregnant() {
          return isPregnant;
     }

     /**
      * @param isPregnant the isPregnant to set
      */
     public void setPregnant(boolean isPregnant) {
          this.isPregnant = isPregnant;
     }

     /**
      * @return the isMedicated
      */
     public boolean isMedicated() {
          return isMedicated;
     }

     /**
      * @param isMedicated the isMedicated to set
      */
     public void setMedicated(boolean isMedicated) {
          this.isMedicated = isMedicated;
     }

     /**
      * @return the phoneNumber
      */
     public String getPhoneNumber() {
          return phoneNumber;
     }

     /**
      * @param phoneNumber the phoneNumber to set
      */
     public void setPhoneNumber(String phoneNumber) {
          this.phoneNumber = phoneNumber;
     }

     /**
      * @return the hasDisability
      */
     public boolean isHasDisability() {
          return hasDisability;
     }

     /**
      * @param hasDisability the hasDisability to set
      */
     public void setHasDisability(boolean hasDisability) {
          this.hasDisability = hasDisability;
     }

     /**
      * @return the maritalStatus
      */
     public String getMaritalStatus() {
          return maritalStatus;
     }

     /**
      * @param maritalStatus the maritalStatus to set
      */
     public void setMaritalStatus(String maritalStatus) {
          this.maritalStatus = maritalStatus;
     }

     /**
      * @return the owner
      */
     public User getOwner() {
          return owner;
     }

     /**
      * @param owner the owner to set
      */
     public void setOwner(User owner) {
          this.owner = owner;
     }

     public Set<Purchase> getItems() {
          return items;
     }

    public void setItems(Set<Purchase> items) {
          this.items = items;
     }

     public boolean isOwner() {
          return isOwner;
     }

     public void setOwner(boolean isOwner) {
          this.isOwner = isOwner;
     }

     @Override
     public String toString() {
          return "Guest [age=" + age + ", checkinDate=" + checkinDate + ", firstName=" + firstName + ", gender="
                    + gender + ", guestId=" + guestId + ", hasDisability=" + hasDisability + ", isMedicated="
                    + isMedicated + ", isOwner=" + isOwner + ", isPregnant=" + isPregnant + ", items=" + items.size()
                    + ", lastName=" + lastName + ", lastUpdate=" + lastUpdate + ", maritalStatus=" + maritalStatus
                    + ", note=" + note + ", owner=" + owner.getEmail() + ", phoneNumber=" + phoneNumber + ", relation=" + relation
                    + ", room=" + room + "]";
     }

  

 

}
