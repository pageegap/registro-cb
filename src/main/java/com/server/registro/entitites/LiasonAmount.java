package com.server.registro.entitites;

import java.io.Serializable;

import org.eclipse.jdt.internal.compiler.ast.DoubleLiteral;

public class LiasonAmount implements Serializable {

    public String name;
    public String email;
    public Double amount;

    public LiasonAmount(String email, Double amount){
        this.email = email;
        this.amount = amount;
    }

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "LiasonAmount [amount=" + amount + ", email=" + email + ", name=" + name + "]";
    }


}