package com.server.registro.entitites;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "token_password")
public class TokenPassword implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long tokenPasswordId;

    private String confirmationTokenPassword;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    // each token is linked to only one user
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "userId", nullable = false)
    private User user;

    public TokenPassword() {
    }

    public TokenPassword(User user) {
        this.user = user;
        createdDate = new Date();
        confirmationTokenPassword = UUID.randomUUID().toString();
    }

    /**
     * @return the confirmationToken
     */
    public String getConfirmationTokenPassword() {
        return confirmationTokenPassword;
    }

    /**
     * @param confirmationToken the confirmationToken to set
     */
    public void setConfirmationTokenPassword(String confirmationTokenPassword) {
        this.confirmationTokenPassword = confirmationTokenPassword;
    }

    /**
     * @return the createdDate
     */
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * @param createdDate the createdDate to set
     */
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the tokenPasswordId
     */
    public long getTokenPasswordId() {
        return tokenPasswordId;
    }

    /**
     * @param tokenPasswordId the tokenId to set
     */
    public void setTokenPasswordId(long tokenPasswordId) {
        this.tokenPasswordId = tokenPasswordId;
    }

}