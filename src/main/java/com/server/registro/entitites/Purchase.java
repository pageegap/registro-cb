package com.server.registro.entitites;

import java.util.Date;


import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Table(name = "purchase")
public class Purchase {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long purchaseId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "guestId", nullable = false)
    private Guest guest;
    
    @LastModifiedDate
    private Date added;

    private Date used;
    
    private String description;
    
    private int code;
    
    private float cost;
    
    public Purchase() {
    }

   
    /**
     * @return the guest
     */
    public Guest getGuest() {
        return guest;
    }

    /**
     * @param guest the guest to set
     */
    public void setGuest(Guest guest) {
        this.guest = guest;
    }

  
    /**
     * @return the added
     */
    public Date getAdded() {
        return added;
    }

    /**
     * @param added the added to set
     */
    public void setAdded(Date added) {
        this.added = added;
    }

    /**
     * @return the used
     */
    public Date getUsed() {
        return used;
    }

    /**
     * @param used the used to set
     */
    public void setUsed(Date used) {
        this.used = used;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public long getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(long purchaseId) {
        this.purchaseId = purchaseId;
    }

    @Override
    public String toString() {
        return "Purchase [added=" + added + ", code=" + code + ", cost=" + cost + ", description=" + description
                + ", guest=" + guest + ", purchaseId=" + purchaseId + ", used=" + used + "]";
    }

  
  
}